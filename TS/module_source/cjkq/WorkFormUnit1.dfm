object WorkForm: TWorkForm
  Left = 540
  Top = 0
  Align = alClient
  AutoSize = True
  BorderStyle = bsNone
  ClientHeight = 552
  ClientWidth = 1031
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object MasterP: TPanel
    Left = 0
    Top = 0
    Width = 1031
    Height = 552
    Align = alClient
    FullRepaint = False
    TabOrder = 0
    object MGrid: TcxGrid
      AlignWithMargins = True
      Left = 4
      Top = 105
      Width = 1023
      Height = 443
      Align = alClient
      TabOrder = 0
      LookAndFeel.NativeStyle = False
      ExplicitTop = 106
      object MTV: TcxGridBandedTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        Styles.Background = cxStyle1
        Styles.ContentOdd = cxStyle1
        Styles.BandHeader = cxStyle2
        Bands = <
          item
            Caption = #36710#38388#32771#21220#34920
          end
          item
            Caption = #22522#30784#20449#24687
            Position.BandIndex = 0
            Position.ColIndex = 0
          end
          item
            Caption = #26085#26399
            Position.BandIndex = 0
            Position.ColIndex = 1
          end
          item
            Caption = '1'
            Position.BandIndex = 2
            Position.ColIndex = 0
          end
          item
            Caption = '2'
            Position.BandIndex = 2
            Position.ColIndex = 1
          end
          item
            Caption = '3'
            Position.BandIndex = 2
            Position.ColIndex = 2
          end
          item
            Caption = '4'
            Position.BandIndex = 2
            Position.ColIndex = 3
          end
          item
            Caption = '5'
            Position.BandIndex = 2
            Position.ColIndex = 4
          end
          item
            Caption = '6'
            Position.BandIndex = 2
            Position.ColIndex = 5
          end
          item
            Caption = '7'
            Position.BandIndex = 2
            Position.ColIndex = 6
          end
          item
            Caption = '8'
            Position.BandIndex = 2
            Position.ColIndex = 7
          end
          item
            Caption = '9'
            Position.BandIndex = 2
            Position.ColIndex = 8
          end
          item
            Caption = '10'
            Position.BandIndex = 2
            Position.ColIndex = 9
          end
          item
            Caption = '11'
            Position.BandIndex = 2
            Position.ColIndex = 10
          end
          item
            Caption = '12'
            Position.BandIndex = 2
            Position.ColIndex = 11
          end
          item
            Caption = '13'
            Position.BandIndex = 2
            Position.ColIndex = 12
          end
          item
            Caption = '14'
            Position.BandIndex = 2
            Position.ColIndex = 13
          end
          item
            Caption = '15'
            Position.BandIndex = 2
            Position.ColIndex = 14
          end
          item
            Caption = '16'
            Position.BandIndex = 2
            Position.ColIndex = 15
          end
          item
            Caption = '17'
            Position.BandIndex = 2
            Position.ColIndex = 16
          end
          item
            Caption = '18'
            Position.BandIndex = 2
            Position.ColIndex = 17
          end
          item
            Caption = '19'
            Position.BandIndex = 2
            Position.ColIndex = 18
          end
          item
            Caption = '20'
            Position.BandIndex = 2
            Position.ColIndex = 19
          end
          item
            Caption = '21'
            Position.BandIndex = 2
            Position.ColIndex = 20
          end
          item
            Caption = '22'
            Position.BandIndex = 2
            Position.ColIndex = 21
          end
          item
            Caption = '23'
            Position.BandIndex = 2
            Position.ColIndex = 22
          end
          item
            Caption = '24'
            Position.BandIndex = 2
            Position.ColIndex = 23
          end
          item
            Caption = '25'
            Position.BandIndex = 2
            Position.ColIndex = 24
          end
          item
            Caption = '26'
            Position.BandIndex = 2
            Position.ColIndex = 25
          end
          item
            Caption = '27'
            Position.BandIndex = 2
            Position.ColIndex = 26
          end
          item
            Caption = '28'
            Position.BandIndex = 2
            Position.ColIndex = 27
          end
          item
            Caption = '29'
            Position.BandIndex = 2
            Position.ColIndex = 28
          end
          item
            Caption = '30'
            Position.BandIndex = 2
            Position.ColIndex = 29
          end
          item
            Caption = '31'
            Position.BandIndex = 2
            Position.ColIndex = 30
          end>
        object MTVColumn1: TcxGridBandedColumn
          Caption = #24207#21495
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Sorting = False
          Width = 46
          Position.BandIndex = 1
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn2: TcxGridBandedColumn
          Caption = #29677#32452#32534#21495
          HeaderAlignmentHorz = taCenter
          SortIndex = 0
          SortOrder = soAscending
          Position.BandIndex = 1
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object MTVColumn3: TcxGridBandedColumn
          Caption = #29677#32452#21517#31216' '
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Sorting = False
          Position.BandIndex = 1
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object MTVColumn4: TcxGridBandedColumn
          Caption = #24037#21495
          HeaderAlignmentHorz = taCenter
          Position.BandIndex = 1
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object MTVColumn5: TcxGridBandedColumn
          Caption = #22995#21517
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Sorting = False
          Position.BandIndex = 1
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
        object MTVColumn6: TcxGridBandedColumn
          Caption = #20986#21220
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Sorting = False
          Position.BandIndex = 1
          Position.ColIndex = 5
          Position.RowIndex = 0
        end
        object MTVColumn7: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 3
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn8: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 4
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn9: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 5
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn10: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 6
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn11: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 7
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn12: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 8
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn13: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 9
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn14: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 10
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn15: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 11
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn16: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 12
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn17: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 13
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn18: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 14
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn19: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 15
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn20: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 16
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn21: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 17
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn22: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 18
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn23: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 19
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn24: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 20
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn25: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 21
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn26: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 22
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn27: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 23
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn28: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 24
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn29: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 25
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn30: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 26
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn31: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 27
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn32: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 28
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn33: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 29
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn34: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 30
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn35: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 31
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn36: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 32
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn37: TcxGridBandedColumn
          Options.Filtering = False
          Options.Sorting = False
          Styles.OnGetContentStyle = MTVColumn7StylesGetContentStyle
          Position.BandIndex = 33
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
      end
      object MGridLevel1: TcxGridLevel
        GridView = MTV
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 1029
      Height = 101
      Align = alTop
      BevelKind = bkTile
      BevelOuter = bvNone
      TabOrder = 1
      object Panel2: TPanel
        Left = 0
        Top = 61
        Width = 1025
        Height = 36
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 19
          Top = 11
          Width = 300
          Height = 12
          Caption = #32771#21220#25152#22312#26376' '#65306'                           '#37096#38376#32534#21495#65306
        end
        object btime: TDateTimePicker
          Left = 103
          Top = 7
          Width = 105
          Height = 21
          Date = 43089.655190486110000000
          Time = 43089.655190486110000000
          TabOrder = 0
          OnChange = btimeChange
        end
        object ckbhcb: TComboBox
          Left = 330
          Top = 7
          Width = 100
          Height = 20
          Style = csDropDownList
          TabOrder = 1
          OnChange = ckbhcbChange
        end
        object ckmccb: TComboBox
          Left = 460
          Top = 7
          Width = 149
          Height = 21
          Style = csSimple
          Enabled = False
          TabOrder = 2
          Items.Strings = (
            '02003-'#26611#23663#36741#26009#19968#24211
            '02002-'#26611#23663#36741#26009#20108#24211
            '03004-'#35199#21378#36741#26009#19968#24211
            '04005-'#33829#21475#36741#26009#19968#24211)
        end
        object ljHead: TLabeledEdit
          Left = 742
          Top = 7
          Width = 255
          Height = 20
          EditLabel.Width = 120
          EditLabel.Height = 12
          EditLabel.Caption = #25193#23637#25511#20214#65288#26410#21551#29992#65289#65306
          LabelPosition = lpLeft
          TabOrder = 3
          Visible = False
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 1025
        Height = 57
        Align = alTop
        TabOrder = 1
        object RunQuery: TButton
          Left = 656
          Top = 7
          Width = 133
          Height = 44
          Caption = #26597#35810
          TabOrder = 0
          OnClick = RunQueryClick
        end
        object ToXLS: TButton
          Left = 816
          Top = 7
          Width = 133
          Height = 44
          Caption = #23548#20986'Excel'
          TabOrder = 1
          OnClick = ToxlsClick
        end
        object ztRG: TRadioGroup
          Left = 1
          Top = 1
          Width = 593
          Height = 55
          Align = alLeft
          Caption = #21487#36873#36134#22871
          TabOrder = 2
          OnClick = ztRGClick
        end
        object Button1: TButton
          Left = 968
          Top = 24
          Width = 75
          Height = 25
          Caption = 'Button1'
          TabOrder = 3
          Visible = False
          OnClick = Button1Click
        end
      end
    end
    object Memo1: TMemo
      Left = 600
      Top = 248
      Width = 321
      Height = 65
      Lines.Strings = (
        'Memo1')
      TabOrder = 2
      Visible = False
    end
  end
  object Cds: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 88
    Top = 424
  end
  object WinMessage: TQBWinMessages
    OnMsgArrived = WinMessageMsgArrived
    Left = 41
    Top = 426
  end
  object dba: TRemoteUniDac
    TaskTimeout = 30
    EnableBCD = False
    EnableFMTBCD = False
    EnableLoadBalance = False
    Left = 176
    Top = 398
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 744
    Top = 344
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clMenuBar
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object stHigh: TcxStyle
      AssignedValues = [svColor]
      Color = clRed
    end
    object stLow: TcxStyle
      AssignedValues = [svColor]
      Color = clMoneyGreen
    end
    object stXiaoshu: TcxStyle
      AssignedValues = [svColor]
      Color = clYellow
    end
  end
end

object WorkForm: TWorkForm
  Left = 309
  Top = 196
  Align = alClient
  AutoSize = True
  BorderStyle = bsNone
  Caption = 'DLL'#20013#30340'Work'#31383#20307
  ClientHeight = 589
  ClientWidth = 1031
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object MasterP: TPanel
    Left = 0
    Top = 0
    Width = 1031
    Height = 589
    Align = alClient
    FullRepaint = False
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1029
      Height = 104
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Bevel2: TBevel
        Left = 0
        Top = 99
        Width = 1029
        Height = 5
        Align = alBottom
        Shape = bsBottomLine
        Style = bsRaised
        ExplicitLeft = 168
        ExplicitTop = 88
        ExplicitWidth = 50
      end
      object Label1: TLabel
        Left = 16
        Top = 66
        Width = 498
        Height = 12
        Caption = #26597#35810#26102#38388'  '#20174'                   '#21040'                    '#25152#26377#21333#25454#22343#20197#24050#23457#26680#24050#36807#36134#20026#26377#25928
      end
      object xmLE: TLabeledEdit
        Left = 88
        Top = 17
        Width = 106
        Height = 20
        EditLabel.Width = 60
        EditLabel.Height = 12
        EditLabel.Caption = #23458#25143#32534#21495#65306
        LabelPosition = lpLeft
        LabelSpacing = 5
        TabOrder = 0
      end
      object beginT: TDateTimePicker
        Left = 95
        Top = 62
        Width = 99
        Height = 21
        Date = 42866.679591331020000000
        Time = 42866.679591331020000000
        TabOrder = 1
      end
      object endT: TDateTimePicker
        Left = 224
        Top = 62
        Width = 97
        Height = 21
        Date = 42866.679591331020000000
        Time = 42866.679591331020000000
        TabOrder = 2
        OnClick = endTClick
        OnExit = endTExit
      end
      object RunQuery: TButton
        Left = 783
        Top = 15
        Width = 93
        Height = 78
        Caption = #26597'  '#35810
        TabOrder = 3
        OnClick = RunQueryClick
      end
      object Toxls: TButton
        Left = 882
        Top = 13
        Width = 100
        Height = 37
        Caption = #20449#24687#23548#20986
        TabOrder = 4
        OnClick = ToxlsClick
      end
      object ztxz: TRadioGroup
        Left = 609
        Top = 7
        Width = 168
        Height = 86
        Caption = #36830#25509#36134#22871
        ItemIndex = 0
        Items.Strings = (
          #39640#28201#20844#21496
          #29616#22330#27979#35797#36134#22871)
        TabOrder = 5
        Visible = False
      end
      object TimeP: TPanel
        Left = 336
        Top = 52
        Width = 267
        Height = 41
        BevelOuter = bvSpace
        BorderStyle = bsSingle
        TabOrder = 6
        object Label2: TLabel
          Left = 16
          Top = 12
          Width = 144
          Height = 12
          Caption = #26680#31639#24180#24230'            '#26376#20221
        end
        object qbYear: TEdit
          Left = 75
          Top = 9
          Width = 49
          Height = 20
          NumbersOnly = True
          TabOrder = 0
          Text = 'qbYear'
        end
        object qbmon: TComboBox
          Left = 172
          Top = 9
          Width = 77
          Height = 22
          Style = csOwnerDrawFixed
          ItemIndex = 0
          TabOrder = 1
          Text = #19968#26376
          Items.Strings = (
            #19968#26376
            #20108#26376
            #19977#26376
            #22235#26376
            #20116#26376
            #20845#26376
            #19971#26376
            #20843#26376
            #20061#26376
            #21313#26376
            #21313#19968#26376
            #21313#20108#26376)
        end
      end
      object ToXlsMX: TButton
        Left = 882
        Top = 56
        Width = 100
        Height = 37
        Caption = #26410#20351#29992
        TabOrder = 7
        Visible = False
      end
      object pnl4: TPanel
        Left = 224
        Top = 9
        Width = 290
        Height = 41
        BevelOuter = bvNone
        TabOrder = 8
        object Q_ima01: TLabeledEdit
          Left = 144
          Top = 8
          Width = 121
          Height = 20
          CharCase = ecUpperCase
          EditLabel.Width = 138
          EditLabel.Height = 12
          EditLabel.Caption = #26009#20214#32534#21495#65306'             '
          LabelPosition = lpLeft
          TabOrder = 0
        end
        object cbb1: TComboBox
          Left = 65
          Top = 8
          Width = 65
          Height = 20
          ItemIndex = 0
          TabOrder = 1
          Text = #31561#20110
          Items.Strings = (
            #31561#20110
            #21253#21547
            #24320#22836#20026
            #32467#23614#20026)
        end
      end
    end
    object MGrid: TAdvStringGrid
      Left = 1
      Top = 105
      Width = 1029
      Height = 483
      Cursor = crDefault
      Align = alClient
      ColCount = 16
      DrawingStyle = gdsClassic
      FixedRows = 2
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssBoth
      TabOrder = 1
      HoverRowCells = [hcNormal, hcSelected]
      OnGetCellColor = MGridGetCellColor
      OnGetAlignment = MGridGetAlignment
      ActiveCellFont.Charset = DEFAULT_CHARSET
      ActiveCellFont.Color = clWindowText
      ActiveCellFont.Height = -11
      ActiveCellFont.Name = 'Tahoma'
      ActiveCellFont.Style = [fsBold]
      ControlLook.FixedGradientHoverFrom = clGray
      ControlLook.FixedGradientHoverTo = clWhite
      ControlLook.FixedGradientDownFrom = clGray
      ControlLook.FixedGradientDownTo = clSilver
      ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownHeader.Font.Color = clWindowText
      ControlLook.DropDownHeader.Font.Height = -11
      ControlLook.DropDownHeader.Font.Name = 'Tahoma'
      ControlLook.DropDownHeader.Font.Style = []
      ControlLook.DropDownHeader.Visible = True
      ControlLook.DropDownHeader.Buttons = <>
      ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownFooter.Font.Color = clWindowText
      ControlLook.DropDownFooter.Font.Height = -11
      ControlLook.DropDownFooter.Font.Name = 'Tahoma'
      ControlLook.DropDownFooter.Font.Style = []
      ControlLook.DropDownFooter.Visible = True
      ControlLook.DropDownFooter.Buttons = <>
      Filter = <>
      FilterDropDown.Font.Charset = DEFAULT_CHARSET
      FilterDropDown.Font.Color = clWindowText
      FilterDropDown.Font.Height = -11
      FilterDropDown.Font.Name = 'Tahoma'
      FilterDropDown.Font.Style = []
      FilterDropDown.TextChecked = 'Checked'
      FilterDropDown.TextUnChecked = 'Unchecked'
      FilterDropDownClear = '(All)'
      FilterEdit.TypeNames.Strings = (
        'Starts with'
        'Ends with'
        'Contains'
        'Not contains'
        'Equal'
        'Not equal'
        'Larger than'
        'Smaller than'
        'Clear')
      FixedRowHeight = 22
      FixedFont.Charset = DEFAULT_CHARSET
      FixedFont.Color = clWindowText
      FixedFont.Height = -11
      FixedFont.Name = 'Tahoma'
      FixedFont.Style = [fsBold]
      FloatFormat = '%.2f'
      HoverButtons.Buttons = <>
      HoverButtons.Position = hbLeftFromColumnLeft
      HTMLSettings.ImageFolder = 'images'
      HTMLSettings.ImageBaseName = 'img'
      PrintSettings.DateFormat = 'dd/mm/yyyy'
      PrintSettings.Font.Charset = DEFAULT_CHARSET
      PrintSettings.Font.Color = clWindowText
      PrintSettings.Font.Height = -11
      PrintSettings.Font.Name = 'Tahoma'
      PrintSettings.Font.Style = []
      PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
      PrintSettings.FixedFont.Color = clWindowText
      PrintSettings.FixedFont.Height = -11
      PrintSettings.FixedFont.Name = 'Tahoma'
      PrintSettings.FixedFont.Style = []
      PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
      PrintSettings.HeaderFont.Color = clWindowText
      PrintSettings.HeaderFont.Height = -11
      PrintSettings.HeaderFont.Name = 'Tahoma'
      PrintSettings.HeaderFont.Style = []
      PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
      PrintSettings.FooterFont.Color = clWindowText
      PrintSettings.FooterFont.Height = -11
      PrintSettings.FooterFont.Name = 'Tahoma'
      PrintSettings.FooterFont.Style = []
      PrintSettings.PageNumSep = '/'
      ScrollWidth = 16
      SearchFooter.FindNextCaption = 'Find &next'
      SearchFooter.FindPrevCaption = 'Find &previous'
      SearchFooter.Font.Charset = DEFAULT_CHARSET
      SearchFooter.Font.Color = clWindowText
      SearchFooter.Font.Height = -11
      SearchFooter.Font.Name = 'Tahoma'
      SearchFooter.Font.Style = []
      SearchFooter.HighLightCaption = 'Highlight'
      SearchFooter.HintClose = 'Close'
      SearchFooter.HintFindNext = 'Find next occurrence'
      SearchFooter.HintFindPrev = 'Find previous occurrence'
      SearchFooter.HintHighlight = 'Highlight occurrences'
      SearchFooter.MatchCaseCaption = 'Match case'
      SortSettings.DefaultFormat = ssAutomatic
      SortSettings.Column = 0
      Version = '8.1.3.0'
      WordWrap = False
      ExplicitTop = 111
      ColWidths = (
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64)
      RowHeights = (
        22
        22
        22
        22
        22
        22
        22
        22
        22
        22)
    end
  end
  object Memo1: TMemo
    Left = 402
    Top = 256
    Width = 327
    Height = 193
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
    Visible = False
  end
  object Cds: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 80
    Top = 424
  end
  object WinMessage: TQBWinMessages
    OnMsgArrived = WinMessageMsgArrived
    Left = 41
    Top = 426
  end
  object dba: TRemoteUniDac
    TaskTimeout = 30
    EnableBCD = False
    EnableFMTBCD = False
    EnableLoadBalance = False
    Left = 176
    Top = 398
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 744
    Top = 344
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clMenuBar
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
  end
  object PrivilegeCDS: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 856
    Top = 240
  end
  object agexp: TAdvGridExcelIO
    AdvStringGrid = MGrid
    Options.ExportOverwriteMessage = 'File %s already exists'#13'Ok to overwrite ?'
    Options.ExportRawRTF = False
    UseUnicode = False
    GridStartRow = 0
    GridStartCol = 0
    Version = '3.13'
    Left = 708
    Top = 484
  end
end

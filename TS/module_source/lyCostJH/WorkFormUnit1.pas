﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd, Vcl.dialogs, strUtils,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj;

type
  TWorkForm = class(TForm)
    Cds: TClientDataSet;
    MasterP: TPanel;
    Panel3: TPanel;
    Bevel2: TBevel;
    qxm: TImage;
    Label1: TLabel;
    xmLE: TLabeledEdit;
    beginT: TDateTimePicker;
    endT: TDateTimePicker;
    RunQuery: TButton;
    Toxls: TButton;
    ztxz: TRadioGroup;
    TimeP: TPanel;
    Label2: TLabel;
    qbYear: TEdit;
    qbmon: TComboBox;
    ToXlsMX: TButton;
    MGrid: TAdvStringGrid;
    xmF: TPanel;
    Panel5: TPanel;
    Bevel3: TBevel;
    xmsa: TButton;
    xmcl: TButton;
    xmok: TButton;
    xmca: TButton;
    xmcb: TAdvStringGrid;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    cxxz: TRadioGroup;
    PrivilegeCDS: TClientDataSet;
    Memo1: TMemo;
    Tljcost: TButton;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure ztxzClick(Sender: TObject);
    procedure qxmClick(Sender: TObject);
    procedure xmclClick(Sender: TObject);
    procedure xmokClick(Sender: TObject);
    procedure xmsaClick(Sender: TObject);
    procedure xmcaClick(Sender: TObject);
    procedure RunQueryClick(Sender: TObject);
    procedure ToxlsClick(Sender: TObject);
    procedure ToXlsMXClick(Sender: TObject);
    procedure MGridGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure qbmonClick(Sender: TObject);
    procedure qbYearExit(Sender: TObject);
    procedure cxxzClick(Sender: TObject);
    procedure TljcostClick(Sender: TObject);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function XmCount(const zh: string): Integer;
    procedure XmcbClose;
    procedure XmcbOpen;
    procedure SetTimeRange;
    function XmcbSelectCount: Integer;
    function RunCDS(SQL: string): boolean;
    function GetFilterS: string;
    procedure FillmGrid(Q: TClientDataSet);
    procedure MGridColTitle(I: Integer);
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure WaitMessage;
    procedure GetPrivilege(TFlag, TID, UID: string);
    procedure GridToxls;
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'LYCostJH_DLL';
  DChar = ','; // 分隔符
  SPCHAR = '-';
  DefaultZT = 'GWGS';
  KeyCol = 11; // 在GRID中用于区别汇总与明细的列
  // Grid列表标题
  XCCC = 9; // 现场库存标题列数

var
  zhanghu: string;
  CurrentZT: string;
  softWareAuth: boolean;
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  MessageFlag: boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.WaitMessage;
begin
  While True do
  Begin
    application.ProcessMessages; // 读取消息队列
    If Not MessageFlag Then
      Break;
  End;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');
  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');
  //
  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage; // 同步处理消息

  // 界面初始化
  // 默认为当天
  endT.Date := now;
  beginT.Date := now;

  // 默认期别 年度为本年 月份为本月
  qbYear.Text := inttostr(YearOf(now));
  qbmon.ItemIndex := MonthOf(now) - 1;

  // 期别显示(默认不显示,只有当项目编号为单选时才会出现)
  // TimeP.Visible := false;

  // 获取是否可修改项目编号权限,设置xeLE的只读性
  GetPrivilege('ASSERT', '1', zhanghu);
  WaitMessage;
  if PrivilegeCDS.RecordCount = 0 then
    xmLE.ReadOnly := True
  else
    xmLE.ReadOnly := PrivilegeCDS.FieldByName('R1').AsString = '0';
end;

// -----------------------------------------------------------------------------
// 业务代码
Const
  XCCN = ',项目编号,年度,月份,项次,料件,单位,来源公司,标准成本';

  // 查询用户负责的项目编号集
  QueryXmSQL = 'SELECT gem01,gem02 FROM %sgem_file where ta_gem32 = ''%s''';
  // 查询主SQL
  MSQL = 'SELECT  TC_PJX09,TC_PJX02,TC_PJX03,R,TC_PJX06,TC_PJX11,lY,StdCost  FROM '
    + sLinebreak + '(' + sLinebreak +
    'SELECT R,TC_PJX09,TC_PJX02,TC_PJX03,TC_PJX06,TC_PJX11,lY, ' + sLinebreak +
    '(CASE LY WHEN ''YKGS'' THEN ts3.gcost  ' + sLinebreak +
    '                         WHEN ''GNGS'' THEN ts2.gcost  ' + sLinebreak +
    '                           ELSE ts1.gcost END) stdCOST  ' + sLinebreak +
    '                              FROM                         ' + sLinebreak +
    '(   ' + sLinebreak +
    'SELECT row_number()over(partition by tc_pjx09 ORDER BY ROWID) R,TC_PJX09,TC_PJX02,TC_PJX03,TC_PJX06,TC_PJX11,TC_PJX12,'
    + sLinebreak +
    'SUBSTR(TC_PJX12,1,INSTR(TC_PJX12,''-'',1) - 1) LY  FROM TC_PJX_FILE ' +
    sLinebreak + 'WHERE TC_PJX02 = %d AND TC_PJX03 = %d AND tc_pjx09 IN (%s) ' +
    sLinebreak + ') TT  ' + sLinebreak +
    'LEFT JOIN (SELECT tc_cua00,tc_cua01,tc_cua02,SUM(tc_cua07) gCost ' +
    ' FROM gwgs.tc_cua_file WHERE tc_cua00 = %0:d ' +
    ' GROUP BY tc_cua00,tc_cua01,tc_cua02) ts1 ON ts1.tc_cua02 = TC_PJX06 ' +
    sLinebreak +
    'LEFT JOIN (SELECT tc_cua00,tc_cua01,tc_cua02,SUM(tc_cua07) gCost ' +
    'FROM gngs.tc_cua_file WHERE tc_cua00 = %0:d ' +
    'GROUP BY tc_cua00,tc_cua01,tc_cua02) ts2 ON ts2.tc_cua02 = TC_PJX06 ' +
    sLinebreak +
    'LEFT JOIN (SELECT tc_cua00,tc_cua01,tc_cua02,SUM(tc_cua07) gCost ' +
    'FROM ykgs.tc_cua_file WHERE tc_cua00 = %0:d ' +
    'GROUP BY tc_cua00,tc_cua01,tc_cua02) ts3 ON ts3.tc_cua02 = TC_PJX06  ' +
    sLinebreak + ') ' + sLinebreak +
  // 过滤条件：没有标准成本的项次
    'WHERE stdCost IS NULL ' + sLinebreak +
    'ORDER BY tc_pjx09,R  ';

var
  xmnF: string; // 项目编号
  bmonth, bYear: word; // 过滤条件中使用到的年与月份

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

function TWorkForm.XmCount(const zh: string): Integer;
// 返回账号所负责的项目数目
var
  I: Integer;
  SQL: string;
begin
  if zh = '' then
    exit(0);
  // 查询返回项目数
  if CurrentZT = '' then
    CurrentZT := DefaultZT;

  SQL := Format(QueryXmSQL, [CurrentZT + '.', zh]);
{$IFDEF DEBUG}LogMsg('LYCOSTJH_GetXM_SQL:' + SQL); {$ENDIF}
  if Not RunCDS(SQL) then
    exit(0);

  if Cds.RecordCount = 0 then
    exit(0);

  dba.ReadDataset(SQL, Cds);
  if Cds.RecordCount = 0 then
    exit(0);

  xmcb.FixedCols := 0;
  xmcb.ColWidths[0] := 20;
  xmcb.Options := xmcb.Options + [goRowSelect, goEditing];
  xmcb.ShowSelection := false;

  // 在GRID中显示项目
  xmcb.RowCount := Cds.RecordCount + 1;
  Cds.First;
  I := 1;
  while not Cds.Eof do
  begin
    xmcb.AddCheckBox(0, I, false, false);
    xmcb.Cells[1, I] := Cds.Fields.Fields[0].AsString;
    xmcb.Cells[2, I] := Cds.Fields.Fields[1].AsString;
    inc(I);
    Cds.Next;
  end;
  Result := Cds.RecordCount;
end;

procedure TWorkForm.xmokClick(Sender: TObject);
// 确定返回的选中项目号写入项目编号中。若全不选中，则弹出出错框。若全选，则筛选条件不包括项目编号条件
var
  I: Integer;
  State: boolean;
  Value: TstringList;
begin
  Value := TstringList.Create;

  for I := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.GetCheckBoxState(0, I, State);
    if State then
      Value.Append(xmcb.Cells[1, I]);
  end;
  if Value.Count = 0 then
    if MessageBox(self.Handle, '没有选中任何项目，是否要关闭？', '提示',
      MB_ICONINFORMATION + MB_OkCancel) = idCancel then
    begin
      Value.Free;
      exit;
    end;

  xmLE.Text := Value.DelimitedText; // 返回项目统计选择项
  Value.Free;
  XmcbClose;
end;

procedure TWorkForm.xmsaClick(Sender: TObject);
var
  I: Integer;
begin
  // 设置所有项目编号全选中
  for I := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.SetCheckBoxState(0, I, True);
    xmcb.RowColor[I] := xmcb.SelectionColor;
  end;
end;

procedure TWorkForm.ztxzClick(Sender: TObject);
var
  s: string;
begin
  // 切换当前选择项，获取当前账套
  case ztxz.ItemIndex of
    1:
      CurrentZT := 'XCXD' // 现场测试账套
  else
    CurrentZT := DefaultZT;
  end;
  // 获取新的项目编号
  xmF.Visible := false;
  TimeP.Visible := false;
  s := xmcb.ColumnHeaders.DelimitedText;
  xmcb.ClearAll;
  xmcb.ColumnHeaders.DelimitedText := s;
  if XmCount(zhanghu) = 0 then
    application.MessageBox(Pchar('当前账套下没有账户：' + zhanghu + ' 所负责的项目!'), '提示');

  // 界面初始化
  xmLE.Text := '';

  s := MGrid.ColumnHeaders.DelimitedText;
  MGrid.ClearAll;
  MGrid.ColumnHeaders.DelimitedText := s;

  self.Caption := '标准成本校核 易拓账号: ' + zhanghu + '  当前账套： ' + ztxz.Items
    [ztxz.ItemIndex];
end;

procedure TWorkForm.XmcbOpen;
// 打开项目编号选择
begin
  MasterP.Enabled := false;
  xmF.Visible := True;
end;

procedure TWorkForm.SetTimeRange;
// 根据不同条件,对收货筛选时间进行自动化处理
begin
  // 与其他dll中的settimeRange不同，本函数作用是获取查询的耗量清单的期别，比其他dll中的功能要少很多
  bYear := strtointdef((qbYear.Text), YearOf(now));
  bmonth := qbmon.ItemIndex + 1;
end;

procedure TWorkForm.XmcbClose;
// 关闭项目编号选择
begin
  xmF.Visible := false;
  MasterP.Enabled := True;
  // 当选中项为1时显示期间选择
  TimeP.Visible := XmcbSelectCount = 1;
  if TimeP.Visible then
    SetTimeRange;
  ResizeForm;
end;

function TWorkForm.XmcbSelectCount: Integer;
// 返回项目编号选择数目
var
  I: Integer;
  State: boolean;
begin
  Result := 0;
  for I := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.GetCheckBoxState(0, I, State);
    if State then
      inc(Result);
  end;
end;

function TWorkForm.RunCDS(SQL: string): boolean;
begin
  Result := false;
  Cds.close;
  if not dba.ReadDataset(SQL, Cds) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    Cds.active := True;
    Result := True;
  end;
end;

procedure TWorkForm.qbmonClick(Sender: TObject);
begin
  SetTimeRange;
end;

procedure TWorkForm.qbYearExit(Sender: TObject);
begin
  SetTimeRange;
end;

procedure TWorkForm.qxmClick(Sender: TObject);
begin
  // 返回项目编号选择,选择前后MasterP大小发生变化不知为何，重新定义其大小
  XmcbOpen;
  ResizeForm;
end;

procedure TWorkForm.xmcaClick(Sender: TObject);
begin
  // 直接退出，设置xmf不可见。保持原筛选条件
  if XmcbSelectCount = 0 then
  begin
    if MessageBox(self.Handle, '没有选中任何项目，是否要关闭？', '提示',
      MB_ICONINFORMATION + MB_OkCancel) = idCancel then
      exit;
  end;
  XmcbClose;
end;

procedure TWorkForm.xmclClick(Sender: TObject);
var
  I: Integer;
begin
  // 设置所有项目编号全不选中
  for I := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.SetCheckBoxState(0, I, false);
    xmcb.RowColor[I] := xmcb.Color;
  end;
end;

function TWorkForm.GetFilterS: string;
// 返回动态筛选条件
var
  FS: TstringList;
  I: Integer;
begin
  Result := '';
  if length(xmLE.Text) < 5 then
  begin
    exit('不是有效的项目编号');
  end;
  if xmLE.Text = '' then
  begin
    exit('项目编号不能为空');
  end;
  FS := TstringList.Create;
  FS.DelimitedText := xmLE.Text;
  for I := 0 to FS.Count - 1 do
    FS[I] := '''' + FS[I] + '''';
  xmnF := FS.DelimitedText;
  FS.Free;

  SetTimeRange;
end;

procedure TWorkForm.RunQueryClick(Sender: TObject);
// 查寻
var
  filter, SQL: string;
begin
  if Not softWareAuth then
  begin
    Randomize;
    if (Random(1000) mod 10 > 0) then
      exit;
  end;

  // 主查询
  screen.Cursor := crSQLWait;
  filter := GetFilterS;
  if filter <> '' then
  begin
    screen.Cursor := crDefault;
    application.MessageBox(Pchar(filter), '提示');
    exit;
  end;
  try
    SQL := Format(MSQL, [bYear, bmonth, xmnF]);

    // Memo1.Lines.Text := SQL;
    // exit;
    RunCDS(SQL);

    if Cds.RecordCount = 0 then
      application.MessageBox('所有的耗量清单均已有标准成本或没有生成耗量清单！', '提示')
    else
      FillmGrid(Cds);
    ResizeForm;
  finally
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.GridToxls;
var
  OpenDlg: TOpenDialog;
  strFileName: string;
begin
  OpenDlg := TOpenDialog.Create(nil);
  try
    OpenDlg.filter := 'EXCLE(*.xlsx)|*.xlsx';
    OpenDlg.DefaultExt := '*.xlsx';
    if OpenDlg.Execute then
    begin
      strFileName := Trim(OpenDlg.FileName);
      if strFileName <> '' then
      begin
        MGrid.SaveToXLS(strFileName);
        application.MessageBox(Pchar('转换完成'), '提示');
      end;
    end;
  finally
    FreeAndNil(OpenDlg);
  end;
end;

procedure TWorkForm.TljcostClick(Sender: TObject);
const
  Fsql = ' VALUES(''%s'',''1'',''%s'','' '','' '','' '',''%1:s'',0,NULL,0,0,''%1:s'','
    + '''1'',NULL,'' '',''%s'',''%s'',''%s'',NULL,''%4:s'',' +
    'SYSDATE,''%3:s'',''%3:s'',''%5:s'')';
  ver = '1'; // 版本号
  gwcw = '0106'; // 部门全为财务部
  gncw = '012809';
  ykcw = '012905';
var
  I: Integer;
  s, SQL: string;
  LJT, bmT, dwT, lyT: TstringList;
  Years: word; // 年份
  zhu: string; // 账号ID
label ET;
begin
  // 对没有成本的转发产品进行标准成本赋值
  // 20180726 目前赋值逻辑为：对所有转发产品标准产品全按0处理
  if Cds.RecordCount = 0 then
    exit;
  LJT := TstringList.Create;
  bmT := TstringList.Create;
  dwT := TstringList.Create;
  lyT := TstringList.Create;

  CDS.First;
  for I := 0 to Cds.RecordCount - 1 do
  begin
    s := Cds.FieldByName('tc_pjx06').AsString;
    if RightStr(s, 1) = 'T' then
    begin
      // 料件赋值
      LJT.Append(s);
      // 部门赋值
      s := Cds.FieldByName('LY').AsString;
      if s = 'GWGS' then
        bmT.Append(gwcw);
      if s = 'GNGS' then
        bmT.Append(gncw);
      if s = 'YKGS' then
        bmT.Append(ykcw);

      // 来源公司赋值
      lyT.Append(Cds.FieldByName('LY').AsString);
      // 单位赋值
      dwT.Append(Cds.FieldByName('TC_PJX11').AsString);
    end;
    Cds.Next;
  end;
  // 若没有转发产品，直接退出
  if LJT.Count = 0 then
    goto ET;
  // 通用字段值赋值
  SQL := 'INSERT ALL ' + sLinebreak;
  Years := bYear;
  zhu := zhanghu;
  // 组装sql
  for I := 0 to LJT.Count - 1 do
  begin
    s := ' INTO ' + lyT[I] + '.TC_CUA_FILE ' +
      Format(Fsql, [Years.ToString, LJT[I], dwT[I], bmT[I], zhu, lyT[I]]) +
      sLinebreak;
    SQL := SQL + s;
  end;
  SQL := SQL + ' SELECT 1 FROM DUAL';
//  Memo1.Lines.Text := SQL;
//  exit;
//执行数据库操作
if dba.ExecuteSql(SQL,false,s) then
   s := '正常给 ' + s + ' 条转发产品赋标准成本'
   else
   s := '转发产品赋标准成本失败,请重试';
   showmessage(s);
ET:
  lyT.Free;
  dwT.Free;
  bmT.Free;
  LJT.Free;

end;

procedure TWorkForm.ToxlsClick(Sender: TObject);
begin
  GridToxls;
end;

procedure TWorkForm.ToXlsMXClick(Sender: TObject);
begin
  GridToxls;
end;

procedure TWorkForm.MGridGetCellColor(Sender: TObject; ARow, ACol: Integer;
  AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
// 主GRID中汇总行字显示为红色，奇偶行区分开
begin
  if MGrid.Cells[KeyCol, ARow] = SPCHAR then
    AFont.Color := clRed;

  if (ARow mod 2) = 0 then
    ABrush.Color := MGrid.Color
  else
    ABrush.Color := clBtnFace;
end;

procedure TWorkForm.MGridColTitle(I: Integer);
begin
  MGrid.ClearAll;
  MGrid.ColCount := XCCC;
  MGrid.ColumnHeaders.DelimitedText := XCCN;
end;

procedure TWorkForm.cxxzClick(Sender: TObject);
begin
  MGridColTitle(cxxz.ItemIndex);
  case cxxz.ItemIndex of
    0:
      Toxls.Enabled := True;
    1:
      Toxls.Enabled := false;
  end;
  ToXlsMX.Enabled := Not Toxls.Enabled;
end;

procedure TWorkForm.FillmGrid(Q: TClientDataSet);
var
  I, j: Integer;
  s: string;
begin
  // 清空GRID
  s := MGrid.ColumnHeaders.DelimitedText;
  MGrid.ClearAll;
  MGrid.ColumnHeaders.DelimitedText := s;
  MGrid.ColumnHeaders[XCCC - 1] := inttostr(bYear) + '年标准成本';

  MGrid.BeginUpdate;
  if Q.RecordCount = 0 then
    exit;
  with MGrid do
  begin
    MGrid.RowCount := Q.RecordCount + 1;
    I := 1;
    Q.First;
    while not Q.Eof do
    begin
      MGrid.Cells[0, I] := I.ToString;
      for j := 0 to Q.FieldCount - 1 do
      begin
        MGrid.Cells[j + 1, I] := Q.Fields.Fields[j].AsString;
      end;
      inc(I);
      Q.Next;
    end;
    row := 1;
    Col := 0;
  end;
  MGrid.AutoSizeColumns(false, 5);
  MGrid.EndUpdate;
  // MGrid.ContractAll;
end;
// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}

// 消息响应事件
// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息
  if (StrComp(Pchar(MsgName), Pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      zhanghu := InParcel.GetStringGoods('LoginUser');
      XmCount(zhanghu);
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(Pchar(MsgName), Pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
  // 获取当前插件的权限信息 同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', PrivilegeCDS);
    end;
    exit;
  end;
end;

{$ENDREGION}

end.

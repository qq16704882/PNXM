﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, cxStyles, cxClasses,
  tmsAdvGridExcel;

type
  TWorkForm = class(TForm)
    Cds: TClientDataSet;
    MasterP: TPanel;
    Panel3: TPanel;
    Bevel2: TBevel;
    qxm: TImage;
    Label1: TLabel;
    xmLE: TLabeledEdit;
    beginT: TDateTimePicker;
    endT: TDateTimePicker;
    RunQuery: TButton;
    Toxls: TButton;
    ztxz: TRadioGroup;
    TimeP: TPanel;
    Label2: TLabel;
    qbYear: TEdit;
    qbmon: TComboBox;
    ToXlsMX: TButton;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    PrivilegeCDS: TClientDataSet;
    Memo1: TMemo;
    MGrid: TAdvStringGrid;
    agexp: TAdvGridExcelIO;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure RunQueryClick(Sender: TObject);
    procedure ToxlsClick(Sender: TObject);
    procedure MGridGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure endTExit(Sender: TObject);
    procedure endTClick(Sender: TObject);
    procedure MGridGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function RunCDS(SQL: string): Boolean;
    function GetFilterS: string;
    procedure FillmGrid(Q: TClientDataSet; tp: Integer = 1);
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure GetPrivilege(TFlag, TID, UID: string);
    procedure WaitMessage;
    procedure FillGridHeader;
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'CBJKCRK_DLL';
  DChar = ','; // 分隔符
  SPCHAR = '-';
  DefaultZT = 'GWGS';

var
  zhanghu: string;
  CurrentZT: string;
  softWareAuth: Boolean;
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  MessageFlag: Boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.WaitMessage;
begin
  While True do
  Begin
    application.ProcessMessages; // 读取消息队列
    If Not MessageFlag Then
      Break;
  End;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');

  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');
  // 获取插件formCaption
  self.Caption := InputParcel.GetAnsiStringGoods('FormCaption');
  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage; // 同步处理消息

  // 界面初始化
  // 默认为当天
  endT.Date := now;
  beginT.Date := now - 28;

  // 默认期别 年度为本年 月份为本月
  qbYear.Text := inttostr(YearOf(now));
  qbmon.ItemIndex := MonthOf(now) - 1;

  // 期别显示(默认不显示,只有当项目编号为单选时才会出现)
  TimeP.Visible := false;

  FillGridHeader;
end;

// -----------------------------------------------------------------------------
// 业务代码
Const

  QueryXmSQL = 'SELECT gem01,gem02 FROM %sgem_file where ta_gem32 = ''%s''';

  SSQL = '  SELECT imm14,gem02,gen02,gea02,week,tm,T FROM  ' + slineBreak +
    '(                                                ' + slineBreak +
    'SELECT imm14,to_char(imm17,''YYYY/WW'') week,MAX(imm17) Tm,''L'' AS T ' +
    ' FROM imm_file WHERE regexp_like(imm01,''^CL01'') AND ' + slineBreak +
    ' immconf = ''Y'' and imm03 = ''Y'' ' + slineBreak +
    ' AND %s AND  %s GROUP BY imm14,to_char(imm17,''YYYY/WW'')' + slineBreak +
    'UNION                                                     ' + slineBreak +
    'SELECT imm14,to_char(imm12,''YYYY/WW'') week,MAX(imm12) Tm,''S'' AS T ' +
    ' FROM imm_file WHERE regexp_like(imm01,''^SH'') AND ' + slineBreak +
    '  imm03 = ''Y'' ' + slineBreak + ' AND %0:s AND       ' + slineBreak +
    ' immud03 = ''1'' AND %2:s ' + ' GROUP BY imm14,to_char(imm12,''YYYY/WW'') '
    + slineBreak + ') aa                              ' + slineBreak +
    'LEFT JOIN gem_file a ON a.gem01 = imm14   ' + slineBreak +
    'LEFT JOIN occ_file b ON b.occ01 = a.ta_gem22   ' + slineBreak +
    'LEFT JOIN gea_file c ON c.gea01 = b.occ20      ' + slineBreak +
    'LEFT JOIN gen_file d ON d.gen01 = b.occ04      ' + slineBreak +
    'ORDER BY imm14,week  DESC                        ';

  headStr: array [0 .. 6] of string = ('序号', '项目编号', '项目名称', '业务经理', '客户分区',
    '入库次数', '出库次数');
  WeekNum = 4;

var
  xmNF, zqTimeF, shTimeF: string; // 项目,时间的过滤条件
  qyear, qmonth, byear, bmonth: Integer;

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

function TWorkForm.RunCDS(SQL: string): Boolean;
begin
  Result := false;
  Cds.close;
  if not dba.ReadDataset(SQL, Cds) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    Cds.active := True;
    Result := True;
  end;
end;

function TWorkForm.GetFilterS: string;
var
  bt, et: Tdate;
begin
  Result := '';

  if xmLE.Text = '' then
  begin
    xmNF := ' 1=1 ';
  end
  else
  begin
    xmNF := StringReplace(trim(xmLE.Text), DChar, '''' + DChar + '''',
      [rfReplaceAll]);
    xmNF := ' imm14 in (''' + xmNF + ''') ';
  end;

  bt := beginT.Date;
  et := endT.Date;

  if et < bt then
  begin
    exit('Error : 结束日期不可小于开始时间！');
  end;

  zqTimeF := ' imm17 BETWEEN to_date(''' + FormatDateTime('yyyymmdd', bt) +
    ''',''yyyymmdd'') AND ' + ' to_date(''' + FormatDateTime('yyyymmdd', et) +
    ''',''yyyymmdd'')';

  shTimeF := ' imm12 BETWEEN to_date(''' + FormatDateTime('yyyymmdd', bt) +
    ''',''yyyymmdd'') AND ' + ' to_date(''' + FormatDateTime('yyyymmdd', et) +
    ''',''yyyymmdd'')';
end;

procedure TWorkForm.RunQueryClick(Sender: TObject);
var
  LJBH: string;
  SQL: string;
  E: string;
begin
  if Not softWareAuth then
  begin
    Randomize;
    if (Random(1000) mod 10 > 0) then
      exit;
  end;
  // 主查询
  MGrid.RowCount := 2;
  screen.Cursor := crSQLWait;
  E := GetFilterS;
  try
    if E <> '' then
    begin
      screen.Cursor := crDefault;
      application.MessageBox(Pchar(E), '提示');
      exit;
    end;
    SQL := Format(SSQL, [xmNF, zqTimeF, shTimeF]);
    // memo1.Text := sql;
    // exit;
    RunCDS(SQL);
    if Cds.RecordCount > 0 then
      FillmGrid(Cds);
  finally
    ResizeForm;
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.ToxlsClick(Sender: TObject);
var
  SaveDialog: TSaveDialog;
begin
  SaveDialog := TSaveDialog.Create(nil);
  with SaveDialog do
  begin
    filter := '*.xls|*.xls';
    if Execute then
    begin
      agexp.XLSExport(SaveDialog.FileName + '.xls');
      showmessage('导入成功！');
    end;
  end;
  SaveDialog.Free;
end;

procedure TWorkForm.FillmGrid(Q: TClientDataSet; tp: Integer = 1);
var
  i, w, wc, R: Integer;
  vi, vo: Integer; // 有效出入库次数
  xm, Ts: string;
  TimeS: string;
  xmList, wkList, weekList: TstringList;
begin
  weekList := TstringList.Create;
  weekList.Sorted := True;
  weekList.Duplicates := dupAccept;
  xmList := TstringList.Create;
  xmList.Sorted := True;
  xmList.Duplicates := dupIgnore;
  wkList := TstringList.Create;
  wkList.Sorted := True;
  wkList.Duplicates := dupIgnore;
  Q.First;
  while not Q.Eof do
  begin
    xmList.Add(Q.FieldByName('imm14').AsString);
    wkList.Add(Q.FieldByName('week').AsString);
    Q.Next;
  end;
  // // 若周数不够weekNum数，补齐
  weekList.DelimitedText := wkList.DelimitedText;
  wc := weekList.Count;
  for i := wc to WeekNum do
    weekList.Add('-');
  try
    MGrid.BeginUpdate;
    // 填充表头
    MGrid.RowCount := xmList.Count + 2;
    MGrid.FixedRows := 2;
    wc := weekList.Count;
    MGrid.Cells[5, 0] := weekList[wc - 1];
    MGrid.Cells[7, 0] := weekList[wc - 2];
    MGrid.Cells[9, 0] := weekList[wc - 3];
    MGrid.Cells[11, 0] := weekList[wc - 4];
    // 填充数据区
    // Mgrid.RowCount := ;
    xm := '';
    Ts := '';
    i := 2;

    Q.First;
    while not Q.Eof do
    begin
      if xm <> Q.FieldByName('imm14').AsString then
      begin
        vi := 0;
        vo := 0;
        MGrid.Cells[0, i] := inttostr(i - 1);
        xm := Q.FieldByName('imm14').AsString;
        MGrid.Cells[1, i] := Q.FieldByName('imm14').AsString;
        MGrid.Cells[2, i] := Q.FieldByName('gem02').AsString;
        MGrid.Cells[3, i] := Q.FieldByName('gen02').AsString;
        MGrid.Cells[4, i] := Q.FieldByName('gea02').AsString;
        MGrid.Cells[13, i] := '0'; //
        MGrid.Cells[14, i] := '0'; //
        inc(i);
      end;
      TimeS := Q.FieldByName('TM').AsString;
      Ts := Q.FieldByName('week').AsString;
      w := weekList.IndexOf(Ts);
      // 如果是四周以前数据，则直接跳过
      if (wc - 1 - w) >= 4 then
      begin
        Q.Next;
        continue;
      end;
      if Q.FieldByName('T').AsString = 'S' then
        R := 5 + (wc - 1 - w) * 2
      else
        R := 5 + (wc - 1 - w) * 2 + 1;
      // 统计出/入库次数
      if TimeS <> '' then
      begin
        if Q.FieldByName('T').AsString = 'S' then
          inc(vi)
        else
          inc(vo);
      end;
      MGrid.Cells[R, i - 1] := TimeS;
      MGrid.Cells[13, i - 1] := vi.ToString; // 统计入库次数
      MGrid.Cells[14, i - 1] := vo.ToString; // 统计出库次数
      Q.Next;
    end;
    MGrid.AutoSizeColumns(false, 5);
  finally
    MGrid.EndUpdate;
    wkList.Free;
    xmList.Free;
    weekList.Free;
  end;
end;

procedure TWorkForm.endTClick(Sender: TObject);
begin
  beginT.Date := endT.Date - 28;
end;

procedure TWorkForm.endTExit(Sender: TObject);
begin
  beginT.Date := endT.Date - 28;
end;

procedure TWorkForm.FillGridHeader;
begin
  MGrid.MergeCells(0, 0, 1, 2);
  MGrid.MergeCells(1, 0, 1, 2);
  MGrid.MergeCells(2, 0, 1, 2);
  MGrid.MergeCells(3, 0, 1, 2);
  MGrid.MergeCells(4, 0, 1, 2);
  MGrid.MergeCells(5, 0, 2, 1);
  MGrid.MergeCells(7, 0, 2, 1);
  MGrid.MergeCells(9, 0, 2, 1);
  MGrid.MergeCells(11, 0, 2, 1);
  MGrid.Cells[0, 0] := headStr[0];
  MGrid.Cells[1, 0] := headStr[1];
  MGrid.Cells[2, 0] := headStr[2];
  MGrid.Cells[3, 0] := headStr[3];
  MGrid.Cells[4, 0] := headStr[4];
  MGrid.Cells[5, 1] := '入库';
  MGrid.Cells[6, 1] := '出库';
  MGrid.Cells[7, 1] := '入库';
  MGrid.Cells[8, 1] := '出库';
  MGrid.Cells[9, 1] := '入库';
  MGrid.Cells[10, 1] := '出库';
  MGrid.Cells[11, 1] := '入库';
  MGrid.Cells[12, 1] := '出库';
  MGrid.MergeCells(13, 0, 1, 2);
  MGrid.MergeCells(14, 0, 1, 2);
  MGrid.Cells[13, 0] := headStr[5];
  MGrid.Cells[14, 0] := headStr[6];
end;

procedure TWorkForm.MGridGetAlignment(Sender: TObject; ARow, ACol: Integer;
  var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow < 2 then
  begin
    HAlign := taCenter;
    VAlign := vtaCenter;
  end;
end;

procedure TWorkForm.MGridGetCellColor(Sender: TObject; ARow, ACol: Integer;
  AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if ARow > 1 then
  begin
    if (ARow mod 2) = 0 then
      ABrush.Color := MGrid.Color
    else
      ABrush.Color := clBtnFace;
    if (MGrid.Cells[0, ARow] <> '') and (MGrid.Cells[ACol, ARow] = '') then
      ABrush.Color := clYellow;
  end;
end;

// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}
// 消息响应事件

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息  同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      zhanghu := InParcel.GetStringGoods('LoginUser');
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(Pchar(MsgName), Pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
  // 获取当前插件的权限信息 同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', PrivilegeCDS);
    end;
    exit;
  end;
end;

{$ENDREGION}

end.

﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd, qbmisc,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles,  cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridCustomView, cxGrid, cxClasses, math, cxGridCustomPopupMenu,
  cxGridPopupMenu;

type
  TWorkForm = class(TForm)
    Cds: TClientDataSet;
    MasterP: TPanel;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    MGrid: TcxGrid;
    MTV: TcxGridBandedTableView;
    MGridLevel1: TcxGridLevel;
    Panel1: TPanel;
    Panel2: TPanel;
    ztRG: TRadioGroup;
    Label1: TLabel;
    dbbhcb: TComboBox;
    dbmccb: TComboBox;
    RunQuery: TButton;
    ToXLS: TButton;
    Panel3: TPanel;
    stHigh: TcxStyle;
    stLow: TcxStyle;
    Button1: TButton;
    Label2: TLabel;
    bDh: TEdit;
    edh: TEdit;
    Memo1: TMemo;
    MTVColumn1: TcxGridBandedColumn;
    MTVColumn2: TcxGridBandedColumn;
    MTVColumn3: TcxGridBandedColumn;
    MTVColumn4: TcxGridBandedColumn;
    MTVColumn5: TcxGridBandedColumn;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure RunQueryClick(Sender: TObject);
    procedure ToxlsClick(Sender: TObject);
    procedure ToXlsMXClick(Sender: TObject);
    procedure dbbhcbChange(Sender: TObject);
    procedure ztRGClick(Sender: TObject);
    procedure MTVColumn10StylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure Button1Click(Sender: TObject);
    procedure MTVTcxGridDataControllerTcxDataSummaryFooterSummaryItems2GetText
      (Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function RunCDS(SQL: string): Boolean;
    procedure FillmGrid(Q: TClientDataSet);
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure IniCKBH(zt: string);
    procedure IniZtRG(zt: string);
    procedure GetPrivilege(TFlag, TID, UID: string);
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'CWKMHZ';
  testZT = 'GWGS,GNGS';
  sHigh = '高';
  sLow = '低';
  sStd = '正常';
  CDBID = 'TS'; // 设置使用的数据源

var
  zhanghu: string;
  softWareAuth: Boolean;
  kConfusion: int32;
  jf, df: array [0 .. 4] of single;

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.IniCKBH(zt: string);
// 查询选定账套的仓库信息
begin
  if zt = '' then
    exit;
  if Not RunCDS
    (format('SELECT wm_concat(aac01) aac01,wm_concat(aac02) aac02 FROM %s.aac_file WHERE aacacti = ''Y''',
    [zt])) then
    exit;

  dbbhcb.Items.DelimitedText := Cds.FieldByName('aac01').AsString;
  dbmccb.Items.DelimitedText := Cds.FieldByName('aac02').AsString;
  dbbhcb.ItemIndex := 0;
  dbmccb.ItemIndex := 0;
end;

procedure TWorkForm.IniZtRG(zt: string);
// 根据权限设置可以选择的账套，并动态设置itemindex, columns值
begin
  ztRG.Items.DelimitedText := zt;
  ztRG.Columns := ztRG.Items.Count;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');

  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');
  self.Caption := InputParcel.GetAnsiStringGoods('FormCaption');
  // caption记录插件的ID

  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);

  // 界面初始化
end;

// -----------------------------------------------------------------------------
// 业务代码
Const
  MSQL = 'SELECT aba00,km,aag02,jfje,dfje FROM ' + sLineBreak +
    '(                                    ' + sLineBreak +
    'SELECT aba00,km,SUM(jfje) jfje,SUM(dfje) dfje FROM  ' + sLineBreak +
    '(                                                     ' + sLineBreak +
    'SELECT aba00,aba01,SUBSTR(abb03,1,4) km,DECODE(abb06,1,abb07,0) jfje,DECODE(abb06,2,abb07,0) dfje FROM %s.aba_file '
    + sLineBreak + 'LEFT JOIN %0:s.abb_file ON aba01 = abb01     ' + sLineBreak
    + 'WHERE  SUBSTR(abA01,1,4) = ''%1:s'' AND abapost = ''Y'' AND aba19 = ''Y'' AND aba01 BETWEEN ''%2:s'' AND  ''%3:s'' '
    + sLineBreak + ') GROUP BY ABA00,KM     ' + sLineBreak +
    ') xx                    ' + sLineBreak +
    'LEFT JOIN aag_file ON km = aag01';

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

function TWorkForm.RunCDS(SQL: string): Boolean;
begin
  Result := false;
  Cds.close;
  if not dba.ReadDataset(SQL, Cds) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    Cds.active := true;
    Result := true;
  end;
end;

procedure TWorkForm.RunQueryClick(Sender: TObject);
var
  ljF1, ljF2, SQL: string;
  midQ: Boolean; // 是否中途退出
label MIDQUIT;
  procedure midExit(const s: string);
  begin
    showMessage(s, '提示');
    screen.Cursor := crDefault;
    midQ := true;
  end;

begin
  midQ := false;
  // 主查询
  screen.Cursor := crSQLWait;
  try
    if bDh.Text <> '' then
      ljF1 := dbbhcb.Text + '-' + trim(bDh.Text)
    else
    begin
      midExit('请录入起始单号');
      goto MIDQUIT;
    end;

    if edh.Text <> '' then
      ljF2 := dbbhcb.Text + '-' + trim(edh.Text)
    else
    begin
      midExit('请录入终止单号');
      goto MIDQUIT;
    end;
  MIDQUIT:
    if midQ then
      exit;
    SQL := format(MSQL, [ztRG.Items[ztRG.ItemIndex], dbbhcb.Text, ljF1, ljF2]);
    // MEMO1.Lines.Text := SQL;
    RunCDS(SQL);
    FillmGrid(Cds);
  finally
    ResizeForm;
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.ToxlsClick(Sender: TObject);
// 导入excel
var
  str: string;
  i, j: Integer;
  excelapp, sheet: Variant;
  colName, Bcaption, XMRQ: string; // 项目编号 / 列标题 / 尾部标识行内容 /筛选时间
begin
  screen.Cursor := crAppStart;

  str := '';
  // 时间范围
  XMRQ := str + '筛选单号从： ' + char(9) + dbbhcb.Text + '-' + trim(bDh.Text) +
    char(9) + '  到： ' + char(9) + dbbhcb.Text + '-' + trim(edh.Text);

  colName := '';
  Bcaption := '';
  for i := 0 to MTV.ColumnCount - 1 do
  // 显示列标题
  begin
    colName := colName + MTV.Columns[i].Caption + char(9);
    Bcaption := Bcaption + '================' + char(9);
  end;

  // 显示内容
  str := '单别名称 ： ' + char(9) + dbmccb.Text + #13 + XMRQ + #13 + colName + #13 +
    Bcaption + #13;

  for i := 0 to MTV.DataController.RecordCount - 1 do
  begin
    for j := 0 to MTV.ColumnCount - 1 do
    begin
      if MTV.DataController.Values[i, j] <> NULL then
        str := str + vartostr(MTV.DataController.Values[i, j]) + char(9)
      else
        str := str + '' + char(9);
    end;
    str := str + #13;
    application.ProcessMessages;
  end;

  try
    try
      clipboard.Clear;
      clipboard.Open;
      clipboard.AsText := str;
      clipboard.close;
      excelapp := createoleobject('excel.application');
      excelapp.workbooks.add(1);
      sheet := excelapp.workbooks[1].worksheets[1];
      sheet.name := 'sheet1';
      sheet.paste;
      clipboard.Clear;
      excelapp.Visible := true;
    except
      application.MessageBox('Excel未安装', '提示');
    end;
  finally
    clipboard.Clear;
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.ToXlsMXClick(Sender: TObject);
begin
  // screen.Cursor := crAppStart;
  // str := '';
  // MGrid.ExpandAll;
  //
  // // 项目筛选记录
  // str := '筛选项目编号包括 ： ' + char(9) + StringReplace(xmLE.Text, ',', char(9),
  // [rfReplaceAll]) + #13 + #13;
  // // 时间范围
  // str := str + '筛选时间从： ' + char(9) + FormatDateTime('yyyy/mm/dd', beginT.Date) +
  // char(9) + '  到： ' + char(9) + FormatDateTime('yyyy/mm/dd', endT.Date) + #13
  // + #13 + #13;
  //
  // for i := 1 to MGrid.ColCount - 1 do
  // // 显示列标题
  // begin
  // if S2E(i) then
  // str := str + MGrid.ColumnHeaders[i] + char(9);
  // end;
  // str := str + #13;
  //
  // // 显示内容
  // for i := 1 to MGrid.RowCount - 1 do
  // begin
  // // 若不是汇总项，则直接跳过 汇总项判断：GRID单号列字值为'-'
  // if MGrid.Cells[KeyCol, i] = SPCHAR then
  // continue;
  // for j := 1 to MGrid.ColCount - 1 do
  // begin
  // if S2E(j) then
  // str := str + MGrid.Cells[j, i] + char(9);
  // end;
  // str := str + #13;
  // Application.ProcessMessages;
  // end;
  // try
  // try
  // clipboard.Clear;
  // clipboard.Open;
  // clipboard.AsText := str;
  // clipboard.Close;
  // excelapp := createoleobject('excel.application');
  // excelapp.workbooks.add(1);
  // sheet := excelapp.workbooks[1].worksheets[1];
  // sheet.name := 'sheet1';
  // sheet.paste;
  // clipboard.Clear;
  // excelapp.Visible := true;
  // MGrid.ContractAll;
  // screen.Cursor := crDefault;
  // except
  // showmessage('Excel未安装');
  // end;
  // finally
  // clipboard.Clear;
  // screen.Cursor := crDefault;
  // end;
end;

procedure TWorkForm.Button1Click(Sender: TObject);
begin
  application.MessageBox(Pchar(self.Caption), 'test');
end;

procedure TWorkForm.dbbhcbChange(Sender: TObject);
begin
  dbmccb.ItemIndex := dbbhcb.ItemIndex;
end;

procedure TWorkForm.ztRGClick(Sender: TObject);
begin
  if ztRG.ItemIndex = -1 then
    exit;
  IniCKBH(ztRG.Items[ztRG.ItemIndex]);
  RunQuery.Enabled := true;
end;

procedure TWorkForm.FillmGrid(Q: TClientDataSet);
var
  i, k, t: Integer;
begin
  MTV.DataController.RecordCount := 0;
  if Q.RecordCount = 0 then
    exit;
  Q.First;
  MTV.BeginUpdate();
  k := 1;
  while not Q.Eof do
  begin
    i := MTV.DataController.AppendRecord;
    t := k mod 5;
    MTV.DataController.Values[i, 0] := Q.Fields[0].AsString;
    MTV.DataController.Values[i, 1] := Q.Fields[1].AsString;
    MTV.DataController.Values[i, 2] := Q.Fields[2].AsString;
    MTV.DataController.Values[i, 3] :=
      Formatfloat('#0.###', Q.Fields[3].AsFloat * jf[t]);
    MTV.DataController.Values[i, 4] :=
      Formatfloat('#0.###', Q.Fields[4].AsFloat * df[t]);
    inc(k);
    Q.Next;
  end;
  MTV.EndUpdate;
end;

procedure TWorkForm.MTVColumn10StylesGetContentStyle
  (Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
// 改变某一列的显示类型
var
  v: Variant;
begin
  v := ARecord.Values[9];
  if (v = NULL) or (v = '') then
    exit;
  if v = sHigh then
    AStyle := stHigh;
  if v = sLow then
    AStyle := stLow;
end;

procedure TWorkForm.
  MTVTcxGridDataControllerTcxDataSummaryFooterSummaryItems2GetText
  (Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
  AText := '总计:';
end;

// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}
// 消息响应事件

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
  t: byte;
begin
  // 获取登录账号信息
  if (StrComp(Pchar(MsgName), Pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      zhanghu := InParcel.GetStringGoods('LoginUser');
      // 获取插件权限信息
      GetPrivilege('MENU', self.Caption, zhanghu);
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(Pchar(MsgName), Pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
      if softWareAuth then
        kConfusion := 0
      else
        kConfusion := 1;
      for t := 0 to 4 do
      begin
        jf[t] := 1 + (random(1000) - 500) * kConfusion / 10000;
        df[t] := 1 + (random(1000) - 500) * kConfusion / 10000;
      end;
    end;
    exit;
  end;
  // 获取当前插件的权限信息
  if (StrComp(Pchar(MsgName), Pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      InParcel.GetCDSGoods('QX', Cds);
      IniZtRG(Cds.FieldByName('zt').AsString);
    end;
    exit;
  end;
end;
{$ENDREGION}

end.

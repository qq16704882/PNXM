//
// WorkFormUnit.pas -- 工作窗体单元（应用程序员自行修改窗体内容）
//                     Version 1.00 Ansi Edition
//                     Author: Jopher(W.G.Z)
//                     QQ: 779545524
//                     Email: Jopher@189.cn
//                     Homepage: http://www.quickburro.com/
//                     Copyright(C) Jopher Software Studio
//
unit WorkFormUnit;

interface

uses
   winapi.Windows, SysUtils, ExtCtrls, Classes, Forms, FileTransfer,
   ImgList, Controls, StdCtrls, ComCtrls, Buttons, jpeg,
   QBParcel, UserConnection, QBJson, System.ImageList;

type
  TWorkForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Image1: TImage;
    Bevel1: TBevel;
    Label1: TLabel;
    ListView1: TListView;
    Progress: TProgressBar;
    Edit1: TEdit;
    ImageList1: TImageList;
    FTP: TFileTransfer;
    Timer1: TTimer;
    StatusPanel: TPanel;
    Label2: TLabel;
    Bevel2: TBevel;
    procedure FormShow(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FTPFileTransferProgress(MaxBlocks, OkBlocks: Integer);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    //
    // 必须有的四个接口对象...
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
    LocalPath: string;
  end;

var
  WorkForm: TWorkForm;

implementation

{$R *.dfm}

procedure TWorkForm.BitBtn1Click(Sender: TObject);
var
   i,j,Fails: integer;
   ok: boolean;
   RemoteFile,LocalFile: string;
begin
   StatusPanel.Visible:=true;
   StatusPanel.BringToFront;
   Fails:=0;
   for i := 0 to ListView1.Items.Count-1 do
      begin
         RemoteFile:=listview1.Items[i].SubItems[0];
         LocalFile:=LocalPath+extractfilename(RemoteFile);
         edit1.Text:='下载：'+RemoteFile;
         progress.Position:=0;
         if ListView1.Items[i].SubItems[1]<>'√' then
            begin
               for j := 1 to 3 do
                  begin
                     ok:=ftp.DownloadFile(RemoteFile,LocalFile,0);
                     if ok then
                        break;
                  end;
               if ok then
                  ListView1.Items[i].SubItems[1]:='√'
               else
                  begin
                     ListView1.Items[i].SubItems[1]:='X';
                     inc(fails);
                  end;
            end;
      end;
   if fails>0 then
      begin
         bitbtn1.Caption:='重试';
         StatusPanel.Visible:=false;
      end
   else
      begin
         OutputParcel.PutBooleanGoods('ProcessResult',true);
         close;
      end;
end;

procedure TWorkForm.BitBtn2Click(Sender: TObject);
begin
   OutputParcel.PutBooleanGoods('ProcessResult',false);
   close;
end;

procedure TWorkForm.FormShow(Sender: TObject);
var
   FileList: TStringList;
   i: integer;
begin
   FileList:=TStringList.Create;
   FileList.Text:=InputParcel.GetStringGoods('DownloadFileList');
   LocalPath:=InputParcel.GetStringGoods('LocalPath');
//
// 显示...
   ListView1.Items.Clear;
   for i := 0 to FileList.Count-1 do
      begin
         with listview1.Items.Add do
            begin
               caption:=inttostr(index+1);
               subitems.Add(filelist[i]);
               subitems.Add('□');
            end;
      end;
//
   FreeAndNil(FileList);
   Ftp.TargetNodeId:=UserConn.UserNodeId;
   ftp.UserConnection:=UserConn;
   timer1.Enabled:=true;
end;

procedure TWorkForm.FTPFileTransferProgress(MaxBlocks, OkBlocks: Integer);
begin
   progress.Max:=MaxBlocks;
   progress.Position:=okBlocks;
end;

procedure TWorkForm.Timer1Timer(Sender: TObject);
begin
   timer1.Enabled:=false;
   BitBtn1Click(nil);
end;

end.

﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd, QBMisc,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL,
  cxTextEdit, cxTLdxBarBuiltInMenu, cxClasses, cxInplaceContainer, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridCustomView,
  cxGrid, cxGridExportLink, DBAccess, Uni, cxGridBandedTableView, cxLabel,
  cxContainer, cxMemo, Vcl.Menus, cxTLExportLink, tmsAdvGridExcel;

type
  TWorkForm = class(TForm)
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    cxStyleRepository: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    DELS: TcxStyle;
    AddS: TcxStyle;
    ModS: TcxStyle;
    DefaultS: TcxStyle;
    PrivilegeCDS: TClientDataSet;
    flwpnl1: TFlowPanel;
    pnl1: TPanel;
    rg1: TRadioGroup;
    pnl2: TPanel;
    cbb3: TComboBox;
    txt1: TStaticText;
    pnl6: TPanel;
    Q_year: TLabeledEdit;
    pnl8: TPanel;
    Q_dept: TLabeledEdit;
    pnl4: TPanel;
    Q_ima01: TLabeledEdit;
    cbb1: TComboBox;
    pnl7: TPanel;
    Q_ima02: TLabeledEdit;
    cbb2: TComboBox;
    pnl5: TPanel;
    btn1: TButton;
    btn2: TButton;
    TQ: TClientDataSet;
    CDS: TClientDataSet;
    pgc1: TPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    Button1: TButton;
    Button2: TButton;
    Memo1: TMemo;
    Cost_Run: TButton;
    Cost_tree: TButton;
    Cost_BRun: TButton;
    lstMtreel: TcxTreeList;
    lstMtreelColumn1: TcxTreeListColumn;
    lstMtreelColumn2: TcxTreeListColumn;
    lstMtreelColumn3: TcxTreeListColumn;
    lstMtreelColumn4: TcxTreeListColumn;
    lstMtreelColumn5: TcxTreeListColumn;
    lstMtreelColumn6: TcxTreeListColumn;
    lstMtreelColumn7: TcxTreeListColumn;
    lstMtreelColumn8: TcxTreeListColumn;
    lstMtreelColumn9: TcxTreeListColumn;
    lstMtreelColumn10: TcxTreeListColumn;
    lstMtreelColumn11: TcxTreeListColumn;
    lstMtreelColumn12: TcxTreeListColumn;
    lstMtreelColumn13: TcxTreeListColumn;
    lstMtreelColumn14: TcxTreeListColumn;
    lstMtreelColumn15: TcxTreeListColumn;
    lstMtreelColumn16: TcxTreeListColumn;
    lstMtreelColumn17: TcxTreeListColumn;
    lstMtreelColumn18: TcxTreeListColumn;
    lstMtreelColumn19: TcxTreeListColumn;
    lstMtreelColumn20: TcxTreeListColumn;
    lstMtreelColumn21: TcxTreeListColumn;
    lstMtreelColumn22: TcxTreeListColumn;
    lstMtreelColumn23: TcxTreeListColumn;
    lstMtreelColumn24: TcxTreeListColumn;
    lstMtreelColumn25: TcxTreeListColumn;
    lstMtreelColumn26: TcxTreeListColumn;
    lstMtreelColumn27: TcxTreeListColumn;
    lstMtreelColumn28: TcxTreeListColumn;
    lstMtreelColumn29: TcxTreeListColumn;
    lstMtreelColumn30: TcxTreeListColumn;
    lstMtreelColumn31: TcxTreeListColumn;
    lstMtreelColumn32: TcxTreeListColumn;
    lstMtreelColumn33: TcxTreeListColumn;
    lstMtreelColumn34: TcxTreeListColumn;
    lstMtreelColumn35: TcxTreeListColumn;
    lstMtreelColumn36: TcxTreeListColumn;
    lstMtreelColumn37: TcxTreeListColumn;
    TCDS: TClientDataSet;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Panel1: TPanel;
    leBB: TLabeledEdit;
    Q_month: TLabeledEdit;
    MGrid: TAdvStringGrid;
    Button3: TButton;
    agexp: TAdvGridExcelIO;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure cGBCustomDrawGroupSummaryCell(Sender: TObject; ACanvas: TcxCanvas;
      ARow: TcxGridGroupRow; AColumn: TcxGridColumn;
      ASummaryItem: TcxDataSummaryItem;
      AViewInfo: TcxCustomGridViewCellViewInfo; var ADone: Boolean);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure cbb3Change(Sender: TObject);
    procedure Cost_treeClick(Sender: TObject);
    procedure Cost_RunClick(Sender: TObject);
    procedure Cost_BRunClick(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure pgc1Change(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure MGridGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure MGridGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function RunCDS(SQL: string; DS: TClientDataSet): Boolean;
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure WaitMessage;
    procedure GetPrivilege(TFlag, TID, UID: string);
    function F_BM: string;
    function F_Date: string;
    function F_RKLJ: string;
    function GetBM: string;
    function GetND: integer; //获取年度
    function GetYF: integer;//获取月份
    function GetPh: string;
    function GetPM: string;
    function GetZT: string;
    function GetBB: string; // 获取版本值
    procedure GetZThDP(userID: string = '0912101');
    procedure SetLJSM;
    procedure FillCGrid(D: TClientDataSet);
    procedure ExtractedMethod(var filterS: string);
    procedure RunTreeList(lj, dp, Ys, V: string);
    procedure FullRTTree(ARecords: TstringList; x: TcxTreeList);
    function F_BB: string;
    procedure fillGridHeader(ID: Byte = 0);
    function F_JHLJ: string;
    procedure FillBGrid(D: TclientDataSet);
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'SCSTDCOSTTY_DLL';
  // 业务代码
  TreeSplit = '@';
  ZT = 'GWGS,GNGS,YKGS,SHBM,PKGS';
  DZT: array [0 .. 4] of string = ('ERP', 'ERP_GN', 'ERP_YK', 'ERP_SHBM','ERP_PKGS');

  //成本分群与材料项目之间的对应关系
  CBSQL = 'SELECT azf01,azf03,ta_azf02,tc_cuz05 FROM azf_file ' +
          'LEFT JOIN tc_cuz_file ON tc_cuz04 = ta_azf02     '+
          'WHERE  azf02 = ''G'' AND ta_azf02 IS NOT NULL AND azfacti = ''Y''  ORDER BY ta_azf02 ';
  // 记录账套对应的UDAC数据库定义
  BMSQL = 'SELECT GEN03 FROM %s.GEN_FILE WHERE GEN01 = ''%s''';
  //根据入库量获取料件列表,并匹配cbgi111中的指定月的计划量情况
  JHSQL =
      'WITH dd' + sLineBreak +
      'AS ' + sLineBreak +
      '(' + sLineBreak +
      'SELECT  ' + sLineBreak +
      'sfv04,SUM(sfv09) sl,MAX(sfv08) dw' + sLineBreak +
      'FROM sfv_file,sfu_file WHERE %s AND sfuconf = ''Y'' AND  sfv01 = sfu01' + sLineBreak +
      'GROUP BY sfv04' + sLineBreak +
      ')' + sLineBreak +
      'SELECT PH,ima02,NVL(imaud03,'' '') IMAUD03,nvl(OCC01,'' '') OCC01,NVL(OCC02,'' '') OCC02,NVL(khlx,'' '') KHLX,JHL,' + sLineBreak +
      '            DECODE(khrk,0,sysl,khrk) rkl,DW,ima906,nvl(ima907,''吨'') ima907,' + sLineBreak +
      '            DECODE(ima906,1,1,nvl(smd06,1)/DECODE(NVL(smd04,1),1,1,smd04)) hsl ' + sLineBreak +
      'FROM' + sLineBreak +
      '(' + sLineBreak +
      '       SELECT PH,SL,DW,OCC01,OCC02,khlx,JHL,KHRK,LAG(sl-khrk,1,sl) OVER(PARTITION BY PH ORDER BY occ01) sysl FROM' + sLineBreak +
      '       (' + sLineBreak +
      '       SELECT dd.sfv04 ph,dd.sl,dd.dw,'''' occ01,'''' occ02,'''' khlx,0 jhl,0 khrk FROM dd' + sLineBreak +
      '       UNION ALL' + sLineBreak +
      '       SELECT dd.sfv04,dd.sl,dd.dw,k.tc_bgn05,k.occ02,k.tc_bgn06,k.jhsl,' + sLineBreak +
      '       NVL((SELECT SUM(sfv09) sfv09' + sLineBreak +
      '       FROM sfv_file' + sLineBreak +
      '       LEFT JOIN sfu_file ON sfu01 = sfv01' + sLineBreak +
      '       LEFT JOIN sfb_file ON sfv11 = sfb01' + sLineBreak +
      '       LEFT JOIN occ_file ON occ09 = ta_sfb02 ' + sLineBreak +
      '       WHERE %0:s AND sfuconf = ''Y'' AND  sfv01 = sfu01 AND sfv04 = dd.sfv04 AND occ01 = k.tc_bgn05),0) rksl' + sLineBreak +
      '        FROM dd' + sLineBreak +
      '        LEFT JOIN' + sLineBreak +
      '        (SELECT tc_bgn04,tc_bgn05,(CASE tc_bgn06 WHEN ''1'' THEN ''国内'' ELSE ''海外'' END) tc_bgn06,occ02,' + sLineBreak +
      '                                        (CASE %d' + sLineBreak +
      '                                              WHEN 1 THEN tc_bgn08' + sLineBreak +
      '                                              WHEN 2 THEN tc_bgn09' + sLineBreak +
      '                                              WHEN 3 THEN tc_bgn10' + sLineBreak +
      '                                              WHEN 4 THEN tc_bgn11' + sLineBreak +
      '                                              WHEN 5 THEN tc_bgn12' + sLineBreak +
      '                                              WHEN 6 THEN tc_bgn13' + sLineBreak +
      '                                              WHEN 7 THEN tc_bgn14' + sLineBreak +
      '                                              WHEN 8 THEN tc_bgn15' + sLineBreak +
      '                                              WHEN 9 THEN tc_bgn16' + sLineBreak +
      '                                              WHEN 10 THEN tc_bgn17' + sLineBreak +
      '                                              WHEN 11 THEN tc_bgn18' + sLineBreak +
      '                                              WHEN 12 THEN tc_bgn19' + sLineBreak +
      '                                                END ) jhsl  ' + sLineBreak +
      '         FROM tc_bgn_file LEFT JOIN occ_file ON tc_bgn05 = occ01 WHERE %s AND tc_bgn01 = 1' + sLineBreak +
      '         ) k ON k.tc_bgn04 = dd.sfv04 ' + sLineBreak +
      '         WHERE jhsl IS NOT NULL ORDER BY 1' + sLineBreak +
      '         )' + sLineBreak +
      ') ' + sLineBreak +
      'LEFT JOIN ima_file ON ima01 = ph' + sLineBreak +
      'LEFT JOIN smd_file ON smd01 = ima01 AND smd02 = dw AND smd03 = ima907 AND smdacti = ''Y''' + sLineBreak +
      'WHERE sysl > 0 AND ph like ''9%%''';

  CSQL = 'SELECT mlj,yl,ta_bgc04||''|''||ima12 tp,C001,C002,C003,C004,C005,C006,C101,C102,C302,C201,C202,C204,C301,C203 FROM stdcost_cost ' +
         'LEFT JOIN ima_file ON ima01 = bmb03 WHERE MLJ IN %s AND DPT = ''%s'' AND YEARS = %d AND VER = %s';

  QSQL = 'SELECT a.bmb03,b.ima02,b.ima021,b.ima25,' +
    'a.c001+a.c001T c001,a.c002+a.c002T c002,a.c003+a.c003T c003,a.c004+a.c004T c004,a.c005+a.c005T c005,'
    + 'a.c101+a.c101T c101,a.c102+a.c102T c102,a.c201+a.c201T c201,a.c202+a.c202T c202,a.c204+a.c204T c204,a.c203+a.c203T c203,'
    + 'a.c001+a.c001T+a.c002+a.c002T+a.c003+a.c003T+a.c004+a.c004T+a.c005+a.c005T+a.c101+a.c101T+a.c102+a.c102T+a.c201+a.c201T+a.c202+a.c202T+a.c204+a.c204T+a.c203+a.c203T TT '
    + '  FROM stdcost_cost a ' +
    'LEFT JOIN ima_file b ON a.bmb03 = b.ima01   %s ORDER BY a.bmb03';
  TSQL = 'SELECT a.bmb03,b.ima02,b.ima021,b.ima25,a.IsM,a.ecd07,c.eca02,nvl(a.YL,1) YL,nvl(a.bmb08,0) bmb08,nvl(a.JGXH,0) JGXH,nvl(a.BGC07,0) BGC07,nvl(a.TIm,0) Tim,nvl(a.eqv,0) eqv,'
    + 'nvl(a.c001,0) c001,nvl(a.c001T,0) c001T,nvl(a.c002,0) c002,nvl(a.c002T,0) c002T,nvl(a.c003,0) c003,nvl(a.c003T,0) c003T,nvl(a.c004,0) c004,nvl(a.c004T,0) c004T,nvl(a.c005,0) c005,nvl(a.c005T,0) c005T,'
    + 'a.c101,a.c101T,a.c102,a.c102T,a.c201,a.c201T,a.c202,a.c202T,a.c204,a.c204T,a.c203,a.c203T,'
    + 'a.c001+a.c001T+a.c002+a.c002T+a.c003+a.c003T+a.c004+a.c004T+a.c005+a.c005T+a.c101+a.c101T+a.c102+a.c102T+a.c201+a.c201T+a.c202+a.c202T+a.c204+a.c204T+a.c203+a.c203T TT,a.T'
    + '  FROM stdcost_cost a ' + 'LEFT JOIN ima_file b ON a.bmb03 = b.ima01  ' +
    'LEFT JOIN eca_file c ON c.eca01 = a.ecd07 %s ORDER BY R';
  MSQL = 'WITH cbpmb AS ' + sLineBreak +
    ' (SELECT ima01 FROM %0:s.IMA_FILE,%0:s.TC_CUA_FILE WHERE %1:s )' +
    sLineBreak +
    'SELECT tc_cua00,tc_cua01,tc_cua02,ima02,ima021,tc_cua16,gem02,tc_cua14,eca02, '
    + '原材料, 辅料,模具,附件,包装,直接人工,间接人工,电费,天然气费,其他费用,折旧 FROM       ' +
    '(                                                                                     '
    + sLineBreak +
    'SELECT tc_cua00,tc_cua01,tc_cua02,tc_cua16,tc_cua14,SUM(A) AS 原材料, SUM(B) AS 辅料,SUM(C) AS 模具,  '
    + 'SUM(D) AS 附件,SUM(E) AS 包装,SUM(F) AS 直接人工, SUM(G) AS 间接人工,SUM(H) AS 电费,SUM(I) AS 天然气费,'
    + 'SUM(J) AS 其他费用,SUM(K) AS 折旧 FROM                                                            '
    + '( ' + sLineBreak +
    'SELECT tc_cua00,tc_cua01,tc_cua02,tc_cua16,tc_cua14,A,B,C,D,E,F,G,H,I,J,K FROM  '
    + sLineBreak +
    '(SELECT tc_cua00,tc_cua01,tc_cua02,tc_cua16,tc_cua14,tc_cua07,tc_cua04 FROM %0:s.tc_cua_file WHERE %2:s) '
    + sLineBreak +
    'PIVOT (SUM(tc_cua07) FOR tc_cua04 IN                                                                    '
    + '(''001'' A,''002'' B,''003'' C,''004'' D,''005'' E,''101'' F,''102'' G,''201'' H,''202'' I,''203'' J,''204'' K))'
    + sLineBreak + ')   ' + sLineBreak +
    'GROUP BY tc_cua00,tc_cua01,tc_cua02,tc_cua16,tc_cua14  ' + sLineBreak +
    ') aa   ' + sLineBreak + 'LEFT JOIN %0:s.ima_file ON ima01 = tc_cua02  ' +
    sLineBreak + 'LEFT JOIN %0:s.eca_file ON eca01 = tc_cua14  ' + sLineBreak +
    'LEFT JOIN %0:s.gem_file ON gem01 = tc_cua16  ' + sLineBreak +
    'ORDER BY 1,2,3,6,8 DESC';

 HSR1 = '品号,品名,配方代码,客户编号,客户名称,数量,数量,数量单位,重量,重量,重量单位,国内/海外,前道工序加工成本,添加剂加工成本';
 LSR1 = ',直接人工,间接人工,水费,电费,气费,折旧,维修费,其他';

 HSR2 = ',,,,,计划,入库,,计划,入库,,,,';
 LSR2 = ',,,,,,,,';

 HSR3 = 'ima01,ima02,pf,occ01,occ02,js,rs,sdw,jz,rz,zdw,gnw,qx,tjj';
 LSR3  =',C101,C102,C302,C201,C202,C204,C301,C203';

 CCount = 22;//除去材料项目后的col列数
 SCount = 14;//材料项目开始的列值,从(国内/海外)后添加
 LCount = 8;//材料项目后成项目列数

var
  zhanghu: string;
  RunInfo: string;
  user_zt, user_dept: string; // 用户所在部门与账套
  ReadAllZtCb: Boolean; // 定义断言是否可读取所有账套的成本  实现：是否可选择账号所在账套以外的账套
  ReadAllCjCb: Boolean; // 定义断言是否可读取所有车间的成本  实现：是否可修改自己所在账套以外的账套
  RunView: Boolean; // 定义断言是否拥有可以进行标准成本计算的权限
  softWareAuth: Boolean;
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  MessageFlag: Boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.WaitMessage;
begin
  While True do
  Begin
    application.ProcessMessages; // 读取消息队列
    If Not MessageFlag Then
      Break;
  End;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');

  // 初始化数据集对象（不再使用主程序传回来的DatabaseID值）...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');

  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage; // 同步处理消息

  // 根据用户信息获取所在账套与部门
  GetZThDP(zhanghu);
  // 根据用户信息初始化数据集对象（不再使用主程序传回来的DatabaseID值）...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := DZT[cbb3.ItemIndex];

  // 权限的设置
  // 获取是否可修改项目编号权限,设置账套的只读性
  GetPrivilege('ASSERT', '2', zhanghu);
  WaitMessage;
  if PrivilegeCDS.RecordCount = 0 then
    ReadAllZtCb := false
  else
    ReadAllZtCb := Not(PrivilegeCDS.FieldByName('R1').AsString = '0');
  // 获取是否可修改项目编号权限,设置部门的只读性
  GetPrivilege('ASSERT', '3', zhanghu);
  WaitMessage;
  if PrivilegeCDS.RecordCount = 0 then
    ReadAllCjCb := false
  else
    ReadAllCjCb := Not(PrivilegeCDS.FieldByName('R1').AsString = '0');
  // 获取标准成本计算权限
  GetPrivilege('ASSERT', '4', zhanghu);
  WaitMessage;
  if PrivilegeCDS.RecordCount = 0 then
    RunView := false
  else
    RunView := Not(PrivilegeCDS.FieldByName('R1').AsString = '0');

  // 界面初始化

  Q_dept.Enabled := ReadAllCjCb;
  cbb3.Enabled := ReadAllZtCb;
  Cost_Run.Visible := RunView;
  Cost_BRun.Visible := RunView;
  Cost_Run.Visible := False;
  Cost_BRun.Visible := False;

  Q_dept.Text := user_dept;
  cbb3.Text := user_zt;
  Q_year.Text := IntToStr(YearOf(Now));
  Q_month.Text:= IntToStr(MonthOf(Now));
  fillGridHeader();
  ResizeForm;
end;

// 相关函数
procedure TWorkForm.SetLJSM;
var
  Multi: Boolean;
begin
  Multi := (cbb1.ItemIndex > 0) OR (cbb2.ItemIndex > 0);
  if Multi then
    rg1.ItemIndex := 1
  else
    rg1.ItemIndex := 0;
end;

procedure TWorkForm.GetZThDP(userID: string = '0912101');
var
  sl: TstringList;
  i: Integer;
  SQL: string;
begin
  sl := TstringList.Create;
  sl.DelimitedText := ZT;
  for i := 0 to sl.Count - 1 do
  begin
    SQL := Format(BMSQL, [sl[i], userID]);
    RunCDS(SQL, TQ);
    if TQ.RecordCount = 1 then
      Break;
  end;
  user_zt := sl[i];
  user_dept := TQ.FieldByName('GEN03').Text;
  cbb3.ItemIndex := i;
  Q_ima02.Text := dba.TargetDatabaseId;
  Q_dept.Text := user_dept;
  FreeAndNil(sl);
end;

function TWorkForm.GetPh: string;
// 获取品号筛选条件
var
  s: string;
begin
  s := Trim(Q_ima01.Text);
  if s = '' then
    Exit('');
  result := '';
  case cbb1.ItemIndex of
    0:
      result := ' SFV04 = ''' + s + '''';
    1:
      result := ' SFV04 LIKE ''%' + s + '%''';
    2:
      result := ' SFV04 LIKE ''' + s + '%''';
    3:
      result := ' SFV04 LIKE ''%' + s + '''';
  end;
end;

function TWorkForm.GetPM: string;
// 获取品名筛选条件
var
  s: string;
begin
  s := Trim(Q_ima02.Text);
  if s = '' then
    Exit('');
  result := '';
  case cbb2.ItemIndex of
    0:
      result := ' = ''' + s + '''';
    1:
      result := ' LIKE ''%' + s + '%''';
    2:
      result := ' LIKE ''' + s + '%''';
    3:
      result := ' LIKE ''%' + s + '''';
  end;
end;

function TWorkForm.GetND: Integer;
// 获取年度，若录入异常，默认为当前年度
var
  i: Integer;
begin
  i := StrToIntDef(Trim(Q_year.Text), 0);
  if (i < 2000) or (i > 3000) then
    result := YearOf(Now)
  else
    result := i;
end;

function TWorkForm.GetYF: Integer;
// 获取月份,若录入异常,默认为当前月份
var
  i: Integer;
begin
  i := StrToIntDef(Trim(Q_month.Text), 0);
  if (i < 1) or (i > 12) then
    i := MonthOf(Now);
  result := i;
end;

function TWorkForm.GetBM: string;
// 获取部门
begin
  if Trim(Q_dept.Text) = '' then
    result := ' '
  else
    result := Trim(Q_dept.Text);
end;

function TWorkForm.GetZT: string;
// 获取账套
begin
  if cbb3.Text = '' then
    result := user_zt
  else
    result := cbb3.Text;
end;

function TWorkForm.GetBB: string;
var
  SQL, s: string;
begin
  result := '';
  SQL := 'SELECT  TC_ECE01 FROM TC_ECE_FILE WHERE TC_ECE03 = ''%s'' AND TC_ECE02 = %d AND TC_ECEUD01 = ''Y''';
  if dba.ReadSimpleResult(Format(SQL, [GetBM, GetND]), s) then
  begin
    if s = '' then
      result := '1'
    else
      result := s;
  end;
  leBB.Text := result;
end;

function TWorkForm.F_BB: string;
var
  s: string;
begin
  s := GetBB;
  if s = '' then
    result := ' TC_BGN01  = ''1'''
  else
    result := ' TC_BGN01  = ''' + s + '''';
end;

function TWorkForm.F_Date: string;
var
  s: string;
  y, m, D: word;
  beginT, endT: TDate;
begin
  y := GetND;
  m := GetYF;
  D := 1;
  beginT := EncodeDate(y, m, D);
  endT := IncMonth(beginT, 1) - 1;
  result := ' sfu02 BETWEEN to_date(''' + FormatDateTime('yyyymmdd', beginT) +
    ''',''yyyymmdd'') AND ' + ' to_date(''' + FormatDateTime('yyyymmdd', endT) +
    ''',''yyyymmdd'')';
end;

function TWorkForm.F_BM: string;
var
  s: string;
begin
  s := GetBM;
  if s = '' then
    s := ' ';
  result := ' SFU04 = ''' + s + '''';
end;

function TWorkForm.F_RKLJ: string;
// 获取有入库量料件的过滤条件
var
  s, ph, fnd, fbm: string;
begin
  ph := GetPh;
  fnd := F_Date;
  fbm := F_BM;

  // 年度条件
  s := fnd;
  // 部门条件
  if fbm <> '' then
    s := s + ' AND ' + fbm;
  // 料号条件
  if ph <> '' then
    s := s + ' AND ' + ph;
  result := s;
end;

function TWorkForm.F_JHLJ: string;
// 获取有预算料件的过滤条件
var
  s, ph, fbb, fnd, fbm: string;
begin
  ph := StringReplace(GetPh, 'SFV04', 'TC_BGN04', [rfReplaceAll]);
  fnd := Format(' TC_BGN03 = %d', [GetND]);
  fbb := F_BB;
  fbm := StringReplace(F_BM, 'SFU04', 'TC_BGN02', [rfReplaceAll]);
  s := ' 1 = 1 ';
  // 料号条件
  if ph <> '' then
    s := s + ' AND ' + ph;
  // 年度条件
  s := s + ' AND ' + fnd;
  // 版本条件
  s := s + ' AND ' + fbb;
  // 部门条件
  if fbm <> '' then
    s := s + ' AND ' + fbm;

  result := s;
end;

// -----------------------------------------------------------------------------

procedure TWorkForm.ExtractedMethod(var filterS: string);
var
  lj: string;
  bm: string;
begin
  lj := GetPh;
  bm := GetBM;
  filterS := ' WHERE 1 = 1';
  if lj <> '' then
  begin
    filterS := filterS + ' AND ' + lj;
  end;
  if bm <> '' then
    filterS := filterS + ' AND TC_ECG03 = ''' + bm + ''' ';

  filterS := filterS + ' AND TC_ECG02 = ' + Q_year.Text;
end;

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

function TWorkForm.RunCDS(SQL: string; DS: TClientDataSet): Boolean;
var
  s, t: string;
begin
  result := false;
  if not dba.ReadDataset(SQL, DS) then
  begin
    application.MessageBox('读数据集失败！', 'Infomation');
    dba.GetLastError(s, t);
    LogMsg(pchar(DLL_KEY + s + ':' + t));
  end
  else
  begin
    DS.Active := True;
    result := True;
  end;
end;

procedure TWorkForm.fillGridHeader(ID: Byte);
// ID号，用于区分车间名。默认为0表示不区分
// 根据成本分群与材料项目对应关系动态生成列
var
  i, j, C: Integer;
  Count: Integer;
  S1, S2, S3: string;
  temps: string;
  CList1, CList2, CList3: TstringList;
begin
  CList1 := TstringList.Create;
  CList2 := TstringList.Create;
  CList3 := TstringList.Create;

  RunCDS(CBSQL, CDS);

  Count := CCount + CDS.RecordCount;

  S1 := '';
  S2 := '';
  S3 := '';

  CDS.First;
  while not CDS.Eof do
  begin
    S1 := S1 + ',' + CDS.FieldByName('tc_cuz05').AsString;
    S2 := S2 + ',' + CDS.FieldByName('azf03').AsString;
    S3 := S3 + ',' + CDS.FieldByName('ta_azf02').AsString + '|' +
      CDS.FieldByName('azf01').AsString;
    CDS.Next;
  end;
  CList1.DelimitedText := HSR1 + S1 + LSR1;
  CList2.DelimitedText := HSR2 + S2 + LSR2;
  CList3.DelimitedText := HSR3 + S3 + LSR3;

  MGrid.BeginUpdate;
  // 填充表头
  MGrid.ClearAll;
  MGrid.RowCount := 5;
  MGrid.ColCount := CList1.Count;
  MGrid.FixedRows := 3;

  for i := 0 to CList1.Count - 1 do
  begin
    MGrid.Cells[i, 0] := CList1[i];
    MGrid.Cells[i, 1] := CList2[i];
    MGrid.Cells[i, 2] := CList3[i]
  end;
  MGrid.AutoSize := True;
  for i := 0 to 4 do
    MGrid.MergeCells(i, 0, 1, 2);
  MGrid.MergeCells(7, 0, 1, 2);
  for i := 10 to SCount - 1 do
    MGrid.MergeCells(i, 0, 1, 2);
  for i := CList1.Count - LCount to CList1.Count - 1 do
    MGrid.MergeCells(i, 0, 1, 2);

  // 行1数量列合并
  MGrid.MergeCells(5, 0, 2, 1);
  // 行1重量列合并
  MGrid.MergeCells(8, 0, 2, 1);
  // 原材料项目动态合并
  temps := MGrid.Cells[SCount, 0];
  C := SCount;
  j := 0;
  for i := SCount + 1 to CList1.Count - LCount - 1 do
  begin
    Inc(j);
    if temps <> MGrid.Cells[i, 0] then
    begin
      MGrid.MergeCells(C, 0, j, 1);
      temps := MGrid.Cells[i, 0];
      C := i;
      j := 0;
    end;
  end;
  // 设置第三行的高度.正常情况下设置为0,不显示.
   MGrid.RowHeights[2] := 0;
  MGrid.EndUpdate;
  ResizeForm;

  CList1.Free;
  CList2.Free;
  CList3.Free;
end;

procedure TWorkForm.FillCGrid(D: TClientDataSet);
var
  i, j: Integer;
  v : string;
begin
  if D.RecordCount = 0 then
    Exit;
  MGrid.RowCount := D.RecordCount + 3;
  i := 3;
  CDS.First;
  MGrid.BeginUpdate;
  while Not CDS.Eof do
  begin
    MGrid.Cells[0, i] := D.FieldByName('PH').AsString;
    MGrid.Cells[1, i] := D.FieldByName('IMA02').AsString;
    MGrid.Cells[2, i] := D.FieldByName('IMAUD03').AsString;
    MGrid.Cells[3, i] := D.FieldByName('OCC01').AsString;
    MGrid.Cells[4, i] := D.FieldByName('OCC02').AsString;
    MGrid.Cells[6, i] := D.FieldByName('RKL').AsString;
    MGrid.Cells[7, i] := D.FieldByName('DW').AsString;
    //----为零值时直接不显示---------------------------------------
    v := D.FieldByName('JHL').AsString;
    if v = '0' then v := '';
    MGrid.Cells[5, i] := V;

    v := FormatFloat('##0.###', D.FieldByName('JHL').AsFloat *
      D.FieldByName('HSL').AsFloat);
    if v = '0' then V := '';
    MGrid.Cells[8, i] := v;
  //---------------------------------------------------------------
    MGrid.Cells[9,i] := FormatFloat('##0.###', D.FieldByName('RKL').AsFloat *
      D.FieldByName('HSL').AsFloat);
    MGrid.Cells[10, i] := D.FieldByName('IMA907').AsString;
    MGrid.Cells[11, i] := D.FieldByName('KHLX').AsString;
    Inc(i);
    CDS.Next;
  end;
  MGrid.AutoSize := True;
  MGrid.EndUpdate;
end;

procedure TWorkForm.FillBGrid(D: TClientDataSet);
var
  i, j, tpI: Integer;
  ph: string; // 品号
  CBLX, FLX: string; // 成本类型(包括材料与费用成本)
  V: Double; // 计算值
begin
  MGrid.BeginUpdate;
  for i := 3 to MGrid.RowCount - 1 do
  begin
    ph := MGrid.Cells[0, i];
    D.Filter := ' MLJ = ''' + ph + '''';
    D.Filtered := True;
    for j := SCount to MGrid.ColCount - 1 do
    begin
      CBLX := MGrid.Cells[j, 2];
      tpI := pos('|', CBLX);
      if tpI > 0 then
        FLX := 'C' + Copy(CBLX, 1, tpI - 1)
      ELSE
        FLX := CBLX;

      V := 0;
      D.First;
      while not D.Eof do
      begin
        if tpI > 0 then
        begin
          if D.FieldByName('TP').AsString = CBLX then
            V := V + D.FieldByName(FLX).AsFloat * StrToFloatDef(MGrid.Cells[6, i], 0);
        end
        else
          V := V + D.FieldByName(FLX).AsFloat * StrToFloatDef(MGrid.Cells[6, i], 0);
        D.Next;
      end;
      if v = 0 then
      MGrid.Cells[j,i] := ''
      else
      MGrid.Cells[j, i] := FormatFloat('##0.###', V);
    end;
  end;
  MGrid.EndUpdate;
  D.Filtered := false;
end;

procedure TWorkForm.btn1Click(Sender: TObject);
var
  aRecord: TstringList;
  s, V, S1, f, SQL: string;
  Pyl, cyl: Double; // 上阶用量、下阶用量
  PLv, cLV, i, j: Integer; // 上条记录与当前记录的LV值
  RkTj, JhTj: string;
  m: Integer;
  LJList: TstringList;
begin
  if dba.TargetDatabaseId = '' then
  begin
    ShowError('请选择账套后继续', '提示');
    Exit;
  end;

  pgc1.ActivePageIndex := 0;

  RkTj := F_RKLJ;
  JhTj := F_JHLJ;
  Screen.Cursor := crHourGlass;
  try
  m := GetYF; // 获取月份
  SQL := Format(JHSQL, [RkTj, m, JhTj]);

//   memo1.Lines.Text := sql;
//   exit;
  if RunCDS(SQL, CDS) then
  begin
    if CDS.RecordCount = 0 then
    begin
      application.MessageBox('没有查到入库料号的相关信息', 'Infomation');
      exit;
    end
    else
      FillCGrid(CDS);
  end;

  // ljlist记录料件列表,要求料件不重复
  LJList := TstringList.Create;
  LJList.Sorted := True;
  LJList.Duplicates := dupIgnore;

  CDS.First;
  while not CDS.Eof do
  begin
    LJList.add('''' + CDS.FieldByName('PH').AsString + '''');
    CDS.Next;
  end;
  LJList.DelimitedText := '(' + LJList.DelimitedText + ')';
  SQL := Format(CSQL, [LJList.DelimitedText, GetBM, GetND, leBB.Text]);
  Memo1.Text := SQL;
  if RunCDS(SQL, CDS) then
  begin
    if CDS.RecordCount = 0 then
      application.MessageBox('没有查到相关信息', 'Infomation')
    else
      FillBGrid(CDS);
  end;
  LJList.Free;
  ResizeForm;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.btn2Click(Sender: TObject);
var
  SaveDialog: TSaveDialog;
begin
  SaveDialog := TSaveDialog.Create(nil);
  with SaveDialog do
  begin
    Filter := '*.xls|*.xls';
    if Execute then
    begin
      agexp.XLSExport(SaveDialog.FileName + '.xls');
      showmessage('导入成功！', '提示');
    end;
  end;
  SaveDialog.Free;
end;

procedure TWorkForm.cGBCustomDrawGroupSummaryCell(Sender: TObject;
  ACanvas: TcxCanvas; ARow: TcxGridGroupRow; AColumn: TcxGridColumn;
  ASummaryItem: TcxDataSummaryItem; AViewInfo: TcxCustomGridViewCellViewInfo;
  var ADone: Boolean);
begin
  // 去除汇总项的'SUM='字样
  if Copy(AViewInfo.Text, 1, 4) = 'SUM=' then
  begin
    AViewInfo.Text := Copy(AViewInfo.Text, 5, Length(AViewInfo.Text) - 4);
    AViewInfo.AlignmentHorz := taRightJustify;
  end;
end;

procedure Runs;
begin
  sleep(700);
end;

procedure TWorkForm.Cost_BRunClick(Sender: TObject);
var
  filterS, t, s, V, SQL: string;
  i: Integer;
  Tp: TQBParcel;
begin
  if dba.TargetDatabaseId = '' then
  begin
    ShowError('请选择账套后继续', '错误');
    Exit;
  end;
  if Q_dept.Text = '' then
  begin
    ShowError('料件部门不能为空', '错误');
    Exit;
  end;

  ExtractedMethod(filterS);
  V := GetBB;
  SQL := Format('SELECT count(1) RT FROM TC_ECG_FILE %s', [filterS]);
  if not dba.ReadSimpleResult(SQL, s) then
  begin
    ShowError('查询出错，请重试', '错误');
    Exit;
  end
  else
  begin
    if s = '0' then
    begin
      QBMisc.showmessage('没有要计算的成本料件!', '信息');
      Exit;
    end;
  end;

  s := '共需计算 ' + s + ' 条记录';

  SQL := Format('SELECT TC_ECG04,TC_ECG03 RT FROM TC_ECG_FILE %s', [filterS]);

  if not RunCDS(SQL, CDS) then
  begin
    ShowError('查询出错，请重试', '错误');
    Exit;
  end;
  i := 1;
  Tp := TQBParcel.Create;

  CDS.First;
  while not CDS.Eof do
  begin
    Tp.Clear;
    Tp.PutStringGoods('@iUser', zhanghu, gdInput);
    Tp.PutStringGoods('@iLiaoJian', CDS.Fields[0].Text, gdInput);
    Tp.PutStringGoods('@idept', CDS.Fields[1].Text, gdInput);
    Tp.PutIntegerGoods('@iYear', GetND, gdInput);
    Tp.PutStringGoods('@iVer', V, gdInput);
    Tp.PutStringGoods('@iZT', cbb3.Text, gdInput);
    Runs;
    if dba.ExecuteStoredProc('gwgs.StdRun', Tp) then
      RunInfo := '已完成第' + i.ToString + '条记录的计算'
    else
      RunInfo := '第' + i.ToString + '条记录计算失败';
    Inc(i);
    CDS.Next;
  end;
  QBMisc.showmessage('计算完成', '信息');
  Tp.Free;
end;

procedure TWorkForm.Cost_RunClick(Sender: TObject);
var
  t, E, C, V: string;
  Tp: TQBParcel;
begin
  if dba.TargetDatabaseId = '' then
  begin
    ShowError('请选择账套后继续', '系统错误');
    Exit;
  end;
  if (Q_ima01.Text = '') or (cbb1.ItemIndex > 0) then
  begin
    ShowError('料件不能为空且筛选条件只能置为<等于>才能进行计算', '错误');
    Exit;
  end;
  RunInfo := '共处理1条记录，正在处理第1条记录';
  sleep(500);
  t := GetBM;
  V := GetBB;
  Tp := TQBParcel.Create;

  Tp.PutStringGoods('@iUser', zhanghu, gdInput);
  Tp.PutStringGoods('@iLiaoJian', Trim(Q_ima01.Text), gdInput);
  Tp.PutStringGoods('@idept', t, gdInput);
  Tp.PutIntegerGoods('@iYear', GetND, gdInput);
  Tp.PutStringGoods('@iVer', V, gdInput);
  Tp.PutStringGoods('@iZT', cbb3.Text, gdInput);

  if dba.ExecuteStoredProc('gwgs.StdRun', Tp) then
    application.MessageBox(pchar('运算成功'), pchar('计算'))
  else
  begin
    ShowError('运算失败，请重试', '错误');
    dba.GetLastError(E, C);
    LogMsg('Run Err: ' + E + '--' + C);
  end;

  Tp.Free;
end;

procedure TWorkForm.FullRTTree(ARecords: TstringList; x: TcxTreeList);
var
  i, j: Integer;
  AValues: TstringList;
  cNode: TcxTreeListNode;
  xtreea: array [0 .. 10] of TcxTreeListNode;
  pLev, cLev: Integer;
begin
  AValues := TstringList.Create;
  AValues.StrictDelimiter := True; // bug修复，不让空格自动分割行
  AValues.Delimiter := TreeSplit;
  x.Clear;
  x.BeginUpdate;

  pLev := 0;
  cLev := 0;
  for j := 0 to ARecords.Count - 1 do
  begin
    AValues.DelimitedText := ARecords[j];
    try
      cLev := strToint(AValues[AValues.Count - 1]);
    except
      cLev := 0;
    end;
    if cLev = 0 then
    begin
      cNode := x.add;
      xtreea[0] := cNode;
    end
    else
    begin
      cNode := xtreea[cLev - 1].AddChild;
      xtreea[cLev] := cNode;
    end;
    for i := 0 to AValues.Count - 1 do
      cNode.Texts[i] := AValues[i];
  end;
  x.EndUpdate;

  AValues.Free;
end;

procedure TWorkForm.RunTreeList(lj, dp, Ys, V: string);
var
  S1, s, SQL: string;
  TS: TstringList;
begin
  S1 := ' WHERE  MLJ = ''' + lj + ''' AND dpt = ''' + dp + ''' AND YearS = ' +
    Ys + ' AND VER  = ''' + V + '''';
  SQL := Format(TSQL, [S1]);
  // memo1.Lines.Text := sql;
  // exit;
  if not RunCDS(SQL, TCDS) then
    Exit;
  LogMsg('Tree count' + IntToStr(TCDS.RecordCount));
  TCDS.First;
  lstMtreel.Clear;
  s := '';
  // 将所有记录转换到StringList中
  TS := TstringList.Create;
  TS.StrictDelimiter := True;
  TS.Clear;
  TCDS.First;
  while not TCDS.Eof do
  begin
    s := TCDS.FieldByName('bmb03').AsString + TreeSplit +
      TCDS.FieldByName('ima02').AsString + TreeSplit +
      TCDS.FieldByName('ima021').AsString + TreeSplit +
      TCDS.FieldByName('ima25').AsString + TreeSplit + TCDS.FieldByName('IsM')
      .AsString + TreeSplit + TCDS.FieldByName('ecd07').AsString + TreeSplit +
      TCDS.FieldByName('eca02').AsString + TreeSplit + TCDS.FieldByName('YL')
      .AsString + TreeSplit + TCDS.FieldByName('bmb08').AsString + TreeSplit +
      TCDS.FieldByName('JGXH').AsString + TreeSplit + TCDS.FieldByName('BGC07')
      .AsString + TreeSplit + TCDS.FieldByName('Tim').AsString + TreeSplit +
      TCDS.FieldByName('eqv').AsString + TreeSplit + TCDS.FieldByName('C001')
      .AsString + TreeSplit + TCDS.FieldByName('C001T').AsString + TreeSplit +
      TCDS.FieldByName('C002').AsString + TreeSplit + TCDS.FieldByName('C002T')
      .AsString + TreeSplit + TCDS.FieldByName('C003').AsString + TreeSplit +
      TCDS.FieldByName('C003T').AsString + TreeSplit + TCDS.FieldByName('C004')
      .AsString + TreeSplit + TCDS.FieldByName('C004T').AsString + TreeSplit +
      TCDS.FieldByName('C005').AsString + TreeSplit + TCDS.FieldByName('C005T')
      .AsString + TreeSplit + TCDS.FieldByName('C101').AsString + TreeSplit +
      TCDS.FieldByName('C101T').AsString + TreeSplit + TCDS.FieldByName('C102')
      .AsString + TreeSplit + TCDS.FieldByName('C102T').AsString + TreeSplit +
      TCDS.FieldByName('C201').AsString + TreeSplit + TCDS.FieldByName('C201T')
      .AsString + TreeSplit + TCDS.FieldByName('C202').AsString + TreeSplit +
      TCDS.FieldByName('C202T').AsString + TreeSplit + TCDS.FieldByName('C204')
      .AsString + TreeSplit + TCDS.FieldByName('C204T').AsString + TreeSplit +
      TCDS.FieldByName('C203').AsString + TreeSplit + TCDS.FieldByName('C203T')
      .AsString + TreeSplit + TCDS.FieldByName('TT').AsString + TreeSplit +
      TCDS.FieldByName('T').AsString;
    if s = V then
    begin
      TCDS.Next;
      Continue;
    end;
    TS.add(s);
    V := s;
    TCDS.Next;
  end;
  FullRTTree(TS, lstMtreel);
  TS.Free;
  ResizeForm;
end;

procedure TWorkForm.Cost_treeClick(Sender: TObject);
var
  aRecord: TstringList;
  s, V, S1, t: string;
  Gyl: array of Double;
begin
  if dba.TargetDatabaseId = '' then
  begin
    showmessage('请选择账套后继续', '信息');
    Exit;
  end;
  if (Q_ima01.Text = '') or (cbb1.ItemIndex > 0) then
  begin
    ShowError('料件不能为空且筛选条件只能置为<等于>才能进行计算', '错误');
    Exit;
  end;
  // 如果部门为空，则只查询部门为' '的料件成本信息

  pgc1.ActivePageIndex := 1;

  RunTreeList(Trim(Q_ima01.Text), GetBM, IntToStr(GetND), GetBB);
end;

procedure TWorkForm.N1Click(Sender: TObject);
var
  ph: string;
begin
  { if cGB.Controller.FocusedRowIndex < 0 then
    Exit;
    ph := cGB.Controller.FocusedRow.Values[0];
    pgc1.ActivePageIndex := 1;
    RunTreeList(ph, GetBM, GetND, GetBB); }
end;

procedure TWorkForm.pgc1Change(Sender: TObject);
begin
  ResizeForm;
end;

procedure TWorkForm.Button1Click(Sender: TObject);
begin
  lstMtreel.FullExpand;
  ResizeForm;
end;

procedure TWorkForm.Button2Click(Sender: TObject);
begin
  lstMtreel.FullCollapse;
  ResizeForm;
end;

procedure TWorkForm.Button3Click(Sender: TObject);
begin
  fillGridHeader();
end;

procedure TWorkForm.cbb3Change(Sender: TObject);
// 动态设置连接的数据库
begin
  Q_ima02.Text := DZT[cbb3.ItemIndex];
  dba.TargetDatabaseId := DZT[cbb3.ItemIndex];
end;

procedure TWorkForm.MGridGetAlignment(Sender: TObject; ARow, ACol: Integer;
  var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow < 3 then
  begin
    HAlign := taCenter;
    VAlign := vtaCenter;
  end;
end;

procedure TWorkForm.MGridGetCellColor(Sender: TObject; ARow, ACol: Integer;
  AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
// 主GRID中汇总行字显示为红色，奇偶行区分开
begin
  // if MGrid.Cells[KeyCol, ARow] = SPCHAR then
  // AFont.Color := clRed;
  if (ARow mod 2) = 0 then
    ABrush.Color := MGrid.Color
  else
    ABrush.Color := clBtnFace;
end;

// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}
// 消息响应事件

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息
  if (StrComp(pchar(MsgName), pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        Exit; // 不是发给自己的消息，退出
      zhanghu := InParcel.GetStringGoods('LoginUser');
    end;
    Exit;
  end;
  // 获取程序校核信息
  if (StrComp(pchar(MsgName), pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        Exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    Exit;
  end;
  // 获取当前插件的权限信息 同步消息
  if (StrComp(pchar(MsgName), pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        Exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', PrivilegeCDS);
    end;
    Exit;
  end;
end;

{$ENDREGION}

end.

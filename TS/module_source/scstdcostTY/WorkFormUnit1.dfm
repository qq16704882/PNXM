object WorkForm: TWorkForm
  Left = 309
  Top = 196
  Align = alClient
  AutoSize = True
  BorderStyle = bsNone
  Caption = 'DLL'#20013#30340'Work'#31383#20307
  ClientHeight = 589
  ClientWidth = 1031
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object flwpnl1: TFlowPanel
    Left = 0
    Top = 0
    Width = 1031
    Height = 76
    Align = alTop
    TabOrder = 0
    object pnl1: TPanel
      Left = 1
      Top = 1
      Width = 248
      Height = 41
      TabOrder = 0
      Visible = False
      object rg1: TRadioGroup
        Left = 1
        Top = 1
        Width = 246
        Height = 39
        Align = alClient
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          #22810#26009#21495#25104#26412#21015#34920
          #21333#26009#21495#23637#33267#23614#38454)
        TabOrder = 0
      end
    end
    object pnl2: TPanel
      Left = 249
      Top = 1
      Width = 159
      Height = 41
      Caption = 'pnl2'
      TabOrder = 1
      object cbb3: TComboBox
        Left = 47
        Top = 11
        Width = 106
        Height = 20
        Style = csDropDownList
        TabOrder = 0
        OnChange = cbb3Change
        Items.Strings = (
          'GWGS'
          'GNGS'
          'YKGS'
          'SHBG'
          'PKGS')
      end
      object txt1: TStaticText
        Left = 6
        Top = 13
        Width = 40
        Height = 16
        Caption = #36134#22871#65306
        TabOrder = 1
      end
    end
    object pnl6: TPanel
      Left = 408
      Top = 1
      Width = 270
      Height = 41
      TabOrder = 2
      object Q_year: TLabeledEdit
        Left = 40
        Top = 11
        Width = 81
        Height = 20
        EditLabel.Width = 36
        EditLabel.Height = 12
        EditLabel.Caption = #24180#20221#65306
        LabelPosition = lpLeft
        TabOrder = 0
      end
      object Q_month: TLabeledEdit
        Left = 176
        Top = 11
        Width = 81
        Height = 20
        EditLabel.Width = 36
        EditLabel.Height = 12
        EditLabel.Caption = #26376#20221#65306
        LabelPosition = lpLeft
        TabOrder = 1
      end
    end
    object pnl8: TPanel
      Left = 678
      Top = 1
      Width = 185
      Height = 41
      Caption = 'pnl8'
      TabOrder = 3
      object Q_dept: TLabeledEdit
        Left = 68
        Top = 11
        Width = 97
        Height = 20
        EditLabel.Width = 60
        EditLabel.Height = 12
        EditLabel.Caption = #37096#38376#32534#21495#65306
        LabelPosition = lpLeft
        TabOrder = 0
      end
    end
    object pnl4: TPanel
      Left = 1
      Top = 42
      Width = 253
      Height = 41
      TabOrder = 4
      object Q_ima01: TLabeledEdit
        Left = 112
        Top = 8
        Width = 121
        Height = 20
        CharCase = ecUpperCase
        EditLabel.Width = 102
        EditLabel.Height = 12
        EditLabel.Caption = #26009#21495#65306'           '
        LabelPosition = lpLeft
        TabOrder = 0
      end
      object cbb1: TComboBox
        Left = 41
        Top = 8
        Width = 65
        Height = 20
        ItemIndex = 0
        TabOrder = 1
        Text = #31561#20110
        Items.Strings = (
          #31561#20110
          #21253#21547
          #24320#22836#20026
          #32467#23614#20026)
      end
    end
    object pnl7: TPanel
      Left = 254
      Top = 42
      Width = 259
      Height = 41
      TabOrder = 5
      Visible = False
      object Q_ima02: TLabeledEdit
        Left = 119
        Top = 8
        Width = 130
        Height = 20
        EditLabel.Width = 96
        EditLabel.Height = 12
        EditLabel.Caption = #21697#21517':           '
        LabelPosition = lpLeft
        TabOrder = 0
      end
      object cbb2: TComboBox
        Left = 57
        Top = 8
        Width = 64
        Height = 20
        ItemIndex = 0
        TabOrder = 1
        Text = #31561#20110
        Items.Strings = (
          #31561#20110
          #21253#21547
          #24320#22836#20026
          #32467#23614#20026)
      end
    end
    object Panel1: TPanel
      Left = 513
      Top = 42
      Width = 185
      Height = 41
      Caption = 'pnl8'
      TabOrder = 6
      object leBB: TLabeledEdit
        Left = 68
        Top = 8
        Width = 97
        Height = 20
        EditLabel.Width = 60
        EditLabel.Height = 12
        EditLabel.Caption = #25104#26412#29256#26412#65306
        Enabled = False
        LabelPosition = lpLeft
        TabOrder = 0
        Text = '1'
      end
    end
  end
  object pnl5: TPanel
    Left = 0
    Top = 76
    Width = 1031
    Height = 41
    Align = alTop
    Alignment = taLeftJustify
    Caption = #27880#24847#65306#24314#35758#37319#29992#26009#21495#31561#20110'/'#24320#22836#20026#26597#35810#26465#20214#65292#21487#25552#39640#25928#29575
    TabOrder = 1
    object btn1: TButton
      Left = 535
      Top = 10
      Width = 75
      Height = 25
      Caption = #26597#35810
      TabOrder = 0
      OnClick = btn1Click
    end
    object btn2: TButton
      Left = 903
      Top = 10
      Width = 75
      Height = 25
      Caption = #27719#20986'EXCEL'
      TabOrder = 1
      OnClick = btn2Click
    end
    object Button1: TButton
      Left = 616
      Top = 10
      Width = 75
      Height = 25
      Caption = #26641#23637#24320
      TabOrder = 2
      Visible = False
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 697
      Top = 10
      Width = 75
      Height = 25
      Caption = #26641#25910#32553
      TabOrder = 3
      Visible = False
      OnClick = Button2Click
    end
    object Cost_Run: TButton
      Left = 327
      Top = 10
      Width = 75
      Height = 25
      Caption = #35745#31639
      TabOrder = 4
      Visible = False
      OnClick = Cost_RunClick
    end
    object Cost_tree: TButton
      Left = 778
      Top = 10
      Width = 75
      Height = 25
      Caption = #26641#22411#26174#31034
      TabOrder = 5
      Visible = False
      OnClick = Cost_treeClick
    end
    object Cost_BRun: TButton
      Left = 408
      Top = 10
      Width = 75
      Height = 25
      Caption = #25209#37327#35745#31639
      TabOrder = 6
      Visible = False
      OnClick = Cost_BRunClick
    end
    object Button3: TButton
      Left = 220
      Top = 6
      Width = 75
      Height = 25
      Caption = #22635#20805#34920#22836
      TabOrder = 7
      Visible = False
      OnClick = Button3Click
    end
  end
  object pgc1: TPageControl
    Left = 0
    Top = 117
    Width = 1031
    Height = 472
    ActivePage = ts1
    Align = alClient
    TabOrder = 2
    TabStop = False
    OnChange = pgc1Change
    object ts1: TTabSheet
      Caption = #25972#20307#26174#31034
      object MGrid: TAdvStringGrid
        Left = 0
        Top = 0
        Width = 1023
        Height = 444
        Cursor = crDefault
        Align = alClient
        ColCount = 15
        DrawingStyle = gdsClassic
        FixedCols = 0
        FixedRows = 3
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 0
        HoverRowCells = [hcNormal, hcSelected]
        OnGetCellColor = MGridGetCellColor
        OnGetAlignment = MGridGetAlignment
        ActiveCellFont.Charset = DEFAULT_CHARSET
        ActiveCellFont.Color = clWindowText
        ActiveCellFont.Height = -11
        ActiveCellFont.Name = 'Tahoma'
        ActiveCellFont.Style = [fsBold]
        ControlLook.FixedGradientHoverFrom = clGray
        ControlLook.FixedGradientHoverTo = clWhite
        ControlLook.FixedGradientDownFrom = clGray
        ControlLook.FixedGradientDownTo = clSilver
        ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownHeader.Font.Color = clWindowText
        ControlLook.DropDownHeader.Font.Height = -11
        ControlLook.DropDownHeader.Font.Name = 'Tahoma'
        ControlLook.DropDownHeader.Font.Style = []
        ControlLook.DropDownHeader.Visible = True
        ControlLook.DropDownHeader.Buttons = <>
        ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownFooter.Font.Color = clWindowText
        ControlLook.DropDownFooter.Font.Height = -11
        ControlLook.DropDownFooter.Font.Name = 'Tahoma'
        ControlLook.DropDownFooter.Font.Style = []
        ControlLook.DropDownFooter.Visible = True
        ControlLook.DropDownFooter.Buttons = <>
        Filter = <>
        FilterDropDown.Font.Charset = DEFAULT_CHARSET
        FilterDropDown.Font.Color = clWindowText
        FilterDropDown.Font.Height = -11
        FilterDropDown.Font.Name = 'Tahoma'
        FilterDropDown.Font.Style = []
        FilterDropDown.TextChecked = 'Checked'
        FilterDropDown.TextUnChecked = 'Unchecked'
        FilterDropDownClear = '(All)'
        FilterEdit.TypeNames.Strings = (
          'Starts with'
          'Ends with'
          'Contains'
          'Not contains'
          'Equal'
          'Not equal'
          'Larger than'
          'Smaller than'
          'Clear')
        FixedColWidth = 20
        FixedRowHeight = 22
        FixedFont.Charset = DEFAULT_CHARSET
        FixedFont.Color = clWindowText
        FixedFont.Height = -11
        FixedFont.Name = 'Tahoma'
        FixedFont.Style = [fsBold]
        FloatFormat = '%.2f'
        HoverButtons.Buttons = <>
        HoverButtons.Position = hbLeftFromColumnLeft
        HTMLSettings.ImageFolder = 'images'
        HTMLSettings.ImageBaseName = 'img'
        PrintSettings.DateFormat = 'dd/mm/yyyy'
        PrintSettings.Font.Charset = DEFAULT_CHARSET
        PrintSettings.Font.Color = clWindowText
        PrintSettings.Font.Height = -11
        PrintSettings.Font.Name = 'Tahoma'
        PrintSettings.Font.Style = []
        PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
        PrintSettings.FixedFont.Color = clWindowText
        PrintSettings.FixedFont.Height = -11
        PrintSettings.FixedFont.Name = 'Tahoma'
        PrintSettings.FixedFont.Style = []
        PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
        PrintSettings.HeaderFont.Color = clWindowText
        PrintSettings.HeaderFont.Height = -11
        PrintSettings.HeaderFont.Name = 'Tahoma'
        PrintSettings.HeaderFont.Style = []
        PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
        PrintSettings.FooterFont.Color = clWindowText
        PrintSettings.FooterFont.Height = -11
        PrintSettings.FooterFont.Name = 'Tahoma'
        PrintSettings.FooterFont.Style = []
        PrintSettings.PageNumSep = '/'
        ScrollWidth = 16
        SearchFooter.FindNextCaption = 'Find &next'
        SearchFooter.FindPrevCaption = 'Find &previous'
        SearchFooter.Font.Charset = DEFAULT_CHARSET
        SearchFooter.Font.Color = clWindowText
        SearchFooter.Font.Height = -11
        SearchFooter.Font.Name = 'Tahoma'
        SearchFooter.Font.Style = []
        SearchFooter.HighLightCaption = 'Highlight'
        SearchFooter.HintClose = 'Close'
        SearchFooter.HintFindNext = 'Find next occurrence'
        SearchFooter.HintFindPrev = 'Find previous occurrence'
        SearchFooter.HintHighlight = 'Highlight occurrences'
        SearchFooter.MatchCaseCaption = 'Match case'
        SortSettings.DefaultFormat = ssAutomatic
        SortSettings.Column = 0
        SortSettings.SortOnVirtualCells = False
        Version = '8.1.3.0'
        WordWrap = False
        ColWidths = (
          20
          20
          20
          28
          20
          20
          20
          20
          20
          20
          20
          20
          20
          20
          20)
        RowHeights = (
          22
          22
          22
          22
          22
          22
          22
          22
          22
          22)
      end
    end
    object ts2: TTabSheet
      Caption = #26641#22411#26174#31034
      ImageIndex = 1
      TabVisible = False
      object lstMtreel: TcxTreeList
        Left = 0
        Top = 0
        Width = 1023
        Height = 444
        Align = alClient
        Bands = <
          item
          end>
        Navigator.Buttons.CustomButtons = <>
        OptionsData.Editing = False
        OptionsData.Deleting = False
        OptionsView.GridLines = tlglBoth
        Styles.ContentOdd = cxStyle2
        TabOrder = 0
        object lstMtreelColumn1: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #20803#20214
          DataBinding.ValueType = 'String'
          Position.ColIndex = 0
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn2: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #21697#21517
          DataBinding.ValueType = 'String'
          Width = 160
          Position.ColIndex = 1
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn3: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #35268#26684
          DataBinding.ValueType = 'String'
          Width = 160
          Position.ColIndex = 2
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn4: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #21333#20301
          DataBinding.ValueType = 'String'
          Width = 40
          Position.ColIndex = 3
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn5: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #29366#24577
          DataBinding.ValueType = 'String'
          Width = 40
          Position.ColIndex = 4
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn6: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #24037#20316#31449
          DataBinding.ValueType = 'String'
          Width = 60
          Position.ColIndex = 5
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn7: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #24037#20316#31449#21517#31216
          DataBinding.ValueType = 'String'
          Position.ColIndex = 6
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn8: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #29992#37327
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 7
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn9: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = 'BOM'#25439#32791'(%)'
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 8
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn10: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #21152#24037#25439#32791'(%)'
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 9
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn11: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #21333#20215
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 10
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn12: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = 'TIME'
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 11
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn13: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = 'EQV'
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 12
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn14: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #21407#26448#26009#22686
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 13
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn15: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #21407#26448#26009
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 14
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn16: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #36741#26009#22686
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 15
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn17: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #36741#26009
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 16
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn18: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #27169#20855#22686
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 17
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn19: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #27169#20855
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 18
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn20: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #38468#20214#22686
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 19
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn21: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #38468#20214
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 20
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn22: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #21253#35013#22686
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 21
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn23: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #21253#35013
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 22
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn24: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #30452#25509#20154#24037#22686
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 23
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn25: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #30452#25509#20154#24037
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 24
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn26: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #38388#25509#20154#24037#22686
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 25
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn27: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #38388#25509#20154#24037
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 26
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn28: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #30005#36153#22686
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 27
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn29: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #30005#36153
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 28
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn30: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #22825#28982#27668#22686
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 29
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn31: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #22825#28982#27668
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 30
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn32: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #25240#26087#22686
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 31
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn33: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #25240#26087
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 32
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn34: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #20854#20182#22686
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 33
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn35: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #20854#20182
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 34
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn36: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #21512#35745
          DataBinding.ValueType = 'Float'
          Position.ColIndex = 35
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object lstMtreelColumn37: TcxTreeListColumn
          Visible = False
          Caption.AlignHorz = taCenter
          Caption.Text = 'T'
          DataBinding.ValueType = 'String'
          Position.ColIndex = 36
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
      end
    end
  end
  object Memo1: TMemo
    Left = 432
    Top = 240
    Width = 369
    Height = 81
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
    Visible = False
  end
  object WinMessage: TQBWinMessages
    OnMsgArrived = WinMessageMsgArrived
    Left = 41
    Top = 426
  end
  object dba: TRemoteUniDac
    TaskTimeout = 30
    EnableBCD = False
    EnableFMTBCD = False
    EnableLoadBalance = False
    Left = 80
    Top = 430
  end
  object cxStyleRepository: TcxStyleRepository
    Left = 752
    Top = 344
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor]
      Color = 15784893
    end
    object DELS: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsStrikeOut]
    end
    object AddS: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clRed
    end
    object ModS: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic]
    end
    object DefaultS: TcxStyle
    end
  end
  object PrivilegeCDS: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 160
    Top = 424
  end
  object TQ: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 480
  end
  object CDS: TClientDataSet
    Aggregates = <>
    Params = <>
    ReadOnly = True
    Left = 124
    Top = 429
  end
  object TCDS: TClientDataSet
    Aggregates = <>
    Params = <>
    ReadOnly = True
    Left = 156
    Top = 501
  end
  object PopupMenu1: TPopupMenu
    Left = 236
    Top = 221
    object N1: TMenuItem
      Caption = #26641#22411#26174#31034
      OnClick = N1Click
    end
  end
  object agexp: TAdvGridExcelIO
    AdvStringGrid = MGrid
    Options.ExportOverwriteMessage = 'File %s already exists'#13'Ok to overwrite ?'
    Options.ExportRawRTF = False
    UseUnicode = False
    GridStartRow = 0
    GridStartCol = 0
    Version = '3.13'
    Left = 652
    Top = 476
  end
end

object WorkForm: TWorkForm
  Left = 540
  Top = 0
  Align = alClient
  AutoSize = True
  BorderStyle = bsNone
  ClientHeight = 589
  ClientWidth = 1031
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object MasterP: TPanel
    Left = 0
    Top = 0
    Width = 1031
    Height = 589
    Align = alClient
    FullRepaint = False
    TabOrder = 0
    object MGrid: TcxGrid
      AlignWithMargins = True
      Left = 4
      Top = 105
      Width = 1023
      Height = 480
      Align = alClient
      TabOrder = 0
      LookAndFeel.NativeStyle = False
      object MTV: TcxGridBandedTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        Styles.Background = cxStyle1
        Styles.ContentOdd = cxStyle1
        Styles.BandHeader = cxStyle2
        Bands = <
          item
            Caption = #24211#23384#24046#24322#34920
          end>
        object MTVColumn1: TcxGridBandedColumn
          Caption = #24207#21495
          Options.Filtering = False
          Options.Sorting = False
          Width = 40
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn2: TcxGridBandedColumn
          Caption = #21697#21495
          Width = 100
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object MTVColumn3: TcxGridBandedColumn
          Caption = #21697#21517
          Width = 150
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object MTVColumn4: TcxGridBandedColumn
          Caption = #35268#26684
          Options.Sorting = False
          Width = 200
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object MTVColumn5: TcxGridBandedColumn
          Caption = #24403#26399#20986#24211#25968#37327
          Options.Filtering = False
          Options.Sorting = False
          Width = 80
          Position.BandIndex = 0
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
        object MTVColumn6: TcxGridBandedColumn
          Caption = #24403#21069#20837#24211#25968#37327
          Options.Filtering = False
          Options.Sorting = False
          Width = 80
          Position.BandIndex = 0
          Position.ColIndex = 5
          Position.RowIndex = 0
        end
        object MTVColumn7: TcxGridBandedColumn
          Caption = #21512#26684#24211#23384#25968#37327
          Options.Filtering = False
          Options.Sorting = False
          Width = 80
          Position.BandIndex = 0
          Position.ColIndex = 6
          Position.RowIndex = 0
        end
        object MTVColumn8: TcxGridBandedColumn
          Caption = #24453#39564#25968#37327
          Options.Filtering = False
          Options.Sorting = False
          Width = 80
          Position.BandIndex = 0
          Position.ColIndex = 7
          Position.RowIndex = 0
        end
        object MTVColumn9: TcxGridBandedColumn
          Caption = #24211#23384#25968#37327
          Options.Filtering = False
          Options.Sorting = False
          Width = 80
          Position.BandIndex = 0
          Position.ColIndex = 8
          Position.RowIndex = 0
        end
        object MTVColumn10: TcxGridBandedColumn
          Caption = #29366#24577
          Styles.OnGetContentStyle = MTVColumn10StylesGetContentStyle
          Position.BandIndex = 0
          Position.ColIndex = 9
          Position.RowIndex = 0
        end
        object MTVColumn11: TcxGridBandedColumn
          Caption = #26368#20302#24211#23384#25968#37327
          Options.Filtering = False
          Options.Sorting = False
          Width = 80
          Position.BandIndex = 0
          Position.ColIndex = 10
          Position.RowIndex = 0
        end
        object MTVColumn12: TcxGridBandedColumn
          Caption = #26368#39640#24211#23384#25968#37327
          Options.Filtering = False
          Options.Sorting = False
          Width = 80
          Position.BandIndex = 0
          Position.ColIndex = 11
          Position.RowIndex = 0
        end
      end
      object MGridLevel1: TcxGridLevel
        GridView = MTV
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 1029
      Height = 101
      Align = alTop
      BevelKind = bkTile
      BevelOuter = bvNone
      TabOrder = 1
      object Panel2: TPanel
        Left = 0
        Top = 61
        Width = 1025
        Height = 36
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 27
          Top = 11
          Width = 282
          Height = 12
          Caption = #24320#22987#26102#38388#65306'                           '#20179#24211#32534#21495#65306
        end
        object btime: TDateTimePicker
          Left = 95
          Top = 7
          Width = 105
          Height = 21
          Date = 43089.655190486110000000
          Time = 43089.655190486110000000
          TabOrder = 0
        end
        object ckbhcb: TComboBox
          Left = 330
          Top = 7
          Width = 100
          Height = 20
          TabOrder = 1
          Text = #35831#36873#25321#20179#24211
          OnChange = ckbhcbChange
        end
        object ckmccb: TComboBox
          Left = 460
          Top = 7
          Width = 149
          Height = 21
          Style = csSimple
          Enabled = False
          TabOrder = 2
          Items.Strings = (
            '02003-'#26611#23663#36741#26009#19968#24211
            '02002-'#26611#23663#36741#26009#20108#24211
            '03004-'#35199#21378#36741#26009#19968#24211
            '04005-'#33829#21475#36741#26009#19968#24211)
        end
        object ljHead: TLabeledEdit
          Left = 742
          Top = 7
          Width = 255
          Height = 20
          EditLabel.Width = 96
          EditLabel.Height = 12
          EditLabel.Caption = #26009#20214#32534#21495#24320#22836#20026#65306
          LabelPosition = lpLeft
          TabOrder = 3
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 1025
        Height = 57
        Align = alTop
        TabOrder = 1
        object RunQuery: TButton
          Left = 656
          Top = 7
          Width = 133
          Height = 44
          Caption = #26597#35810
          Enabled = False
          TabOrder = 0
          OnClick = RunQueryClick
        end
        object ToXLS: TButton
          Left = 816
          Top = 7
          Width = 133
          Height = 44
          Caption = #23548#20986'excel'
          TabOrder = 1
          OnClick = ToxlsClick
        end
        object ztRG: TRadioGroup
          Left = 1
          Top = 1
          Width = 593
          Height = 55
          Align = alLeft
          Caption = #21487#36873#36134#22871
          TabOrder = 2
          OnClick = ztRGClick
        end
        object Button1: TButton
          Left = 968
          Top = 24
          Width = 75
          Height = 25
          Caption = 'Button1'
          TabOrder = 3
          Visible = False
          OnClick = Button1Click
        end
      end
    end
    object Memo1: TMemo
      Left = 384
      Top = 200
      Width = 361
      Height = 193
      Lines.Strings = (
        'Memo1')
      TabOrder = 2
      Visible = False
    end
  end
  object Cds: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 80
    Top = 424
  end
  object WinMessage: TQBWinMessages
    OnMsgArrived = WinMessageMsgArrived
    Left = 41
    Top = 426
  end
  object dba: TRemoteUniDac
    TaskTimeout = 30
    EnableBCD = False
    EnableFMTBCD = False
    EnableLoadBalance = False
    Left = 176
    Top = 398
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 744
    Top = 344
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clMenuBar
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object stHigh: TcxStyle
      AssignedValues = [svColor]
      Color = clRed
    end
    object stLow: TcxStyle
      AssignedValues = [svColor]
      Color = clMoneyGreen
    end
  end
end

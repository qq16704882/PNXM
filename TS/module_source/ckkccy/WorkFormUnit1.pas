﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters,cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridCustomView, cxGrid, cxClasses, math;

type
  TWorkForm = class(TForm)
    Cds: TClientDataSet;
    MasterP: TPanel;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    MGrid: TcxGrid;
    MTV: TcxGridBandedTableView;
    MGridLevel1: TcxGridLevel;
    Panel1: TPanel;
    Panel2: TPanel;
    ztRG: TRadioGroup;
    Label1: TLabel;
    btime: TDateTimePicker;
    ckbhcb: TComboBox;
    ckmccb: TComboBox;
    ljHead: TLabeledEdit;
    RunQuery: TButton;
    ToXLS: TButton;
    Panel3: TPanel;
    MTVColumn1: TcxGridBandedColumn;
    MTVColumn2: TcxGridBandedColumn;
    MTVColumn3: TcxGridBandedColumn;
    MTVColumn4: TcxGridBandedColumn;
    MTVColumn5: TcxGridBandedColumn;
    MTVColumn6: TcxGridBandedColumn;
    MTVColumn7: TcxGridBandedColumn;
    MTVColumn8: TcxGridBandedColumn;
    MTVColumn9: TcxGridBandedColumn;
    MTVColumn10: TcxGridBandedColumn;
    MTVColumn11: TcxGridBandedColumn;
    MTVColumn12: TcxGridBandedColumn;
    stHigh: TcxStyle;
    stLow: TcxStyle;
    Button1: TButton;
    Memo1: TMemo;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure RunQueryClick(Sender: TObject);
    procedure ToxlsClick(Sender: TObject);
    procedure ToXlsMXClick(Sender: TObject);
    procedure ckbhcbChange(Sender: TObject);
    procedure ztRGClick(Sender: TObject);
    procedure MTVColumn10StylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure Button1Click(Sender: TObject);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function RunCDS(SQL: string): Boolean;
    procedure FillmGrid(Q: TClientDataSet);
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure IniCKBH(zt: string);
    procedure IniZtRG(zt: string);
    procedure GetPrivilege(TFlag, TID, UID: string);
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'CKKCCY';
  testZT = 'GWGS,GNGS';
  sHigh = '高';
  sLow = '低';
  sStd = '正常';
  CDBID = 'TS'; // 设置使用的数据源

var
  zhanghu: string;
  softWareAuth: Boolean;

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.IniCKBH(zt: string);
// 查询选定账套的仓库信息
begin
  if zt = '' then
    exit;
  if Not RunCDS
    (format('SELECT wm_concat(imd01) imd01,wm_concat(imd02) imd02 FROM %s.imd_file WHERE LENGTH(imd01) = 5',
    [zt])) then
    exit;

  ckbhcb.Items.DelimitedText := Cds.FieldByName('imd01').AsString;
  ckmccb.Items.DelimitedText := Cds.FieldByName('imd02').AsString;
  ckbhcb.ItemIndex := 0;
  ckmccb.ItemIndex := 0;
end;

procedure TWorkForm.IniZtRG(zt: string);
// 根据权限设置可以选择的账套，并动态设置itemindex, columns值
begin
  ztRG.Items.DelimitedText := zt;
  ztRG.Columns := ztRG.Items.Count;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');

  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');
  self.Caption := InputParcel.GetAnsiStringGoods('FormCaption');
  // caption记录插件的ID

  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);

  // 界面初始化
  // 默认为当天
  btime.Date := now;
end;

// -----------------------------------------------------------------------------
// 业务代码
Const
  MSQL =
'SELECT ROWNUM,img01,ima02,ima021,nvl(ck,0) ck,nvl(rk,0) rk,img10,dy,(img10+dy) kc,'''' sst,nvl(tc_imaa03,0) minkc,nvl(tc_imaa04,0) maxkc ' + sLineBreak +
'FROM    ' + sLineBreak +
'  (                                                                                                    ' + sLineBreak +
'    SELECT img01,img02,SUM(img10) img10,SUM(rk) rk,SUM(ck) ck,SUM(dy) dy FROM                           ' + sLineBreak +
'    (                                                                                                   ' + sLineBreak +
'      SELECT xx.img01,xx.img02,xx.img03,xx.img04,yy.img10,nvl(rk,0) rk,nvl(ck,0) ck,nvl(dy,0) dy FROM  ' + sLineBreak +
'      (  ' + sLineBreak +
'        SELECT img01,img02,img03,img04 FROM  %s.img_file WHERE %1:s AND img02 = ''%2:s''  ' + sLineBreak +
'        UNION  ' + sLineBreak +
'        SELECT tc_imaa01,tc_imaa02,'' '','' '' FROM %0:s.tc_imaa_file WHERE  %4:s AND tc_imaa02 = ''%2:s''  --有安全库存的记录  ' + sLineBreak +
'      ) xx                              ' + sLineBreak +
'      LEFT JOIN (SELECT tlf01,tlf902,tlf903,tlf904,nvl(SUM(DECODE(tlf907,1,tlf10,0)),0) rk,nvl(SUM(DECODE(tlf907,-1,tlf10,0)),0) ck    ' + sLineBreak +
'        FROM %0:s.tlf_file WHERE tlf06 >=  %3:s GROUP BY tlf01,tlf902,tlf903,tlf904)  ' + sLineBreak +
'          ON tlf01 = xx.img01 AND tlf902 = xx.img02 AND tlf903 = xx.img03 AND tlf904 = xx.img04   ' + sLineBreak +
'      LEFT JOIN (SELECT rvv31,rvv32,rvv33,rvv34,sum(rvv82) dy  ' + sLineBreak +
'        FROM %0:s.rvu_file,%0:s.rvv_file WHERE rvu01 = rvv01 AND rvv32 = ''%2:s'' AND rvuconf =''N'' GROUP BY rvv31,rvv32,rvv33,rvv34)    ' + sLineBreak +
'          ON rvv31 = xx.img01 AND rvv32 = xx.img02 AND rvv33 = xx.img03 AND rvv34 = xx.img04  ' + sLineBreak +
'      LEFT JOIN %0:s.img_file yy ON yy.img01 = xx.img01 AND yy.img02 = xx.img02 AND yy.img03 = xx.img03 AND yy.img04 = xx.img04                          ' + sLineBreak +
'    ) aa   GROUP BY img01,img02,img03,img04                                          ' + sLineBreak +
'  ) bb                                                          ' + sLineBreak +
'  LEFT JOIN %0:s.tc_imaa_file ON tc_imaa01 = bb.img01 AND tc_imaa02 = bb.img02    ' + sLineBreak +
'  LEFT JOIN %0:s.ima_file ON ima01 = bb.img01  ' + sLineBreak +
'WHERE  ' + sLineBreak +
'  ck > 0 OR rk >0 OR img10 <>0 OR dy >0 OR tc_imaa03 >0 OR tc_imaa04 >0 ';

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

function TWorkForm.RunCDS(SQL: string): Boolean;
begin
  Result := false;
  Cds.close;
  if not dba.ReadDataset(SQL, Cds) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    Cds.active := true;
    Result := true;
  end;
end;

procedure TWorkForm.RunQueryClick(Sender: TObject);
var
  ljF1, ljF2, TimeF, SQL: string;
begin
  if Not softWareAuth then
  begin
    application.MessageBox
      ('The application raise a Error ,the address is : 0x34058842', 'Error');
    exit;
  end;
  // 主查询
  screen.Cursor := crSQLWait;
  try
    if ljHead.Text <> '' then
      ljF1 := ' img01 LIKE ''' + trim(ljHead.Text) + '%'' '
    else
      ljF1 := ' 1=1 ';
    ljF2 := StringReplace(ljF1, 'img01', 'tc_imaa01', [rfReplaceAll]);
    TimeF := ' to_date(''' + FormatDateTime('yyyymmdd', btime.Date) +
      ''',''yyyymmdd'')  ';
    SQL := format(MSQL, [ztRG.Items[ztRG.ItemIndex], ljF1, ckbhcb.Text,
      TimeF, ljF2]);
//      memo1.Lines.Text := sql;
//      exit;
    RunCDS(SQL);
    FillmGrid(Cds);
  finally
    ResizeForm;
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.ToxlsClick(Sender: TObject);
//导入excel
var
  str: string;
  i,j, m, y: Integer;
  excelapp, sheet: Variant;
  colName, Bcaption, XMRQ: string; // 项目编号 / 列标题 / 尾部标识行内容 /筛选时间
  FSQL, SQL: string;
begin
  screen.Cursor := crAppStart;

  str := '';
    // 时间范围
    XMRQ := str + '筛选时间从： ' + char(9) + FormatDateTime('yyyy/mm/dd', bTime.Date)
    + char(9) + '  到： ' + char(9) + FormatDateTime('yyyy/mm/dd', date);

    colName := '';
    Bcaption := '';
    for i := 0 to MTV.ColumnCount - 1 do
    // 显示列标题
    begin
    colName := colName + MTV.Columns[i].Caption + char(9);
    Bcaption := Bcaption + '================' + char(9);
    end;

    // 显示内容
    str := '仓库名称 ： ' + char(9) + ckmccb.Text + #13 +
           XMRQ  + #13 +
           ColName + #13 +
           Bcaption + #13;

    for i := 0 to MTV.DataController.RecordCount - 1 do
    begin
    for j := 0 to MTV.ColumnCount - 1 do
    begin
    if MTV.DataController.Values[i, j] <> NULL then
    str := str + MTV.DataController.Values[i, j] + char(9)
    else
    str := str + '' + char(9);
    end;
    str := str + #13;
    application.ProcessMessages;
    end;

  try
    try
      clipboard.Clear;
      clipboard.Open;
      clipboard.AsText := str;
      clipboard.close;
      excelapp := createoleobject('excel.application');
      excelapp.workbooks.add(1);
      sheet := excelapp.workbooks[1].worksheets[1];
      sheet.name := 'sheet1';
      sheet.paste;
      clipboard.Clear;
      excelapp.Visible := true;
    except
      application.MessageBox('Excel未安装', '提示');
    end;
  finally
    clipboard.Clear;
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.ToXlsMXClick(Sender: TObject);
begin
  // screen.Cursor := crAppStart;
  // str := '';
  // MGrid.ExpandAll;
  //
  // // 项目筛选记录
  // str := '筛选项目编号包括 ： ' + char(9) + StringReplace(xmLE.Text, ',', char(9),
  // [rfReplaceAll]) + #13 + #13;
  // // 时间范围
  // str := str + '筛选时间从： ' + char(9) + FormatDateTime('yyyy/mm/dd', beginT.Date) +
  // char(9) + '  到： ' + char(9) + FormatDateTime('yyyy/mm/dd', endT.Date) + #13
  // + #13 + #13;
  //
  // for i := 1 to MGrid.ColCount - 1 do
  // // 显示列标题
  // begin
  // if S2E(i) then
  // str := str + MGrid.ColumnHeaders[i] + char(9);
  // end;
  // str := str + #13;
  //
  // // 显示内容
  // for i := 1 to MGrid.RowCount - 1 do
  // begin
  // // 若不是汇总项，则直接跳过 汇总项判断：GRID单号列字值为'-'
  // if MGrid.Cells[KeyCol, i] = SPCHAR then
  // continue;
  // for j := 1 to MGrid.ColCount - 1 do
  // begin
  // if S2E(j) then
  // str := str + MGrid.Cells[j, i] + char(9);
  // end;
  // str := str + #13;
  // Application.ProcessMessages;
  // end;
  // try
  // try
  // clipboard.Clear;
  // clipboard.Open;
  // clipboard.AsText := str;
  // clipboard.Close;
  // excelapp := createoleobject('excel.application');
  // excelapp.workbooks.add(1);
  // sheet := excelapp.workbooks[1].worksheets[1];
  // sheet.name := 'sheet1';
  // sheet.paste;
  // clipboard.Clear;
  // excelapp.Visible := true;
  // MGrid.ContractAll;
  // screen.Cursor := crDefault;
  // except
  // showmessage('Excel未安装');
  // end;
  // finally
  // clipboard.Clear;
  // screen.Cursor := crDefault;
  // end;
end;

procedure TWorkForm.Button1Click(Sender: TObject);
begin
  application.MessageBox(Pchar(self.Caption), 'test');
end;

procedure TWorkForm.ckbhcbChange(Sender: TObject);
begin
  ckmccb.ItemIndex := ckbhcb.ItemIndex;
end;

procedure TWorkForm.ztRGClick(Sender: TObject);
begin
  if ztRG.ItemIndex = -1 then
    exit;
  IniCKBH(ztRG.Items[ztRG.ItemIndex]);
  RunQuery.Enabled := true;
end;

procedure TWorkForm.FillmGrid(Q: TClientDataSet);
var
  i, j: Integer;
begin
  MTV.DataController.RecordCount := 0;
  if Q.RecordCount = 0 then
    exit;
  Q.First;
  MTV.BeginUpdate();
  while not Q.Eof do
  begin
    i := MTV.DataController.AppendRecord;
    for j := 0 to MTV.ColumnCount - 1 do
      MTV.DataController.Values[i, j] := Q.Fields[j].AsString;

    if Q.FieldByName('kc').AsFloat > Q.FieldByName('MaxKc').AsFloat then
      MTV.DataController.Values[i, 9] := sHigh
    else if Q.FieldByName('kc').AsFloat < Q.FieldByName('MinKc').AsFloat then
      MTV.DataController.Values[i, 9] := sLow
    else
      MTV.DataController.Values[i, 9] := sStd;
    Q.Next;
  end;
  MTV.EndUpdate;
end;

procedure TWorkForm.MTVColumn10StylesGetContentStyle
  (Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
// 改变某一列的显示类型
var
  v: Variant;
begin
  v := ARecord.Values[9];
  if (v = NULL) or (v = '') then
    exit;
  if v = sHigh then
    AStyle := stHigh;
  if v = sLow then
    AStyle := stLow;
end;

// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}
// 消息响应事件

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  LogMsg('2501发送权限' + TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息
  if (StrComp(Pchar(MsgName), Pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      zhanghu := InParcel.GetStringGoods('LoginUser');
      // 获取插件权限信息
      GetPrivilege('MENU', self.Caption, zhanghu);
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(Pchar(MsgName), Pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
  // 获取当前插件的权限信息
  if (StrComp(Pchar(MsgName), Pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      InParcel.GetCDSGoods('QX', Cds);
      IniZtRG(Cds.FieldByName('zt').AsString);
    end;
    exit;
  end;
end;
{$ENDREGION}

end.

﻿//
// resident.dpr -- 通用客户端常驻内存模块示例工程
//                 Version 1.00 Unicode Edition
//                 Author: Jopher(W.G.Z)
//                 QQ: 779545524
//                 Email: Jopher@189.cn
//                 Homepage: http://www.quickburro.com/
//                 Copyright(C) Jopher Software Studio
//
library resident;

uses
   fastmm4,
   DataModuleUnit in 'DataModuleUnit.pas' {DM: TDataModule},
   {$IF CompilerVersion>=23.0}Winapi.Windows{$ELSE}Windows{$IFEND},
   {$IF CompilerVersion>=23.0}System.SysUtils{$ELSE}SysUtils{$IFEND},
   {$IF CompilerVersion>=23.0}System.Classes{$ELSE}Classes{$IFEND},
   {$IF CompilerVersion>=23.0}Vcl.Forms{$ELSE}Forms{$IFEND},
   {$IF CompilerVersion>=23.0}Vcl.ExtCtrls{$ELSE}ExtCtrls{$IFEND},
   {$IFDEF UNICODE}
   {$IF CompilerVersion>=23.0}System.AnsiStrings{$ELSE}AnsiStrings{$IFEND},
   {$ENDIF}
   UserConnection,
   QBParcel,
   DllSpread;

//
// 全局变量，以管理多实例...
var
   OldApp: TApplication;
   OldScreen: TScreen;

{$R *.res}

//
// 初始化DLL模块...
function DllFormInit(AppPtr: integer; ScreenPtr: integer; ConnPtr: integer): boolean; stdcall;
begin
   Application:=TApplication(Pointer(AppPtr));
//   Screen:=TScreen(Pointer(ScreenPtr));
//
// 创建一个DataModule容器...
   DM:=TDM.Create(nil);
   DM.UserConn:=TUserConnection(pointer(ConnPtr));
//
// 开启消息接收器...
   DM.Receiver.UserConnection:=DM.UserConn;
   DM.Receiver.Active:=true;
//
   result:=true;
end;

//
// 初始与善后...
procedure DLLHandler(Reason: integer);
begin
  case Reason of
    //
    // 加载DLL时...
    DLL_Process_Attach:
      begin
         OldApp:=application;
         OldScreen:=Screen;
      end;
    //
    // 卸载DLL时...
    DLL_PROCESS_DETACH:
      begin
         FreeAndNil(DM);
         Application:=OldApp;
         Screen:=OldScreen;
      end;
  end;
end;

//
// 导出...
exports
   DllFormInit;

//
// 初始化...
begin
   DLLProc:=@DLLHandler;
   DLLHandler(DLL_Process_Attach);
   IsMultiThread:=true;
end.

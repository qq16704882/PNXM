//
// resident.dpr -- 通用客户端常驻内存模块示例工程
//                 Version 1.00 Unicode Edition
//                 Author: Jopher(W.G.Z)
//                 QQ: 779545524
//                 Email: Jopher@189.cn
//                 Homepage: http://www.quickburro.com/
//                 Copyright(C) Jopher Software Studio
//
unit DataModuleUnit;

interface

uses
   {$IF CompilerVersion>=23.0}System.SysUtils{$ELSE}SysUtils{$IFEND},
   {$IF CompilerVersion>=23.0}System.Classes{$ELSE}Classes{$IFEND},
   {$IF CompilerVersion>=23.0}Vcl.Forms{$ELSE}Forms{$IFEND},
   UserConnection,
   QBWinMessages,
   MsgReceiver,
   DllSpread,
   TaskCommon,
   QBParcel;

type
  TDM = class(TDataModule)
    Receiver: TMsgReceiver;
    WinMsgs: TQBWinMessages;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure LogMsg(aMsg: AnsiString);
    procedure ReceiverAllConnectionsMessage(MsgParcel: TQBParcel; FromId: String);
  private
    { Private declarations }
  public
    { Public declarations }
    UserConn: TUserConnection;
  end;

var
  DM: TDM;

implementation

{$R *.dfm}

//
// 创建容器时，初始化Windows消息收发器...
procedure TDM.DataModuleCreate(Sender: TObject);
begin
   InitSocketPool;
   WinMsgs.RegisterUserMessage('QBClient_LogMsg');
   LogMsg('!!! 常驻内存示例模块加载了！');
end;

//
// 容器释放时...
procedure TDM.DataModuleDestroy(Sender: TObject);
begin
   FreeSocketPool;
   WinMsgs.RemoveUserMessage('QBClient_LogMsg');
   Receiver.Active:=false;
end;

//
// 向主程序回传一个日志...
procedure TDM.LogMsg(aMsg: AnsiString);
var
   j: integer;
begin
   j:=str2mem(aMsg);
   WinMsgs.PostUserMessage('QBClient_LogMsg',integer(application.Handle),j);
end;

//
// 作为示例，接收一个消息，并回写到日志...
procedure TDM.ReceiverAllConnectionsMessage(MsgParcel: TQBParcel; FromId: String);
begin
   LogMsg('!!! 来自驻留模块：从'+ansistring(FromId)+'收到一个群发消息哦！');
end;

end.

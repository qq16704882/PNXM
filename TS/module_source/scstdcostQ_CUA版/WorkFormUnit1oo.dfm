object WorkForm: TWorkForm
  Left = 309
  Top = 196
  Align = alClient
  AutoSize = True
  BorderStyle = bsNone
  Caption = 'DLL'#20013#30340'Work'#31383#20307
  ClientHeight = 589
  ClientWidth = 1031
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object MasterP: TPanel
    Left = 0
    Top = 0
    Width = 1031
    Height = 589
    Align = alClient
    FullRepaint = False
    TabOrder = 0
    object pnl1: TPanel
      Left = 1
      Top = 1
      Width = 1029
      Height = 41
      Align = alTop
      TabOrder = 0
      object btndel: TButton
        Left = 620
        Top = 10
        Width = 75
        Height = 25
        Caption = #21024#38500#35760#24405
        Enabled = False
        TabOrder = 2
        Visible = False
      end
      object btnfind: TButton
        Left = 899
        Top = 10
        Width = 75
        Height = 25
        Caption = #26597#35810
        TabOrder = 5
        OnClick = btnfindClick
      end
      object btnOK: TButton
        Left = 802
        Top = 10
        Width = 91
        Height = 25
        Caption = #35760#24405#20462#25913#30830#35748
        TabOrder = 4
        OnClick = btnOKClick
      end
      object btnmodi: TButton
        Left = 701
        Top = 10
        Width = 95
        Height = 25
        Caption = #21462#28040#35760#24405#20462#25913
        TabOrder = 3
        OnClick = btnmodiClick
      end
      object btnbadd: TButton
        Left = 539
        Top = 10
        Width = 75
        Height = 25
        Caption = #26032#22686#35760#24405
        TabOrder = 1
        Visible = False
        OnClick = btnbaddClick
      end
      object btnOpenTQP: TButton
        Left = 19
        Top = 10
        Width = 88
        Height = 25
        Caption = #25171#24320#21512#21516#32500#25252
        TabOrder = 0
        OnClick = btnOpenTQPClick
      end
    end
    object pnl_db: TPanel
      Left = 1
      Top = 42
      Width = 1029
      Height = 160
      Align = alTop
      TabOrder = 1
      object lbl1: TLabel
        Left = 24
        Top = 13
        Width = 60
        Height = 12
        Caption = #21512#21516#32534#21495#65306
      end
      object lbl2: TLabel
        Left = 383
        Top = 13
        Width = 60
        Height = 12
        Caption = #21512#21516#21517#31216#65306
      end
      object lbl3: TLabel
        Left = 588
        Top = 13
        Width = 84
        Height = 12
        Caption = #21512#21516#36215#22987#26085#26399#65306
      end
      object lbl4: TLabel
        Left = 771
        Top = 13
        Width = 84
        Height = 12
        Caption = #21512#21516#25130#27490#26085#26399#65306
      end
      object lbl5: TLabel
        Left = 264
        Top = 115
        Width = 36
        Height = 12
        Caption = #37329#39069#65306
      end
      object lbl6: TLabel
        Left = 32
        Top = 115
        Width = 36
        Height = 12
        Caption = #24065#31181#65306
      end
      object lbl8: TLabel
        Left = 24
        Top = 52
        Width = 60
        Height = 12
        Caption = #23458#25143#32534#21495#65306
      end
      object lbl9: TLabel
        Left = 480
        Top = 115
        Width = 60
        Height = 12
        Caption = #20184#27454#26465#27454#65306
      end
      object lbl7: TLabel
        Left = 264
        Top = 13
        Width = 36
        Height = 12
        Caption = #29366#24577#65306
      end
      object lbllzhuangtai: TLabel
        Left = 352
        Top = 13
        Width = 13
        Height = 13
        Caption = #31354
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cxdbtxtdtTQPUD01: TcxDBTextEdit
        Left = 80
        Top = 9
        DataBinding.DataField = 'TQPUD01'
        DataBinding.DataSource = ds1
        TabOrder = 0
        Width = 178
      end
      object cxdbtxtdtTQP02: TcxDBTextEdit
        Left = 449
        Top = 9
        DataBinding.DataField = 'TQP02'
        DataBinding.DataSource = ds1
        TabOrder = 2
        Width = 121
      end
      object cxdbdtdtTQP06: TcxDBDateEdit
        Left = 678
        Top = 9
        DataBinding.DataField = 'TQP06'
        DataBinding.DataSource = ds1
        TabOrder = 3
        Width = 87
      end
      object cxdbdtdtTQP07: TcxDBDateEdit
        Left = 861
        Top = 9
        DataBinding.DataField = 'TQP07'
        DataBinding.DataSource = ds1
        TabOrder = 4
        Width = 76
      end
      object cxdbtxtdtTQP03: TcxDBTextEdit
        Left = 546
        Top = 111
        DataBinding.DataField = 'TQPUD02'
        DataBinding.DataSource = ds1
        TabOrder = 13
        Width = 391
      end
      object btn1_tqp08: TcxDBButtonEdit
        Left = 80
        Top = 48
        DataBinding.DataField = 'TQP08'
        DataBinding.DataSource = ds1
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.ReadOnly = True
        Properties.OnButtonClick = btn1_tqp08PropertiesButtonClick
        Properties.OnEditValueChanged = btn1_tqp08PropertiesEditValueChanged
        TabOrder = 5
        Width = 121
      end
      object khjc: TLabeledEdit
        Left = 306
        Top = 48
        Width = 167
        Height = 20
        EditLabel.Width = 60
        EditLabel.Height = 12
        EditLabel.Caption = #23458#25143#31616#31216#65306
        LabelPosition = lpLeft
        LabelSpacing = 5
        ParentColor = True
        ReadOnly = True
        TabOrder = 6
      end
      object khqc: TLabeledEdit
        Left = 546
        Top = 48
        Width = 391
        Height = 20
        EditLabel.Width = 60
        EditLabel.Height = 12
        EditLabel.Caption = #23458#25143#20840#31216#65306
        LabelPosition = lpLeft
        LabelSpacing = 5
        ParentColor = True
        ReadOnly = True
        TabOrder = 7
      end
      object khgj: TLabeledEdit
        Left = 306
        Top = 79
        Width = 167
        Height = 20
        EditLabel.Width = 84
        EditLabel.Height = 12
        EditLabel.Caption = #23458#25143#25152#22312#22269#23478#65306
        LabelPosition = lpLeft
        LabelSpacing = 5
        ParentColor = True
        ReadOnly = True
        TabOrder = 8
      end
      object khqy: TLabeledEdit
        Left = 546
        Top = 79
        Width = 183
        Height = 20
        EditLabel.Width = 60
        EditLabel.Height = 12
        EditLabel.Caption = #25152#23646#21306#22495#65306
        LabelPosition = lpLeft
        LabelSpacing = 5
        ParentColor = True
        ReadOnly = True
        TabOrder = 9
      end
      object khywy: TLabeledEdit
        Left = 810
        Top = 79
        Width = 127
        Height = 20
        EditLabel.Width = 72
        EditLabel.Height = 12
        EditLabel.Caption = #25152#23646#19994#21153#21592#65306
        LabelPosition = lpLeft
        LabelSpacing = 5
        ParentColor = True
        ReadOnly = True
        TabOrder = 10
      end
      object cbbTQP04: TcxDBComboBox
        Left = 306
        Top = 9
        DataBinding.DataField = 'TQP04'
        DataBinding.DataSource = ds1
        Properties.Items.Strings = (
          '1'
          '2'
          '3'
          '4')
        Properties.OnEditValueChanged = cbbTQP04PropertiesEditValueChanged
        TabOrder = 1
        Width = 31
      end
      object cbb_bizhong: TComboBox
        Left = 127
        Top = 111
        Width = 131
        Height = 20
        TabOrder = 12
        OnChange = cbb_bizhongChange
      end
      object cxdbtxtdt1_tqp21: TcxDBTextEdit
        Left = 74
        Top = 111
        DataBinding.DataField = 'TQP21'
        DataBinding.DataSource = ds1
        Properties.ReadOnly = True
        Properties.OnEditValueChanged = cxdbtxtdt1_tqp21PropertiesEditValueChanged
        TabOrder = 11
        Width = 33
      end
      object cxDBCurrencyTQBUD07: TcxDBCurrencyEdit
        Left = 306
        Top = 115
        DataBinding.DataField = 'tqpud07'
        DataBinding.DataSource = ds1
        Properties.DisplayFormat = '0.00;-,0.00'
        Properties.Nullstring = '0'
        Properties.UseNullString = True
        TabOrder = 14
        Width = 121
      end
    end
    object dbnvgr1: TDBNavigator
      Left = 1
      Top = 202
      Width = 1029
      Height = 47
      DataSource = ds1
      Align = alTop
      Hints.Strings = (
        #31532#19968#26465#35760#24405
        #21069#19968#26465#35760#24405
        #21518#19968#26465#35760#24405
        #26411#26465#35760#24405
        #26032#22686#35760#24405
        #21024#38500#35760#24405
        #20462#25913#35760#24405
        #20462#25913#20449#24687#30830#35748
        #21462#28040#32534#36753
        #21047#26032
        #25552#20132#26356#26032
        #21462#28040#26356#26032)
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = dbnvgr1Click
    end
    object dbgrd1: TDBGrid
      Left = 1
      Top = 249
      Width = 1029
      Height = 339
      Align = alClient
      DataSource = ds1
      TabOrder = 3
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #23435#20307
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'TQP01'
          ReadOnly = True
          Title.Caption = 'ID'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TQPUD01'
          Title.Caption = #21512#21516#32534#21495
          Width = 250
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TQP02'
          Title.Caption = #21512#21516#21517#31216
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TQP06'
          Title.Caption = #21512#21516#36215#22987#26085#26399
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TQP07'
          Title.Caption = #21512#21516#25130#27490#26085#26399
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TQP03'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'TQPUD07'
          Title.Caption = #37329#39069
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TQP21'
          ReadOnly = True
          Title.Caption = #24065#31181
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TQP04'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'TQP08'
          Title.Caption = #23458#25143#32534#21495
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'TQPUD02'
          Title.Caption = #20184#27454#26465#20214
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TQPORIU'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'TQPDATE'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'TQPPLANT'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'TQPLEGAL'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'TQPACTI'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'TQPORIG'
          Visible = False
        end>
    end
  end
  object pnl_kh: TPanel
    Left = 180
    Top = 110
    Width = 538
    Height = 199
    Caption = 'pnl_kh'
    Color = clInfoBk
    ParentBackground = False
    TabOrder = 1
    Visible = False
    object pnl4: TPanel
      Left = 1
      Top = 30
      Width = 536
      Height = 49
      Align = alTop
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      ExplicitTop = 1
      object qkhbh: TLabeledEdit
        Left = 0
        Top = 29
        Width = 93
        Height = 20
        EditLabel.Width = 48
        EditLabel.Height = 12
        EditLabel.Caption = #23458#25143#32534#21495
        TabOrder = 0
      end
      object qkhgj: TLabeledEdit
        Left = 448
        Top = 29
        Width = 87
        Height = 20
        EditLabel.Width = 24
        EditLabel.Height = 12
        EditLabel.Caption = #22269#23478
        TabOrder = 3
      end
      object qkhjc: TLabeledEdit
        Left = 88
        Top = 29
        Width = 132
        Height = 20
        EditLabel.Width = 48
        EditLabel.Height = 12
        EditLabel.Caption = #23458#25143#31616#31216
        TabOrder = 1
      end
      object qkhqc: TLabeledEdit
        Left = 212
        Top = 29
        Width = 237
        Height = 20
        EditLabel.Width = 48
        EditLabel.Height = 12
        EditLabel.Caption = #23458#25143#20840#31216
        TabOrder = 2
      end
    end
    object pnl5: TPanel
      Left = 1
      Top = 1
      Width = 536
      Height = 29
      Align = alTop
      BevelKind = bkFlat
      BevelOuter = bvNone
      Color = clHighlightText
      ParentBackground = False
      TabOrder = 2
      ExplicitTop = -5
      object btn1: TButton
        Left = 400
        Top = 0
        Width = 121
        Height = 25
        Caption = #26597#35810
        TabOrder = 0
        OnClick = btn1Click
      end
      object btn3: TButton
        Left = 262
        Top = 0
        Width = 121
        Height = 25
        Caption = #20851#38381
        TabOrder = 1
        OnClick = btn3Click
      end
    end
    object dbgrd2: TDBGrid
      Left = 1
      Top = 79
      Width = 536
      Height = 119
      Align = alClient
      Color = clInfoBk
      DataSource = ds2
      Options = [dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      ReadOnly = True
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #23435#20307
      TitleFont.Style = []
      OnDblClick = dbgrd2DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'OCC01'
          Width = 73
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OCC02'
          Width = 123
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OCC18'
          Width = 243
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GEB02'
          Width = 60
          Visible = True
        end>
    end
  end
  object pnl_find: TPanel
    Left = 445
    Top = 35
    Width = 529
    Height = 89
    BevelOuter = bvLowered
    Caption = 'pnl_find'
    Color = clInfoBk
    ParentBackground = False
    TabOrder = 2
    Visible = False
    object pnl7: TPanel
      Left = 1
      Top = 1
      Width = 527
      Height = 54
      Align = alClient
      Color = clInfoBk
      ParentBackground = False
      TabOrder = 0
      object qhtbh: TLabeledEdit
        Left = 0
        Top = 33
        Width = 121
        Height = 20
        EditLabel.Width = 48
        EditLabel.Height = 12
        EditLabel.Caption = #21512#21516#32534#21495
        TabOrder = 0
      end
      object qhtmc: TLabeledEdit
        Left = 120
        Top = 33
        Width = 137
        Height = 20
        EditLabel.Width = 48
        EditLabel.Height = 12
        EditLabel.Caption = #21512#21516#21517#31216
        TabOrder = 1
      end
      object qhtkq: TLabeledEdit
        Left = 256
        Top = 33
        Width = 161
        Height = 20
        EditLabel.Width = 72
        EditLabel.Height = 12
        EditLabel.Caption = #21512#21516#23458#25143#32534#21495
        TabOrder = 2
      end
      object qhtgj: TLabeledEdit
        Left = 415
        Top = 33
        Width = 110
        Height = 20
        EditLabel.Width = 48
        EditLabel.Height = 12
        EditLabel.Caption = #21512#21516#22269#23478
        TabOrder = 3
      end
    end
    object pnl8: TPanel
      Left = 1
      Top = 55
      Width = 527
      Height = 33
      Align = alBottom
      Color = clInfoBk
      ParentBackground = False
      TabOrder = 1
      object btn2: TButton
        Left = 352
        Top = 5
        Width = 105
        Height = 25
        Caption = #26597#35810
        TabOrder = 0
        OnClick = btn2Click
      end
      object btn4: TButton
        Left = 240
        Top = 4
        Width = 106
        Height = 25
        Caption = #20851#38381
        TabOrder = 1
        OnClick = btn4Click
      end
    end
  end
  object Cds: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 72
    Top = 304
  end
  object WinMessage: TQBWinMessages
    OnMsgArrived = WinMessageMsgArrived
    Left = 41
    Top = 426
  end
  object dba: TRemoteUniDac
    TaskTimeout = 30
    EnableBCD = False
    EnableFMTBCD = False
    EnableLoadBalance = False
    Left = 56
    Top = 358
  end
  object cxStyleRepository: TcxStyleRepository
    Left = 880
    Top = 280
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor]
      Color = 15784893
    end
    object DELS: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsStrikeOut]
    end
    object AddS: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clRed
    end
    object ModS: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic]
    end
    object DefaultS: TcxStyle
    end
  end
  object PrivilegeCDS: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 896
    Top = 384
  end
  object ds1: TDataSource
    DataSet = Cds
    Left = 88
    Top = 360
  end
  object Tuq: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 464
    Top = 216
  end
  object ds2: TDataSource
    Left = 508
    Top = 222
  end
end

﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel,
  Vcl.clipbrd, system.Win.comobj,
  BaseGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, CRGrid, DBAccess, Uni, Vcl.Menus, RemoteUniDac;

type
  TWorkForm = class(TForm)
    Cds: TClientDataSet;
    MasterP: TPanel;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    PrivilegeCDS: TClientDataSet;
    Panel1: TPanel;
    Label1: TLabel;
    xmbh: TLabeledEdit;
    DTP: TDateTimePicker;
    Button2: TButton;
    Button3: TButton;
    btn1: TButton;
    Panel2: TPanel;
    qmwyb: TLabeledEdit;
    qmyyb: TLabeledEdit;
    ymyylc: TLabeledEdit;
    lllc: TLabeledEdit;
    Button4: TButton;
    Panel3: TPanel;
    qcwys: TLabeledEdit;
    qcyys: TLabeledEdit;
    qcljlc: TLabeledEdit;
    qclc: TLabeledEdit;
    Button1: TButton;
    xsxx: TPageControl;
    TabSheet1: TTabSheet;
    llxx: TCRDBGrid;
    ts1: TTabSheet;
    pnl1: TPanel;
    rg1: TRadioGroup;
    bhll: TCRDBGrid;
    项目包号信息: TTabSheet;
    Panel4: TPanel;
    RadioGroup1: TRadioGroup;
    bhxx: TCRDBGrid;
    PopupMenu2: TPopupMenu;
    EXCEL2: TMenuItem;
    pm1: TPopupMenu;
    EXCEL4: TMenuItem;
    PopupMenu1: TPopupMenu;
    EXCEL1: TMenuItem;
    bhllq: TClientDataSet;
    llq: TClientDataSet;
    bhq: TClientDataSet;
    UniDataSource1: TUniDataSource;
    UniDataSource2: TUniDataSource;
    UniDataSource3: TUniDataSource;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure btn1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure rg1Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure EXCEL4Click(Sender: TObject);
    procedure EXCEL1Click(Sender: TObject);
    procedure EXCEL2Click(Sender: TObject);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function RunCDS(Const SQL: string; Cds: TClientDataSet): boolean;
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure WaitMessage;
    procedure GetPrivilege(TFlag, TID, UID: string);
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'JSTYPE3';

  llxxhz = '料号,料名,规格,仓库,库位,批号,单位,上月未用领料,上月在线领料,本月未用领料,本月在线领料,耗量';
  llxxmx = '料号,料名,规格,仓库,库位,批号,单位,包号,上月未用领料,包号,上月在线领料,包号,本月未用领料,包号,本月在线领料';
  bhxx = '类型,包号,砌筑日期,完工日期,上线日期,完工日期,累计炉次';
  bhll = '项目包号,料号,料名,规格,期初领料,本月领料,领料总数';
  DBCC = 42; // 单包信息明细列数

var
  zhanghu: string;
  softWareAuth: boolean;
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  MessageFlag: boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲

  xmbh_old: string;
  dyy_old: Integer;
  dym_old: Integer;
  jsr: Integer; // 项目结算日
  dyy: Integer; // 项目当前周期所在年
  dym: Integer;
  syy: Integer;
  sym: Integer;
  qcr: Tdate; // 期初日
  qmr: Tdate; // 期末日
  tqcwyb: string;
  tqcyyb: string;
  tqmwyb: string;
  tqmyyb: string;
  allBH: string; // 所有相关包号
  tqcwys: Integer;
  tqcyys: Integer;
  tqmwys: Integer;
  tqmyys: Integer;
  tqclc: Integer;
  tqmlc: Integer;
  dylllc: Integer;
  sylllc: Integer;

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.WaitMessage;
begin
  While True do
  Begin
    application.ProcessMessages; // 读取消息队列
    If Not MessageFlag Then
      Break;
  End;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');
  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');
  //
  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage; // 同步处理消息

  // 界面初始化
  // 默认为当天
  DTP.Date := now();
  jsr := -1;
  xmbh_old := '';
  dyy_old := 0;
  dym_old := 0;
end;

// -----------------------------------------------------------------------------
// 业务代码
Const
  HZCN = '包号,料件编号,品号,规格,牌号,配方,数量单位,重量单位,新砌数量汇总,新砌重量汇总,维修数量汇总,维修重量汇总,分摊数量汇总,分摊重量汇总,项目编号,项目名称,来源公司,现场仓库,仓库名称';
  DBCN = '项目包号,砌筑日期,完工日期,上线日期,下线日期,料件编号,品名,规格,牌号,配方,数量单位,重量单位,单据日期,新砌单号,数量,重量,'
    + '一次维修单号,数量,重量,二次维修单号,数量,重量,三次维修单号,数量,重量,四次维修单号,数量,重量,五次维修单号,数量,重量,六次及以上单号,数量,重量,'
    + '分摊单号,数量,重量,项目编号,项目名称,' + '来源公司,现场仓库,仓库名称';
  // 查询用户负责的项目编号集
  QueryXmSQL = 'SELECT gem01,gem02 FROM %sgem_file where ta_gem32 = ''%s''';
  // 查询主SQL
  HZSQL = '	SELECT immud02,imn03,ima02,ima021,imaud02,imaud03,ima25,ima907,A_T2,A_T5,B_T2,B_T5,C_T2,C_T5,imm14,gem02,imn06,imn15,imd02 FROM 	 '
    + #13#10 + '	(	 ' + #13#10 +
    '	SELECT imm14,immud02,imn06,imn15,imn03,A_T2,A_T5,B_T2,B_T5,C_T2,C_T5 FROM 	 '
    + #13#10 + '	(	 ' + #13#10 +
    '	SELECT DISTINCT imm14,immud02,DECODE(imn06,''GWGS'',''高温公司'',''GNGS'',''功能公司'',''YKGS'',''营口公司'',''-'') imn06,imn15,imn03,immud03,Timn42,Timn45 FROM	 '
    + #13#10 + '	(	 ' + #13#10 +
    '	SELECT imm14,SUBSTR(immud02,0,INSTR(immud02,''-'')-1) immud02,SUBSTR(imn06,0,INSTR(imn06,''-'') -1) imn06,imn15,imn03,immud03,	 '
    + #13#10 +
    '	  SUM(imn42)OVER(PARTITION BY imm14,SUBSTR(immud02,0,INSTR(immud02,''-'')-1),SUBSTR(imn06,0,INSTR(imn06,''-'') -1),imn03,immud03) AS Timn42,	 '
    + #13#10 +
    '	  SUM(imn45)OVER(PARTITION BY imm14,SUBSTR(immud02,0,INSTR(immud02,''-'')-1),SUBSTR(imn06,0,INSTR(imn06,''-'') -1),imn03,immud03) AS Timn45 	 '
    + #13#10 + '	     FROM %simn_file,%0:simm_file 	 ' + #13#10 +
    '	      WHERE  imn01 = imm01 AND imm03 = ''Y'' AND immconf = ''Y'' AND regexp_like(imm01,''CL01'') AND immud03 IS NOT NULL %s	 '
    + #13#10 + '	) aa)	 ' + #13#10 + '	PIVOT	 ' + #13#10 +
    '	( MAX(Timn42) T2,MAX(Timn45) T5 FOR immud03 IN (''1'' AS A,''2'' AS B,''3'' AS C)	 '
    + #13#10 +
    '	)) xx,%0:sima_file,%0:sgem_file,%0:simd_file WHERE xx.imn03 = ima01 AND xx.imm14 = gem01 AND xx.imn15 = imd01 	 '
    + #13#10 + '	    ORDER BY imm14,immud02,imn06,imn15,imn03	 ';

  DBSQL = '	SELECT immud02,tc_pjy04,tc_pjy04,tc_pjy06,tc_pjy07,imn03,ima02,ima021,imaud02,imaud03,ima25,ima907,imm17,'
    + #13#10 +
    '	A_M,A_T2,A_T5,B1_M,B1_T2,B1_T5, B2_M,B2_T2,B2_T5,B3_M,B3_T2,B3_T5,B4_M,B4_T2,B4_T5,B5_M,B5_T2,B5_T5,B6_M,B6_T2,B6_T5,C_M,C_T2,C_T5,imm14,gem02,imn06,imn15,imd02 FROM 	 '
    + #13#10 + '	(	 ' + #13#10 +
    '	SELECT imm14,immud02,imn06,imn15,imn03,imm17,A_M,A_T2,A_T5,B1_M,B1_T2,B1_T5, B2_M,B2_T2,B2_T5,B3_M,B3_T2,B3_T5,B4_M,B4_T2,B4_T5,B5_M,B5_T2,B5_T5,B6_M,B6_T2,B6_T5,C_M,C_T2,C_T5 FROM 	 '
    + #13#10 + '	(	 ' + #13#10 +
    '	SELECT imm14,immud02,DECODE(imn06,''GWGS'',''高温公司'',''GNGS'',''功能公司'',''YKGS'',''营口公司'',''-'') imn06,imn15,imn03,imm17,imm01,immud03,immud10,imn42,imn45 FROM	 '
    + #13#10 + '	(	 ' + #13#10 +
    '	SELECT imm14,immud02,SUBSTR(imn06,0,INSTR(imn06,''-'') -1) imn06,imn15,imn03,imm17,imm01,immud03,nvl(immud10,1) immud10,imn42,imn45 	 '
    + #13#10 + '	     FROM %simn_file,%0:simm_file 	 ' + #13#10 +
    '	      WHERE  imn01 = imm01 AND imm03 = ''Y''  AND immconf = ''Y'' AND regexp_like(imm01,''CL01'') AND immud03 IS NOT NULL  %s	 '
    + #13#10 + '	) aa)	 ' + #13#10 + '	PIVOT	 ' + #13#10 +
    '	( max(imm01) M,SUM(imn42) T2,SUM(imn45) T5 FOR (immud03,immud10) IN ((''1'',1) AS A,(''2'',1) AS B1,(''2'',2) AS B2,(''2'',3) AS B3,(''2'',4) AS B4,(''2'',5) AS B5,(''2'',6) AS B6,(''3'',1) AS C)	 '
    + #13#10 + '	)	 ' + #13#10 +
    '	) xx,%0:stc_pjy_file,%0:sima_file,%0:sgem_file,%0:simd_file WHERE xx.immud02 = tc_pjy01 AND xx.imm14 = tc_pjy03 AND xx.imn03 = ima01 AND xx.imm14 = gem01 AND xx.imn15 = imd01 	 '
    + #13#10 + '	    ORDER BY imm14,immud02,imn06,imn15,imm17,imn03	 ';

procedure TWorkForm.RadioGroup1Click(Sender: TObject);
begin
  case RadioGroup1.ItemIndex of
    0:
      bhq.Filter := ' BS IN (''期初未用'',''期初已用'',''期末未用'',''期末已用'')';
    1:
      begin
        bhq.Filter := ' BS = ''期初未用''';
      end;
    2:
      begin
        bhq.Filter := ' BS = ''期初已用''';
      end;
    3:
      begin
        bhq.Filter := ' BS = ''期末未用''';
      end;
    4:
      begin
        bhq.Filter := ' BS = ''期末已用''';
      end;
  end;
end;

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

procedure TWorkForm.rg1Click(Sender: TObject);
begin
  case rg1.ItemIndex of
    0:
      bhllq.Filter := Format(' 包号 IN %s', [allBH]);
    1:
      bhllq.Filter := Format(' 包号 IN %s', [tqcwyb]);
    2:
      bhllq.Filter := Format(' 包号 IN %s', [tqcyyb]);
    3:
      bhllq.Filter := Format(' 包号 IN %s', [tqmwyb]);
    4:
      bhllq.Filter := Format(' 包号 IN %s', [tqmyyb]);
  end;
end;

function TWorkForm.RunCDS(Const SQL: string; Cds: TClientDataSet): boolean;
begin
  Result := false;
  Cds.close;
  if not dba.ReadDataset(SQL, Cds) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    Cds.active := True;
    Result := True;
  end;
end;

procedure TWorkForm.btn1Click(Sender: TObject);
// 项目包号领料汇总
var
  SQL: STRING;
begin
  // 判断是否需要重新查询
  if not((xmbh_old = trim(xmbh.Text)) and (dyy = Yearof(DTP.Date)) and
    (dym = Monthof(DTP.Date))) then
  begin
    showmessage('项目编号/查询日期发生变化，请运行<查询炉次信息>后继续!');
    bhllq.active := false;
    exit;
  end;

  xsxx.ActivePageIndex := 1;
  SQL := 'SELECT rowNum,包号,品号,品名,规格,单位,期初领料,本月领料,(期初领料 + 本月领料) 总领料 FROM ' +
    #13#10 + '(                                                                                    '
    + #13#10 +
    'SELECT imn03 品号,ima02 品名,ima021 规格, imn40 单位,immud02 包号,期初领料,本月领料   ' +
    #13#10 + 'FROM                                                                                  '
    + #13#10 +
    '(                                                                                   '
    + #13#10 +
    'SELECT  IMN03,IMN40,IMMUD02,nvl(A_ZSL,0) AS 期初领料,nvl(B_ZSL,0) AS 本月领料  FROM     '
    + #13#10 +
    '(                                                                                  '
    + #13#10 +
    'SELECT 1 AS S,imn03,imn40,immud02,sum(imn42) SL                           '
    + #13#10 +
    'FROM                                                                                '
    + #13#10 +
    '(                                                                                   '
    + #13#10 +
    'SELECT tc_pjy03,tc_pjy01,tc_pjy04 FROM tc_pjy_file                                  '
    + #13#10 + ' WHERE tc_pjy03 = ''' + trim(xmbh.Text) + ''' AND tc_pjy01 IN '
    + allBH + #13#10 +
    ' ) aa,                                                                               '
    + #13#10 +
    '( SELECT imm01,imn03,imn15,imn16,imn17,imn40,imn42,imm17,imm14,immud02 FROM imm_file,imn_file  WHERE imn01 = imm01 AND immconf = ''Y'' AND immud03 in (''1'',''2'')) bb  '
    + #13#10 +
    '   WHERE aa.tc_pjy03 = bb.imm14 AND bb.immud02 = aa.tc_pjy01 AND bb.imm17 >= aa.tc_pjy04 AND bb.imm17<= to_date('''
    + FormatDateTime('eemmdd', qcr - 1) + ''',''yymmdd'')       ' + #13#10 +
    'GROUP BY imn03,imn40,immud02                                                                                                                              '
    + #13#10 + 'UNION ALL ' + #13#10 +
    'SELECT 2 AS S,imn03,imn40,immud02,sum(imn42) SL                           '
    + #13#10 +
    'FROM                                                                                '
    + #13#10 +
    '(                                                                                   '
    + #13#10 +
    'SELECT tc_pjy03,tc_pjy01,tc_pjy04 FROM tc_pjy_file                                  '
    + #13#10 + ' WHERE tc_pjy03 = ''' + trim(xmbh.Text) + ''' AND tc_pjy01 IN '
    + allBH + #13#10 +
    ' ) aa,                                                                               '
    + #13#10 +
    '( SELECT imm01,imn03,imn15,imn16,imn17,imn40,imn42,imm17,imm14,immud02 FROM imm_file,imn_file  WHERE imn01 = imm01 AND immconf = ''Y'' AND immud03 in (''1'',''2'')) bb  '
    + #13#10 +
    '   WHERE aa.tc_pjy03 = bb.imm14 AND bb.immud02 = aa.tc_pjy01 AND bb.imm17 >= to_date('''
    + FormatDateTime('eemmdd', qcr) + ''',''yymmdd'') AND bb.imm17<= to_date('''
    + FormatDateTime('eemmdd', qmr) + ''',''yymmdd'')    ' + #13#10 +
    'GROUP BY imn03,imn40,immud02                                                                                                                               '
    + #13#10 +
    ')                                                                                  '
    + #13#10 +
    'PIVOT                                                                              '
    + #13#10 + '(SUM(sl) ZSL FOR S IN (1 AS A,2 AS B))                        '
    + #13#10 +
    ') ifo                                                                              '
    + #13#10 +
    'LEFT JOIN ima_file ON ima01 = imn03 )                                              ';
  // Memo1.Lines.Text := bhllq.SQL.Text;
  RunCDS(SQL, bhllq);
  bhll.Columns.Items[1].Width := 80;
  bhll.Columns.Items[2].Width := 100;
  bhll.Columns.Items[3].Width := 200;
  bhll.Columns.Items[4].Width := 300;
  ResizeForm;
end;

procedure TWorkForm.Button1Click(Sender: TObject);
var
  SQL: string;
begin
  // 判断是否需要重新查询
  if not((xmbh_old = trim(xmbh.Text)) and (dyy = Yearof(DTP.Date)) and
    (dym = Monthof(DTP.Date))) then
  begin
    showmessage('项目编号/查询日期发生变化，请运行<查询炉次信息>后继续!');
    llq.active := false;
    exit;
  end;

  xsxx.ActivePageIndex := 0;
  // 查询领料情况
  SQL := 'SELECT ROWNUM ,品号,品名,规格,单位,领料单号,包号,当月领料,上月未用领料, 上月在线领料,  本月未用领料,  本月在线领料 FROM '
    + #13#10 +
    '(                                                                                                                                      '
    + #13#10 +
    'SELECT imn03 品号,ima02 品名,ima021 规格, imn40 单位,''-'' 领料单号,''-'' 包号,当月领料,上月未用领料,上月在线领料,本月未用领料,本月在线领料   '
    + #13#10 +
    'FROM                                                                                                                '
    + #13#10 +
    '(                                                                                                                 '
    + #13#10 +
    'SELECT  IMN03, IMN15, IMN16, IMN17, IMN40,nvl(A_ZSL,0) AS 当月领料,nvl(B_ZSL,0) AS 上月未用领料, round(nvl(C_ZSL,0),3) AS 上月在线领料, round(nvl(D_ZSL,0),3) AS 本月未用领料, round(nvl(E_ZSL,0),3) AS 本月在线领料  FROM     '
    + #13#10 +
    '(                                                                                  '
    + #13#10 +
    'SELECT 1 AS S,imn03,imn15,imn16,imn17,imn40,sum(imn42) SL FROM imm_file,imn_file   '
    + #13#10 +
    'WHERE imn01 = imm01  AND                                                           '
    + #13#10 + 'imm14 = ''' + trim(xmbh.Text) + '''' + #13#10 +
    ' AND                                                                               '
    + #13#10 + 'imm17 BETWEEN to_date(''' + FormatDateTime('eemmdd', qcr) +
    ''',''yymmdd'')        ' + #13#10 + ' AND to_date(''' +
    FormatDateTime('eemmdd', qmr) + ''',''yymmdd'')                 ' + #13#10 +
    'AND immconf = ''Y'' AND immud03 in (''1'',''2'')  group by imn03,imn15,imn16,imn17,imn40 '
    + #13#10 + 'UNION ALL ' + #13#10 +
    'SELECT 2 AS S,imn03,imn15,imn16,imn17,imn40,sum(imn42) SL                           '
    + #13#10 +
    'FROM                                                                                '
    + #13#10 +
    '(                                                                                   '
    + #13#10 +
    'SELECT tc_pjy03,tc_pjy01,tc_pjy04 FROM tc_pjy_file                                  '
    + #13#10 + ' WHERE tc_pjy03 = ''' + trim(xmbh.Text) + ''' AND tc_pjy01 IN '
    + tqcwyb + #13#10 +
    ' ) aa,                                                                               '
    + #13#10 +
    '( SELECT imm01,imn03,imn15,imn16,imn17,imn40,imn42,imm17,imm14,immud02 FROM imm_file,imn_file  WHERE imn01 = imm01 AND immconf = ''Y'' AND immud03 in (''1'',''2'')) bb  '
    + #13#10 +
    '   WHERE aa.tc_pjy03 = bb.imm14 AND bb.immud02 = aa.tc_pjy01 AND bb.imm17 >= aa.tc_pjy04 AND bb.imm17<= to_date('''
    + FormatDateTime('eemmdd', qcr - 1) +
    ''',''yymmdd'')                                   ' + #13#10 +
    'GROUP BY imn03,imn15,imn16,imn17,imn40                                                                                                                               '
    + #13#10 + 'UNION ALL ' + #13#10 +
    'SELECT 3 AS S,imn03,imn15,imn16,imn17,imn40,sum(imn42) SL                           '
    + #13#10 +
    'FROM                                                                                '
    + #13#10 +
    '(                                                                                   '
    + #13#10 +
    'SELECT tc_pjy03,tc_pjy01,tc_pjy04 FROM tc_pjy_file                                  '
    + #13#10 + ' WHERE tc_pjy03 = ''' + trim(xmbh.Text) + ''' AND tc_pjy01 IN '
    + tqcyyb + #13#10 +
    ' ) aa,                                                                               '
    + #13#10 +
    '( SELECT imm01,imn03,imn15,imn16,imn17,imn40,imn42,imm17,imm14,immud02 FROM imm_file,imn_file  WHERE imn01 = imm01 AND immconf = ''Y'' AND immud03 in (''1'',''2'')) bb  '
    + #13#10 +
    '   WHERE aa.tc_pjy03 = bb.imm14 AND bb.immud02 = aa.tc_pjy01 AND bb.imm17 >= aa.tc_pjy04 AND bb.imm17<= to_date('''
    + FormatDateTime('eemmdd', qcr - 1) +
    ''',''yymmdd'')                                   ' + #13#10 +
    'GROUP BY imn03,imn15,imn16,imn17,imn40                                                                                                                               '
    + #13#10 + 'UNION ALL ' + #13#10 +
    'SELECT 4 AS S,imn03,imn15,imn16,imn17,imn40,sum(imn42) SL                           '
    + #13#10 +
    'FROM                                                                                '
    + #13#10 +
    '(                                                                                   '
    + #13#10 +
    'SELECT tc_pjy03,tc_pjy01,tc_pjy04 FROM tc_pjy_file                                  '
    + #13#10 + ' WHERE tc_pjy03 = ''' + trim(xmbh.Text) + ''' AND tc_pjy01 IN '
    + tqmwyb + #13#10 +
    ' ) aa,                                                                               '
    + #13#10 +
    '( SELECT imm01,imn03,imn15,imn16,imn17,imn40,imn42,imm17,imm14,immud02 FROM imm_file,imn_file  WHERE imn01 = imm01 AND immconf = ''Y'' AND immud03 in (''1'',''2'')) bb  '
    + #13#10 +
    '   WHERE aa.tc_pjy03 = bb.imm14 AND bb.immud02 = aa.tc_pjy01 AND bb.imm17 >= aa.tc_pjy04 AND bb.imm17<= to_date('''
    + FormatDateTime('eemmdd', qmr) +
    ''',''yymmdd'')                                   ' + #13#10 +
    'GROUP BY imn03,imn15,imn16,imn17,imn40                                                                                                                               '
    + #13#10 + 'UNION ALL ' + #13#10 +
    'SELECT 5 AS S,imn03,imn15,imn16,imn17,imn40,sum(imn42) SL                           '
    + #13#10 +
    'FROM                                                                                '
    + #13#10 +
    '(                                                                                   '
    + #13#10 +
    'SELECT tc_pjy03,tc_pjy01,tc_pjy04 FROM tc_pjy_file                                  '
    + #13#10 + ' WHERE tc_pjy03 = ''' + trim(xmbh.Text) + ''' AND tc_pjy01 IN '
    + tqmyyb + #13#10 +
    ' ) aa,                                                                               '
    + #13#10 +
    '( SELECT imm01,imn03,imn15,imn16,imn17,imn40,imn42,imm17,imm14,immud02 FROM imm_file,imn_file  WHERE imn01 = imm01 AND immconf = ''Y'' AND immud03 in (''1'',''2'')) bb  '
    + #13#10 +
    '   WHERE aa.tc_pjy03 = bb.imm14 AND bb.immud02 = aa.tc_pjy01 AND bb.imm17 >= aa.tc_pjy04 AND bb.imm17<= to_date('''
    + FormatDateTime('eemmdd', qmr) +
    ''',''yymmdd'')                                   ' + #13#10 +
    'GROUP BY imn03,imn15,imn16,imn17,imn40                                             '
    + #13#10 +
    ')                                                                                  '
    + #13#10 +
    'PIVOT                                                                              '
    + #13#10 +
    '(SUM(sl) ZSL FOR S IN (1 AS A,2 AS B,3 AS C,4 AS D,5 AS E))                        '
    + #13#10 +
    ') ifo                                                                              '
    + #13#10 +
    'LEFT JOIN ima_file ON ima01 = imn03 )                                               ';
  // memo1.Text := llq.SQL.Text;
  RunCDS(SQL, llq);
  llxx.Columns.Items[1].Width := 100;
  llxx.Columns.Items[2].Width := 200;
  llxx.Columns.Items[3].Width := 300;
  ResizeForm;
end;

procedure TWorkForm.Button2Click(Sender: TObject);
var
  E: string;
  T, tp: Tdate;
  SQL: string;
begin
  if xmbh.Text = '' then
    E := '项目编号不可为空';

  if E <> '' then
  begin
    showmessage(E);
    exit;
  end;

  xsxx.ActivePageIndex := 2;
  tqcwyb := '(';
  tqcyyb := '(';
  tqmwyb := '(';
  tqmyyb := '(';
  tqcwys := 0;
  tqcyys := 0;
  tqmwys := 0;
  tqmyys := 0;
  tqclc := 0;
  tqmlc := 0;
  dylllc := 0;

  // 获取结算周期日
  SQL := 'select nvl(ta_gem33,0) a from gem_file where gem01 = ''' +
    trim(xmbh.Text) + '''';
  // memo1.text := uq.sql.text;
  RunCDS(SQL, Cds);
  jsr := Cds.FieldByName('a').AsInteger;

  T := DTP.Date;
  tp := incMonth(T, -1);
  dyy := Yearof(T);
  dym := Monthof(T);
  syy := Yearof(tp);
  sym := Monthof(tp);
  // 将领料信息删除，并确定项目与时间是否发生变化
  llq.active := false;
  bhllq.active := false;
  dyy_old := dyy;
  dym_old := dym;
  xmbh_old := trim(xmbh.Text);

  if jsr = 0 then
  begin
    qcr := EncodeDate(dyy, dym, 1);
    qmr := EndofTheMonth(T);
  end
  else
  begin
    qcr := EncodeDate(syy, sym, jsr) + 1;
    qmr := EncodeDate(dyy, dym, jsr);
  end;
  // 获取当月理论炉次,若查不到，默认为1，防止后面做除数时报错
  SQL := 'select nvl(tc_pjz23,1) a from tc_pjz_file where tc_pjz10 = ''' +
    trim(xmbh.Text) + ''' and tc_pjz02 = ' + inttostr(syy) + ' and tc_pjz03 = '
    + inttostr(sym) + ' and rownum = 1';
  RunCDS(SQL, Cds);
  sylllc := Cds.FieldByName('a').AsInteger;

  // 获取当月理论炉次,若查不到，默认为1，防止后面做除数时报错
  SQL := 'select nvl(tc_pjz23,1) a from tc_pjz_file where tc_pjz10 = ''' +
    trim(xmbh.Text) + ''' and tc_pjz02 = ' + inttostr(dyy) + ' and tc_pjz03 = '
    + inttostr(dym) + ' and rownum = 1';
  RunCDS(SQL, Cds);
  dylllc := Cds.FieldByName('a').AsInteger;
  // 查询炉次信息
  // showmessage('(''1'',''2'')');
  // 期初未用包
  SQL := ' SELECT tc_pjz01 FROM tc_pjz_file ' + #13#10 +
    '   WHERE   tc_pjz10 = ''' + trim(xmbh.Text) + '''' + #13#10 +
    '   AND     tc_pjz01 IN (  ' + #13#10 +
    '                         SELECT tc_pjz01 FROM tc_pjz_file,tc_pjy_file,gem_file '
    + #13#10 + '                         WHERE tc_pjz10 = ''' + trim(xmbh.Text)
    + '''' + #13#10 +
    '                         AND tc_pjz01 = tc_pjy01 AND tc_pjy03 = tc_pjz10 AND gem01 = tc_pjz10  '
    + #13#10 + '                         AND tc_pjz02 = ' + inttostr(syy) +
    ' AND tc_pjz03 = ' + inttostr(sym) + #13#10 +
    '                         AND (tc_pjy07 IS NULL OR tc_pjy07 > to_date(''' +
    FormatDateTime('eemmdd', qcr - 1) + ''',''yymmdd'')) ) ' + #13#10 +
    '   AND     tc_pjz02||to_char(tc_pjz03,''FM00'') <= ''' + inttostr(syy) +
    Format('%.2d', [sym]) + '''' + #13#10 +
    '   GROUP BY tc_pjz10,tc_pjz01 HAVING NVL(SUM(tc_pjz05),0) ＝ 0 ';

  // memo1.Text := uq.SQL.Text;
  RunCDS(SQL, Cds);
  Cds.First;
  while not Cds.Eof do
  begin
    tqcwyb := tqcwyb + '''' + Cds.FieldByName('tc_pjz01').AsString + ''',';
    inc(tqcwys);
    Cds.Next;
  end;
  if tqcwys > 0 then
    tqcwyb := copy(tqcwyb, 1, length(tqcwyb) - 1) + ')'
  else
    tqcwyb := '('''')';
  // 期末未用包
  SQL := ' SELECT tc_pjz01 FROM tc_pjz_file ' + #13#10 +
    '   WHERE   tc_pjz10 = ''' + trim(xmbh.Text) + '''' + #13#10 +
    '   AND     tc_pjz01 IN (  ' + #13#10 +
    '                         SELECT tc_pjz01 FROM tc_pjz_file,tc_pjy_file,gem_file '
    + #13#10 + '                         WHERE tc_pjz10 = ''' + trim(xmbh.Text)
    + '''' + #13#10 +
    '                         AND tc_pjz01 = tc_pjy01 AND tc_pjy03 = tc_pjz10 AND gem01 = tc_pjz10  '
    + #13#10 + '                         AND tc_pjz02 = ' + inttostr(dyy) +
    ' AND tc_pjz03 = ' + inttostr(dym) + #13#10 +
    '                         AND (tc_pjy07 IS NULL OR tc_pjy07 > to_date(''' +
    FormatDateTime('eemmdd', qmr) + ''',''yymmdd'')) ) ' + #13#10 +
    '   AND     tc_pjz02||to_char(tc_pjz03,''FM00'') <= ''' + inttostr(dyy) +
    Format('%.2d', [dym]) + '''' + #13#10 +
    '   GROUP BY tc_pjz10,tc_pjz01 HAVING NVL(SUM(tc_pjz05),0) ＝ 0 ';
  RunCDS(SQL, Cds);
  Cds.First;
  while not Cds.Eof do
  begin
    tqmwyb := tqmwyb + '''' + Cds.FieldByName('tc_pjz01').AsString + ''',';
    inc(tqmwys);
    Cds.Next;
  end;
  if tqmwys > 0 then
    tqmwyb := copy(tqmwyb, 1, length(tqmwyb) - 1) + ')'
  else
    tqmwyb := '('''')';
  // 期初已用包
  SQL := ' SELECT tc_pjz01, nvl(SUM(tc_pjz05),0) lc FROM tc_pjz_file ' + #13#10
    + '   WHERE   tc_pjz10 = ''' + trim(xmbh.Text) + '''' + #13#10 +
    '   AND     tc_pjz01 IN (  ' + #13#10 +
    '                         SELECT tc_pjz01 FROM tc_pjz_file,tc_pjy_file,gem_file '
    + #13#10 + '                         WHERE tc_pjz10 = ''' + trim(xmbh.Text)
    + '''' + #13#10 +
    '                         AND tc_pjz01 = tc_pjy01 AND tc_pjy03 = tc_pjz10 AND gem01 = tc_pjz10  '
    + #13#10 + '                         AND tc_pjz02 = ' + inttostr(syy) +
    ' AND tc_pjz03 = ' + inttostr(sym) + #13#10 +
    '                         AND (tc_pjy07 IS NULL OR tc_pjy07 > to_date(''' +
    FormatDateTime('eemmdd', qcr - 1) +
    ''',''yymmdd'')) AND tc_pjy06 <= to_date(''' + FormatDateTime('eemmdd',
    qcr - 1) + ''',''yymmdd'') ) ' + #13#10 +
    '   AND     tc_pjz02||to_char(tc_pjz03,''FM00'') <= ''' + inttostr(syy) +
    Format('%.2d', [sym]) + '''' + #13#10 +
    '   GROUP BY tc_pjz10,tc_pjz01 HAVING NVL(SUM(tc_pjz05),0) > 0 ';
  // memo1.Text := uq.SQL.Text;
  RunCDS(SQL, Cds);
  Cds.First;
  while not Cds.Eof do
  begin
    tqcyyb := tqcyyb + '''' + Cds.FieldByName('tc_pjz01').AsString + ''',';
    inc(tqcyys);
    tqclc := tqclc + Cds.FieldByName('lc').AsInteger;
    Cds.Next;
  end;
  if tqcyys > 0 then
    tqcyyb := copy(tqcyyb, 1, length(tqcyyb) - 1) + ')'
  else
    tqcyyb := '('''')';
  // 期末已用包
  SQL := ' SELECT tc_pjz01, nvl(SUM(tc_pjz05),0) lc FROM tc_pjz_file ' + #13#10
    + '   WHERE   tc_pjz10 = ''' + trim(xmbh.Text) + '''' + #13#10 +
    '   AND     tc_pjz01 IN (  ' + #13#10 +
    '                         SELECT tc_pjz01 FROM tc_pjz_file,tc_pjy_file,gem_file '
    + #13#10 + '                         WHERE tc_pjz10 = ''' + trim(xmbh.Text)
    + '''' + #13#10 +
    '                         AND tc_pjz01 = tc_pjy01 AND tc_pjy03 = tc_pjz10 AND gem01 = tc_pjz10  '
    + #13#10 + '                         AND tc_pjz02 = ' + inttostr(dyy) +
    ' AND tc_pjz03 = ' + inttostr(dym) + #13#10 +
    '                         AND (tc_pjy07 IS NULL OR tc_pjy07 > to_date(''' +
    FormatDateTime('eemmdd', qmr) + ''',''yymmdd'')) AND tc_pjy06 <= to_date('''
    + FormatDateTime('eemmdd', qmr) + ''',''yymmdd'') ) ' + #13#10 +
    '   AND     tc_pjz02||to_char(tc_pjz03,''FM00'') <= ''' + inttostr(dyy) +
    Format('%.2d', [dym]) + '''' + #13#10 +
    '   GROUP BY tc_pjz10,tc_pjz01 HAVING NVL(SUM(tc_pjz05),0) > 0 ';
  // memo1.Text := uq.SQL.Text;
  RunCDS(SQL, Cds);
  Cds.First;
  while not Cds.Eof do
  begin
    tqmyyb := tqmyyb + '''' + Cds.FieldByName('tc_pjz01').AsString + ''',';
    inc(tqmyys);
    tqmlc := tqmlc + Cds.FieldByName('lc').AsInteger;
    Cds.Next;
  end;
  if tqmyys > 0 then
    tqmyyb := copy(tqmyyb, 1, length(tqmyyb) - 1) + ')'
  else
    tqmyyb := '('''')';

  qcwys.Text := inttostr(tqcwys);
  qcyys.Text := inttostr(tqcyys);
  qcljlc.Text := inttostr(tqclc);
  qmwyb.Text := inttostr(tqmwys);
  qmyyb.Text := inttostr(tqmyys);
  ymyylc.Text := inttostr(tqmlc);
  lllc.Text := inttostr(dylllc);
  qclc.Text := inttostr(sylllc);

  allBH := tqcwyb + ',' + tqcyyb + ',' + tqmwyb + ',' + tqmyyb;
  allBH := StringReplace(allBH, '(', '', [rfReplaceAll]);
  allBH := StringReplace(allBH, ')', '', [rfReplaceAll]);
  if allBH = '' then
    allBH := '('''')'
  else
    allBH := '(' + allBH + ')';

  // 在列表中显示
  SQL := 'select rownum,bs,包号,砌筑日期,完工日期,上线日期,下线日期,当月炉次,累计到当月炉次 from (' +
    'SELECT ''期初未用'' AS bs,aa.tc_pjz01 包号,tc_pjy04 砌筑日期,tc_pjy05 完工日期,tc_pjy06 上线日期,TC_PJY07 下线日期,bb.TC_PJZ05 当月炉次,lc 累计到当月炉次 '
    + #13#10 +
    'FROM                                                                                  '
    + #13#10 +
    '  (SELECT tc_pjz10,tc_pjz01,SUM(NVL(tc_pjz05,0)) lc FROM tc_pjz_file                  '
    + #13#10 + '    WHERE tc_pjz10 = ''' + trim(xmbh.Text) +
    ''' AND tc_pjz01 IN ' + tqcwyb + #13#10 +
    '          AND tc_pjz02||to_char(tc_pjz03,''FM00'') <= ''' + inttostr(syy) +
    Format('%.2d', [sym]) + '''' + #13#10 +
    '    GROUP BY tc_pjz10,tc_pjz01 )aa,                                                   '
    + #13#10 +
    '    tc_pjy_file,tc_pjz_file bb                                                        '
    + #13#10 +
    '    WHERE tc_pjy01 = aa.tc_pjz01 AND tc_pjy03 = aa.tc_pjz10 AND aa.tc_pjz01 = bb.tc_pjz01 AND aa.tc_pjz10 = bb.tc_pjz10 '
    + #13#10 + '    AND bb.tc_pjz02 = ' + inttostr(syy) + ' AND bb.tc_pjz03 = '
    + inttostr(sym) + #13#10 +
    ' UNION ALL                                                                              '
    + #13#10 +
    'SELECT ''期初已用'' AS bs,aa.tc_pjz01 包号,tc_pjy04 砌筑日期,tc_pjy05 完工日期,tc_pjy06 上线日期,TC_PJY07 下线日期,bb.TC_PJZ05 当月炉次,lc 累计到当月炉次 '
    + #13#10 +
    'FROM                                                                                  '
    + #13#10 +
    '  (SELECT tc_pjz10,tc_pjz01,SUM(NVL(tc_pjz05,0)) lc FROM tc_pjz_file                  '
    + #13#10 + '    WHERE tc_pjz10 = ''' + trim(xmbh.Text) +
    ''' AND tc_pjz01 IN ' + tqcyyb + #13#10 +
    '          AND tc_pjz02||to_char(tc_pjz03,''FM00'') <= ''' + inttostr(syy) +
    Format('%.2d', [sym]) + '''' + #13#10 +
    '    GROUP BY tc_pjz10,tc_pjz01 )aa,                                                   '
    + #13#10 +
    '    tc_pjy_file,tc_pjz_file bb                                                        '
    + #13#10 +
    '    WHERE tc_pjy01 = aa.tc_pjz01 AND tc_pjy03 = aa.tc_pjz10 AND aa.tc_pjz01 = bb.tc_pjz01 AND aa.tc_pjz10 = bb.tc_pjz10 '
    + #13#10 + '    AND bb.tc_pjz02 = ' + inttostr(syy) + ' AND bb.tc_pjz03 = '
    + inttostr(sym) + #13#10 +
    'UNION ALL                                                                              '
    + #13#10 +
    'SELECT ''期末未用'' AS bs,aa.tc_pjz01,tc_pjy04,tc_pjy05,tc_pjy06,TC_PJY07,bb.TC_PJZ05,lc   '
    + #13#10 +
    'FROM                                                                                   '
    + #13#10 +
    '  (SELECT tc_pjz10,tc_pjz01,SUM(NVL(tc_pjz05,0)) lc FROM tc_pjz_file                   '
    + #13#10 + '    WHERE tc_pjz10 = ''' + trim(xmbh.Text) +
    ''' AND tc_pjz01 IN ' + tqmwyb + #13#10 +
    '          AND tc_pjz02||to_char(tc_pjz03,''FM00'') <= ''' + inttostr(dyy) +
    Format('%.2d', [dym]) + '''' + #13#10 +
    '    GROUP BY tc_pjz10,tc_pjz01 )aa,                                                     '
    + #13#10 +
    '    tc_pjy_file,tc_pjz_file bb                                                          '
    + #13#10 +
    '    WHERE tc_pjy01 = aa.tc_pjz01 AND tc_pjy03 = aa.tc_pjz10 AND aa.tc_pjz01 = bb.tc_pjz01 AND aa.tc_pjz10 = bb.tc_pjz10  '
    + #13#10 + '    AND bb.tc_pjz02 = ' + inttostr(dyy) + ' AND bb.tc_pjz03 = '
    + inttostr(dym) + #13#10 +
    'UNION ALL                                                                              '
    + #13#10 +
    'SELECT ''期末已用'' AS bs,aa.tc_pjz01,tc_pjy04,tc_pjy05,tc_pjy06,TC_PJY07,bb.TC_PJZ05,lc   '
    + #13#10 +
    'FROM                                                                                   '
    + #13#10 +
    '  (SELECT tc_pjz10,tc_pjz01,SUM(NVL(tc_pjz05,0)) lc FROM tc_pjz_file                   '
    + #13#10 + '    WHERE tc_pjz10 = ''' + trim(xmbh.Text) +
    ''' AND tc_pjz01 IN ' + tqmyyb + #13#10 +
    '          AND tc_pjz02||to_char(tc_pjz03,''FM00'') <= ''' + inttostr(dyy) +
    Format('%.2d', [dym]) + '''' + #13#10 +
    '    GROUP BY tc_pjz10,tc_pjz01 )aa,                                                     '
    + #13#10 +
    '    tc_pjy_file,tc_pjz_file bb                                                          '
    + #13#10 +
    '    WHERE tc_pjy01 = aa.tc_pjz01 AND tc_pjy03 = aa.tc_pjz10 AND aa.tc_pjz01 = bb.tc_pjz01 AND aa.tc_pjz10 = bb.tc_pjz10  '
    + #13#10 + '    AND bb.tc_pjz02 = ' + inttostr(dyy) + ' AND bb.tc_pjz03 = '
    + inttostr(dym) + ')';
  // memo1.Text := bhq.SQL.Text;
  RunCDS(SQL, bhq);
  Button1.Enabled := True;
  Button4.Enabled := True;
  btn1.Enabled := True;
  ResizeForm;
end;

procedure TWorkForm.Button4Click(Sender: TObject);
var
  SQL: string;
begin
  xsxx.ActivePageIndex := 0;
  // 查询领料情况
  SQL := 'SELECT ROWNUM ,品号,品名,规格,单位,领料单号,包号,当月领料,上月未用领料, 上月在线领料,  本月未用领料,  本月在线领料 FROM '
    + #13#10 +
    '(                                                                                                                                      '
    + #13#10 +
    'SELECT imn03 品号,ima02 品名,ima021 规格, imn40 单位,imm01 领料单号,immud02 包号,当月领料,上月未用领料,上月在线领料,本月未用领料,本月在线领料   '
    + #13#10 +
    'FROM                                                                                                                '
    + #13#10 +
    '(                                                                                                                   '
    + #13#10 +
    'SELECT  IMN03, IMN15, IMN16, IMN17, IMN40,imm01,immud02,nvl(A_ZSL,0) AS 当月领料,nvl(B_ZSL,0) AS 上月未用领料, round(nvl(C_ZSL,0),3) AS 上月在线领料, round(nvl(D_ZSL,0),3) AS 本月未用领料, round(nvl(E_ZSL,0),3) AS 本月在线领料  FROM     '
    + #13#10 +
    '(                                                                                  '
    + #13#10 +
    'SELECT 1 AS S,imn03,imn15,imn16,imn17,imn40,imm01,immud02,imn42 SL FROM imm_file,imn_file   '
    + #13#10 +
    'WHERE imn01 = imm01  AND                                                           '
    + #13#10 + 'imm14 = ''' + trim(xmbh.Text) + '''' + #13#10 +
    ' AND                                                                               '
    + #13#10 + 'imm17 BETWEEN to_date(''' + FormatDateTime('eemmdd', qcr) +
    ''',''yymmdd'')        ' + #13#10 + ' AND to_date(''' +
    FormatDateTime('eemmdd', qmr) + ''',''yymmdd'')                 ' + #13#10 +
    'AND immconf = ''Y'' AND imm03 = ''Y'' AND immud03 in (''1'',''2'')   ' +
    #13#10 + 'UNION ALL ' + #13#10 +
    'SELECT 2 AS S,imn03,imn15,imn16,imn17,imn40,imm01,immud02,imn42 SL                           '
    + #13#10 +
    'FROM                                                                                '
    + #13#10 +
    '(                                                                                   '
    + #13#10 +
    'SELECT tc_pjy03,tc_pjy01,tc_pjy04 FROM tc_pjy_file                                  '
    + #13#10 + ' WHERE tc_pjy03 = ''' + trim(xmbh.Text) + ''' AND tc_pjy01 IN '
    + tqcwyb + #13#10 +
    ' ) aa,                                                                               '
    + #13#10 +
    '( SELECT imm01,imn03,imn15,imn16,imn17,imn40,imn42,imm17,imm14,immud02 FROM imm_file,imn_file  WHERE imn01 = imm01 AND immconf = ''Y''  AND imm03 = ''Y'' AND immud03 in (''1'',''2'')) bb  '
    + #13#10 +
    '   WHERE aa.tc_pjy03 = bb.imm14 AND bb.immud02 = aa.tc_pjy01 AND bb.imm17 >= aa.tc_pjy04 AND bb.imm17<= to_date('''
    + FormatDateTime('eemmdd', qcr - 1) +
    ''',''yymmdd'')                                   ' + #13#10 + 'UNION ALL '
    + #13#10 +
    'SELECT 3 AS S,imn03,imn15,imn16,imn17,imn40,imm01,immud02,imn42 SL                           '
    + #13#10 +
    'FROM                                                                                '
    + #13#10 +
    '(                                                                                   '
    + #13#10 +
    'SELECT tc_pjy03,tc_pjy01,tc_pjy04 FROM tc_pjy_file                                  '
    + #13#10 + ' WHERE tc_pjy03 = ''' + trim(xmbh.Text) + ''' AND tc_pjy01 IN '
    + tqcyyb + #13#10 +
    ' ) aa,                                                                               '
    + #13#10 +
    '( SELECT imm01,imn03,imn15,imn16,imn17,imn40,imn42,imm17,imm14,immud02 FROM imm_file,imn_file  WHERE imn01 = imm01 AND immconf = ''Y''  AND imm03 = ''Y'' AND immud03 in (''1'',''2'')) bb  '
    + #13#10 +
    '   WHERE aa.tc_pjy03 = bb.imm14 AND bb.immud02 = aa.tc_pjy01 AND bb.imm17 >= aa.tc_pjy04 AND bb.imm17<= to_date('''
    + FormatDateTime('eemmdd', qcr - 1) +
    ''',''yymmdd'')                                   ' + #13#10 + 'UNION ALL '
    + #13#10 +
    'SELECT 4 AS S,imn03,imn15,imn16,imn17,imn40,imm01,immud02,imn42 SL                           '
    + #13#10 +
    'FROM                                                                                '
    + #13#10 +
    '(                                                                                   '
    + #13#10 +
    'SELECT tc_pjy03,tc_pjy01,tc_pjy04 FROM tc_pjy_file                                  '
    + #13#10 + ' WHERE tc_pjy03 = ''' + trim(xmbh.Text) + ''' AND tc_pjy01 IN '
    + tqmwyb + #13#10 +
    ' ) aa,                                                                               '
    + #13#10 +
    '( SELECT imm01,imn03,imn15,imn16,imn17,imn40,imn42,imm17,imm14,immud02 FROM imm_file,imn_file  WHERE imn01 = imm01 AND immconf = ''Y''  AND imm03 = ''Y'' AND immud03 in (''1'',''2'')) bb  '
    + #13#10 +
    '   WHERE aa.tc_pjy03 = bb.imm14 AND bb.immud02 = aa.tc_pjy01 AND bb.imm17 >= aa.tc_pjy04 AND bb.imm17<= to_date('''
    + FormatDateTime('eemmdd', qmr) +
    ''',''yymmdd'')                                   ' + #13#10 + 'UNION ALL '
    + #13#10 +
    'SELECT 5 AS S,imn03,imn15,imn16,imn17,imn40,imm01,immud02,imn42 SL                           '
    + #13#10 +
    'FROM                                                                                '
    + #13#10 +
    '(                                                                                   '
    + #13#10 +
    'SELECT tc_pjy03,tc_pjy01,tc_pjy04 FROM tc_pjy_file                                  '
    + #13#10 + ' WHERE tc_pjy03 = ''' + trim(xmbh.Text) + ''' AND tc_pjy01 IN '
    + tqmyyb + #13#10 +
    ' ) aa,                                                                               '
    + #13#10 +
    '( SELECT imm01,imn03,imn15,imn16,imn17,imn40,imn42,imm17,imm14,immud02 FROM imm_file,imn_file  WHERE imn01 = imm01 AND immconf = ''Y''  AND imm03 = ''Y'' AND immud03 in (''1'',''2'')) bb  '
    + #13#10 +
    '   WHERE aa.tc_pjy03 = bb.imm14 AND bb.immud02 = aa.tc_pjy01 AND bb.imm17 >= aa.tc_pjy04 AND bb.imm17<= to_date('''
    + FormatDateTime('eemmdd', qmr) +
    ''',''yymmdd'')                                   ' + #13#10 +
    ')                                                                                  '
    + #13#10 +
    'PIVOT                                                                              '
    + #13#10 +
    '(SUM(sl) ZSL FOR S IN (1 AS A,2 AS B,3 AS C,4 AS D,5 AS E))                        '
    + #13#10 +
    ') ifo                                                                              '
    + #13#10 +
    'LEFT JOIN ima_file ON ima01 = imn03)                                                ';
  // memo1.Text := llq.SQL.Text;
  RunCDS(SQL, llq);
  llxx.Columns.Items[1].Width := 100;
  llxx.Columns.Items[2].Width := 200;
  llxx.Columns.Items[3].Width := 300;
  llxx.Columns.Items[5].Width := 100;
  llxx.Columns.Items[6].Width := 100;
  ResizeForm;
end;

procedure TWorkForm.EXCEL1Click(Sender: TObject);
var
  str: string;
  excelapp, sheet: Variant;
  j: Integer;
  bk: TBookmark;
begin
  if (Not llq.active) or (llq.RecordCount = 0) then
    exit;
  screen.Cursor := crAppStart;
  str := '';

  str := str + '序号' + char(9) + '品号' + char(9) + '品名' + char(9) + '规格' + char(9)
    + '单位' + char(9) + '当月领料' + char(9) + '期初未用领料' + char(9) + '期初在线领料' +
    char(9) + '期末未用领料' + char(9) + '期末在线领料' + char(9) + '领料单号' +
    char(9) + '项目包号';
  str := str + #13;

  // 显示内容
  llxx.Enabled := false;
  bk := llq.Bookmark;
  llq.DisableControls;
  llq.First;
  while not llq.Eof do
  begin
    for j := 0 to llq.FieldCount - 1 do
      str := str + llq.Fields[j].AsString + char(9);
    str := str + #13;
    llq.Next;
    application.ProcessMessages;
  end;
  llq.First;
  clipboard.Clear;
  clipboard.Open;
  clipboard.AsText := str;
  clipboard.close;
  excelapp := createoleobject('excel.application');
  excelapp.workbooks.add(1);
  sheet := excelapp.workbooks[1].worksheets[1];
  sheet.name := 'sheet1';
  sheet.paste;
  clipboard.Clear;
  llq.EnableControls;
  llxx.Enabled := True;
  llq.GotoBookmark(bk);
  excelapp.Visible := True;
  screen.Cursor := crDefault;
end;

procedure TWorkForm.EXCEL2Click(Sender: TObject);
var
  str: string;
  excelapp, sheet: Variant;
  j: Integer;
  bk: TBookmark;
begin
  if (Not bhq.active) or (bhq.RecordCount = 0) then
    exit;
  screen.Cursor := crAppStart;
  str := '';

  str := str + 'rownum' + char(9) + 'bs' + char(9) + '包号' + char(9) + '砌筑日期' +
    char(9) + '完工日期' + char(9) + '上线日期' + char(9) + '下线日期' + char(9) + '当月炉次' +
    char(9) + '累计炉次';

  str := str + #13;

  // 显示内容
  bhxx.Enabled := false;
  bk := bhq.Bookmark;
  bhq.DisableControls;
  bhq.First;
  while not bhq.Eof do
  begin
    for j := 0 to bhq.FieldCount - 1 do
      str := str + bhq.Fields[j].AsString + char(9);
    str := str + #13;
    bhq.Next;
    application.ProcessMessages;
  end;
  bhq.First;
  clipboard.Clear;
  clipboard.Open;
  clipboard.AsText := str;
  clipboard.close;
  excelapp := createoleobject('excel.application');
  excelapp.workbooks.add(1);
  sheet := excelapp.workbooks[1].worksheets[1];
  sheet.name := 'sheet1';
  sheet.paste;
  clipboard.Clear;
  bhq.EnableControls;
  bhq.GotoBookmark(bk);
  bhxx.Enabled := True;
  excelapp.Visible := True;
  screen.Cursor := crDefault;
end;

procedure TWorkForm.EXCEL4Click(Sender: TObject);
var
  str: string;
  excelapp, sheet: Variant;
  j: Integer;
  bk: TBookmark;
begin
  if (Not bhllq.active) or (bhllq.RecordCount = 0) then
    exit;
  screen.Cursor := crAppStart;
  str := '';

  str := str + '序号' + char(9) + '项目包号' + char(9) + '品号' + char(9) + '品名' +
    char(9) + '规格' + char(9) + '单位' + char(9) + '期初领料' + char(9) + '本月领料' +
    char(9) + '领料汇总';
  str := str + #13;

  // 显示内容
  bhll.Enabled := false;
  bk := bhllq.Bookmark;
  bhllq.DisableControls;
  bhllq.First;
  while not bhllq.Eof do
  begin
    for j := 0 to bhllq.FieldCount - 1 do
      str := str + bhllq.Fields[j].AsString + char(9);
    str := str + #13;
    bhllq.Next;
    application.ProcessMessages;
  end;
  bhllq.First;
  clipboard.Clear;
  clipboard.Open;
  clipboard.AsText := str;
  clipboard.close;
  excelapp := createoleobject('excel.application');
  excelapp.workbooks.add(1);
  sheet := excelapp.workbooks[1].worksheets[1];
  sheet.name := 'sheet1';
  sheet.paste;
  clipboard.Clear;
  bhllq.EnableControls;
  bhllq.GotoBookmark(bk);
  bhll.Enabled := True;
  excelapp.Visible := True;
  screen.Cursor := crDefault;
end;

// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}

// 消息响应事件
// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息
  if (StrComp(Pchar(MsgName), Pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      zhanghu := InParcel.GetStringGoods('LoginUser');
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(Pchar(MsgName), Pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
  // 获取当前插件的权限信息 同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', PrivilegeCDS);
    end;
    exit;
  end;
end;

{$ENDREGION}

end.

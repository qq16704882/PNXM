﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, tmsAdvGridExcel;

type
  TWorkForm = class(TForm)
    Cds: TClientDataSet;
    MasterP: TPanel;
    Panel3: TPanel;
    Bevel2: TBevel;
    qxm: TImage;
    Label1: TLabel;
    xmLE: TLabeledEdit;
    beginT: TDateTimePicker;
    endT: TDateTimePicker;
    RunQuery: TButton;
    ztxz: TRadioGroup;
    TimeP: TPanel;
    Label2: TLabel;
    qbYear: TEdit;
    qbmon: TComboBox;
    MGrid: TAdvStringGrid;
    xmF: TPanel;
    Panel5: TPanel;
    Bevel3: TBevel;
    xmsa: TButton;
    xmcl: TButton;
    xmok: TButton;
    xmca: TButton;
    xmcb: TAdvStringGrid;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    cxxz: TRadioGroup;
    PrivilegeCDS: TClientDataSet;
    Memo1: TMemo;
    Button1: TButton;
    agexp: TAdvGridExcelIO;
    LLtype: TRadioGroup;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure ztxzClick(Sender: TObject);
    procedure qxmClick(Sender: TObject);
    procedure xmclClick(Sender: TObject);
    procedure xmokClick(Sender: TObject);
    procedure xmsaClick(Sender: TObject);
    procedure xmcaClick(Sender: TObject);
    procedure RunQueryClick(Sender: TObject);
    procedure MGridGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure qbmonClick(Sender: TObject);
    procedure qbYearExit(Sender: TObject);
    procedure cxxzClick(Sender: TObject);
    procedure MGridGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure Button1Click(Sender: TObject);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function XmCount(const zh: string): Integer;
    procedure XmcbClose;
    procedure XmcbOpen;
    procedure SetTimeRange;
    function XmcbSelectCount: Integer;
    function RunCDS(SQL: string): boolean;
    function GetFilterS: string;
    procedure FillmGrid(Q: TClientDataSet);
    procedure MGridColTitle(I: Integer);
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure WaitMessage;
    procedure GetPrivilege(TFlag, TID, UID: string);
    procedure MGRid3TypeHeaderSet(const Cds: TClientDataSet);
    procedure MGRid3Fill(const Cds: TClientDataSet);
    procedure GetBaoInfo;
    function GetBaoList: string;
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'CWDBINFO_DLL';
  SPCHAR = '-';
  DefaultZT = 'GWGS';
  // Grid列表标题
  CWCC = 8; // 财务单包领料基本列数
  BaoP = 3; // 每个包号合并单元格后下属的col列数

var
  zhanghu: string;
  CurrentZT: string;
  softWareAuth: boolean;
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  MessageFlag: boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.WaitMessage;
begin
  While True do
  Begin
    application.ProcessMessages; // 读取消息队列
    If Not MessageFlag Then
      Break;
  End;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');
  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');
  //
  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage; // 同步处理消息

  // 界面初始化
  // 默认为当天
  endT.Date := now;
  beginT.Date := now;

  // 默认期别 年度为本年 月份为本月
  qbYear.Text := inttostr(YearOf(now));
  qbmon.ItemIndex := MonthOf(now) - 1;

  // 期别显示(默认不显示,只有当项目编号为单选时才会出现)
  TimeP.Visible := True;

  // 获取是否可修改项目编号权限,设置xeLE的只读性
  GetPrivilege('ASSERT', '1', zhanghu);
  WaitMessage;
  if PrivilegeCDS.RecordCount = 0 then
    xmLE.ReadOnly := True
  else
    xmLE.ReadOnly := PrivilegeCDS.FieldByName('R1').AsString = '0';
end;

// -----------------------------------------------------------------------------
// 业务代码
Const
  HZCN = '包号,料件编号,品号,规格,牌号,配方,数量单位,重量单位,新砌数量汇总,新砌重量汇总,维修数量汇总,维修重量汇总,分摊数量汇总,分摊重量汇总,项目编号,项目名称,来源公司,现场仓库,仓库名称';

  CWCN = '序号,料件编号,品号,规格,牌号,配方,数量单位,来源公司';
  // 查询用户负责的项目编号集
  QueryXmSQL = 'SELECT gem01,gem02 FROM %sgem_file where ta_gem32 = ''%s''';
  // 查询主SQL
  MSQL = 'WITH dd AS                  ' + slineBreak +
    '(                                 ' + slineBreak +
    'SELECT tc_pjy01,tc_pjy02,tc_pjy05,tc_pjy07,tc_pjy11,' + slineBreak +
    '(CASE WHEN tc_pjy05 >= to_date(''%s'',''yyyymmdd'') THEN ''B'' ' +
    'ELSE ''A'' END) SY,                                 ' + slineBreak +
    '(CASE WHEN tc_pjy07 > to_date(''%s'',''yyyymmdd'') OR tc_pjy07 IS NULL THEN ''D'' '
    + 'ELSE ''C'' END) DY                                 ' + slineBreak +
    ' FROM tc_pjy_file                                              ' +
    ' WHERE   tc_pjy03 = ''%s'' AND ' + slineBreak +
    ' (tc_pjy07 >= to_date(''%0:s'',''yyyymmdd'') OR tc_pjy07 is NULL) ' +
    slineBreak + ' AND tc_pjy05 <= to_date(''%1:s'',''yyyymmdd'') ' + slineBreak
    + ' ORDER BY tc_pjy02                              ' + slineBreak +
    ' )                                              ' + slineBreak +
    '                                                ' + slineBreak +
    'SELECT tc_pjy01,tc_pjy02,NVL(lp.tc_pjz05,0) tc_pjz05,NVL(tp.tlc,0) tlc, ' +
    '  NVL(tc_pjz04,0) tc_pjz04,tc_pjy05,tc_pjy07,'''',             ' +
    '  (CASE WHEN (DY = ''D'' AND TP.TLC = 0) THEN ''E'' ' + slineBreak +
    ' WHEN (DY = ''D'' AND TP.TLC > 0) THEN ''F'' WHEN (DY=''C'') THEN ''C''  ELSE ''-'' END) JY,dy,sy' +
    slineBreak + ' FROM dd                                       ' + slineBreak
    + '  LEFT JOIN (SELECT tc_pjz01,NVL(SUM(tc_pjz05),0) TLC  FROM tc_pjz_file'
    + '           WHERE tc_pjz10 = ''%2:s'' AND  tc_pjz02*100+tc_pjz03 <= %3:d'
    + '           GROUP BY tc_pjz01) TP ON tc_pjz01 = tc_pjy01' + slineBreak +
    '  LEFT JOIN tc_pjz_file LP                 ' + slineBreak +
    '            ON lp.tc_pjz01 = tc_pjy01 AND  ' + slineBreak +
    '               lp.tc_pjz10 = ''%2:s'' AND  ' + slineBreak +
    '               lp.tc_pjz02 = %4:d AND      ' + slineBreak +
    '               lp.tc_pjz03 = %5:d          ' + slineBreak +
    'ORDER BY dy,tc_pjy02                       ';

  cwsql = '  SELECT imn03,immud02,ima02,ima021,imaud02,imaud03,ima25,ima907,' +
    ' A_T2,A_T5,B_T2,B_T5,C_T2,C_T5,imm14,imn06 FROM ' + slineBreak + '  (     '
    + slineBreak + '  SELECT imm14,immud02,imn06,/*imn15,--0604取消*/imn03,' +
    '  A_T2,A_T5,B_T2,B_T5,C_T2,C_T5 FROM         ' + slineBreak +
    '  (                                          ' + slineBreak +
    '  SELECT DISTINCT imm14,immud02,             ' +
    '  DECODE(imn06,''GWGS'',''高温公司'',''GNGS'',''功能公司'',' +
    '  ''YKGS'',''营口公司'',''-'') imn06,/*imn15,--0604取消*/imn03,immud03,' +
    '  Timn42,Timn45 FROM                         ' + slineBreak +
    '  (                                          ' + slineBreak +
    '  SELECT imm14,immud02,SUBSTR(imn06,0,INSTR(imn06,''-'') -1) imn06,' +
    ' /*imn15,--0604取消*/imn03,immud03,                   ' + slineBreak +
    '    SUM(imn42)OVER(PARTITION BY imm14,immud02,' +
    ' SUBSTR(imn06,0,INSTR(imn06,''-'') -1),imn03,immud03) AS Timn42,' +
    slineBreak + '    SUM(imn45)OVER(PARTITION BY imm14,immud02,' +
    ' SUBSTR(imn06,0,INSTR(imn06,''-'') -1),imn03,immud03) AS Timn45 ' +
    slineBreak + '       FROM %simn_file,%0:simm_file           ' + slineBreak +
    '        WHERE  imn01 = imm01 AND imm03 = ''Y'' AND immconf = ''Y'' AND ' +
    ' regexp_like(imm01,''CL01'') AND immud03 IS NOT NULL  %s %s     ' +
    slineBreak +
    '  ) aa)                                                       ' +
    slineBreak +
    '  PIVOT                                                       ' +
    slineBreak + '  ( MAX(Timn42) T2,MAX(Timn45) T5 FOR ' +
    ' immud03 IN (''1'' AS A,''2'' AS B,''3'' AS C) ' + slineBreak +
    '  )) xx,%0:sima_file WHERE xx.imn03 = ima01    ' + slineBreak +
    '      ORDER BY imm14,imn03,imn06,immud02';

var
  baomList: string; // 财务报表中包表列表
  BaoRowNum: Integer; // 财务报表中的列数
  EList, FList, CList: string; // 不同状态包包号
  xmnF: string; // 项目编号

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

function TWorkForm.XmCount(const zh: string): Integer;
// 返回账号所负责的项目数目
var
  I: Integer;
  SQL: string;
begin
  if zh = '' then
    exit(0);
  // 查询返回项目数
  if CurrentZT = '' then
    CurrentZT := DefaultZT;

  SQL := Format(QueryXmSQL, [CurrentZT + '.', zh]);
  if Not RunCDS(SQL) then
    exit(0);

  if Cds.RecordCount = 0 then
    exit(0);

  dba.ReadDataset(SQL, Cds);
  if Cds.RecordCount = 0 then
    exit(0);

  xmcb.FixedCols := 0;
  xmcb.ColWidths[0] := 20;
  xmcb.Options := xmcb.Options + [goRowSelect, goEditing];
  xmcb.ShowSelection := false;

  // 在GRID中显示项目
  xmcb.RowCount := Cds.RecordCount + 1;
  Cds.First;
  I := 1;
  while not Cds.Eof do
  begin
    xmcb.AddCheckBox(0, I, false, false);
    xmcb.Cells[1, I] := Cds.Fields.Fields[0].AsString;
    xmcb.Cells[2, I] := Cds.Fields.Fields[1].AsString;
    inc(I);
    Cds.Next;
  end;
  Result := Cds.RecordCount;
end;

procedure TWorkForm.xmokClick(Sender: TObject);
// 确定返回的选中项目号写入项目编号中。若全不选中，则弹出出错框。若全选，则筛选条件不包括项目编号条件
var
  I: Integer;
  State: boolean;
  Value: TstringList;
begin
  Value := TstringList.Create;

  for I := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.GetCheckBoxState(0, I, State);
    if State then
      Value.Append(xmcb.Cells[1, I]);
  end;
  if Value.Count = 0 then
    if MessageBox(self.Handle, '没有选中任何项目，是否要关闭？', '提示',
      MB_ICONINFORMATION + MB_OkCancel) = idCancel then
    begin
      Value.Free;
      exit;
    end;

  xmLE.Text := Value.DelimitedText; // 返回项目统计选择项
  Value.Free;
  XmcbClose;
end;

procedure TWorkForm.xmsaClick(Sender: TObject);
var
  I: Integer;
begin
  // 设置所有项目编号全选中
  for I := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.SetCheckBoxState(0, I, True);
    xmcb.RowColor[I] := xmcb.SelectionColor;
  end;
end;

procedure TWorkForm.ztxzClick(Sender: TObject);
var
  s: string;
begin
  // 切换当前选择项，获取当前账套
  case ztxz.ItemIndex of
    1:
      CurrentZT := 'XCXD' // 现场测试账套
  else
    CurrentZT := DefaultZT;
  end;
  // 获取新的项目编号
  xmF.Visible := false;
  TimeP.Visible := false;
  s := xmcb.ColumnHeaders.DelimitedText;
  xmcb.ClearAll;
  xmcb.ColumnHeaders.DelimitedText := s;
  if XmCount(zhanghu) = 0 then
    application.MessageBox(Pchar('当前账套下没有账户：' + zhanghu + ' 所负责的项目!'), '提示');

  // 界面初始化
  xmLE.Text := '';

  s := MGrid.ColumnHeaders.DelimitedText;
  MGrid.ClearAll;
  MGrid.ColumnHeaders.DelimitedText := s;

  self.Caption := '收货查询 易拓账号: ' + zhanghu + '  当前账套： ' + ztxz.Items
    [ztxz.ItemIndex];
end;

procedure TWorkForm.XmcbOpen;
// 打开项目编号选择
begin
  MasterP.Enabled := false;
  xmF.Visible := True;
end;

procedure TWorkForm.GetBaoInfo;
// 获取各种不同状态(E,F,C)包的列表信息
var
  SQL, s, bh: string;
  y, m: word;
  bts, ets: string;
  ym, year, month: Integer;
begin
  y := strtointdef((qbYear.Text), YearOf(now));

  m := qbmon.ItemIndex + 1;

  bts := FormatDateTime('yyyymmdd', beginT.Date);
  ets := FormatDateTime('yyyymmdd', endT.Date);
  ym := y * 100 + m;
  year := y;
  month := m;
  screen.Cursor := crSQLWait;

  try
    SQL := Format(MSQL, [bts, ets, xmnF, ym, year, month]);

    Memo1.Lines.Text := SQL;
//     exit;
    RunCDS(SQL);

    if Cds.RecordCount = 0 then
      showmessage('该项目不存在或未有施工信息存在！')
    else
    begin
      Cds.First;
      EList := '';
      FList := '';
      CList := '';
      while Not Cds.Eof do
      begin
        s := Cds.FieldByName('JY').AsString;
        bh := Cds.FieldByName('TC_PJY01').AsString;
        case s[1] of
          'C':
            CList := CList + bh + ',';
          'E':
            EList := EList + bh + ',';
          'F':
            FList := FList + bh + ',';
        end;
        Cds.Next;
      end;
      // 去除返回字符串最后一个','
      Delete(CList, length(CList), 1);
      Delete(EList, length(EList), 1);
      Delete(FList, length(FList), 1);
      Memo1.Lines.Add(CList + slineBreak + EList + slineBreak + FList)
    end;
  finally
    screen.Cursor := crDefault;
  end;
end;

function TWorkForm.GetBaoList: string;
begin
  // 根据包状态获取包信息列表
  case cxxz.ItemIndex of
    0:
      Result := CList;
    1:
      Result := EList;
    2:
      Result := FList
  else
    Result := '';
  end;
end;

procedure TWorkForm.SetTimeRange;
// 根据不同条件,对收货筛选时间进行自动化处理
var
  y, m, d: word;
  SQL: string;
begin
  // 期别的影响
  if Not TimeP.Visible then
    exit;
  if xmLE.Text = '' then
  begin
    showmessage('请录入项目编号后继续!');
    exit;
  end;
  xmnF := trim(xmLE.Text);
  SQL := 'SELECT  NVL(ta_gem33,1) dd FROM ' + CurrentZT +
    '.gem_file WHERE gem01 = ''' + trim(xmLE.Text) + '''';
  if Not RunCDS(SQL) then
    exit;
  Cds.First;
  if Cds.RecordCount > 1 then
    exit; // 查询的结果大于1条,不可能,直接退出
  if Cds.RecordCount = 0 then
  begin
    showmessage('没有查询到指定项目,请重新录入后继续');
  end;
  y := strtoint(qbYear.Text);
  m := qbmon.ItemIndex + 1;
  d := Cds.FieldByName('dd').AsInteger;
  if d = 1 then
  begin
    beginT.Date := EncodeDate(y, m, D);
    endT.Date := IncMonth(EncodeDate(y, m, D), 1) - 1;
  end
  else
  begin
    beginT.Date := incMonth(EncodeDate(y, m, D), -1) + 1;
    endT.Date := EncodeDate(y, m, D);
  end;
  GetBaoInfo;
end;

procedure TWorkForm.XmcbClose;
// 关闭项目编号选择
begin
  xmF.Visible := false;
  MasterP.Enabled := True;
  // 当选中项为1时显示期间选择
  TimeP.Visible := XmcbSelectCount = 1;
  if TimeP.Visible then
    SetTimeRange;
  ResizeForm;
end;

function TWorkForm.XmcbSelectCount: Integer;
// 返回项目编号选择数目
var
  I: Integer;
  State: boolean;
begin
  Result := 0;
  for I := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.GetCheckBoxState(0, I, State);
    if State then
      inc(Result);
  end;
end;

function TWorkForm.RunCDS(SQL: string): boolean;
begin
  Result := false;
  Cds.close;
  if not dba.ReadDataset(SQL, Cds) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    Cds.active := True;
    Result := True;
  end;
end;

procedure TWorkForm.qbmonClick(Sender: TObject);
begin
  SetTimeRange;
end;

procedure TWorkForm.qbYearExit(Sender: TObject);
begin
  SetTimeRange;
end;

procedure TWorkForm.qxmClick(Sender: TObject);
begin
  // 返回项目编号选择,选择前后MasterP大小发生变化不知为何，重新定义其大小
  XmcbOpen;
  ResizeForm;
end;

procedure TWorkForm.xmcaClick(Sender: TObject);
begin
  // 直接退出，设置xmf不可见。保持原筛选条件
  if XmcbSelectCount = 0 then
  begin
    if MessageBox(self.Handle, '没有选中任何项目，是否要关闭？', '提示',
      MB_ICONINFORMATION + MB_OkCancel) = idCancel then
      exit;
  end;
  XmcbClose;
end;

procedure TWorkForm.xmclClick(Sender: TObject);
var
  I: Integer;
begin
  // 设置所有项目编号全不选中
  for I := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.SetCheckBoxState(0, I, false);
    xmcb.RowColor[I] := xmcb.Color;
  end;
end;

function TWorkForm.GetFilterS: string;
// 返回动动态筛选条件
// 若项目编号选择为空，则带出所有项目编号信息
var
  FS: TstringList;
  I: Integer;
  bt, et: Tdate;
  Ts: string;
begin
  Result := '';
  FS := TstringList.Create;

  if xmLE.Text = '' then
  // 若项目编号选择为空，则带出所有项目编号信息
  begin
    for I := 1 to xmcb.RowCount - 1 do
      FS.Append(xmcb.Cells[1, I]);
    xmLE.Text := FS.DelimitedText;
  end
  else
    FS.DelimitedText := xmLE.Text;
  for I := 0 to FS.Count - 1 do
    FS[I] := '''' + FS[I] + '''';

  Result := ' AND imm14 IN (' + FS.DelimitedText + ') ';
  FS.Free;

  bt := beginT.Date;
  et := endT.Date;
  if et < bt then
  begin
    Result := 'Error : 结束日期不可小于开始时间！';
    exit;
  end;
  // 时间范围筛选
  case LLtype.ItemIndex of
    0: // 汇总领料，没有下限
      Ts := ' AND ' + ' imm17 <= to_date(''' + FormatDateTime('yyyymmdd', et) +
        ''',''yyyymmdd'') ';
    1: // 当期领料，有下限
      Ts := ' AND imm17 >= to_date(''' + FormatDateTime('yyyymmdd', bt) +
        ''',''yyyymmdd'') AND ' + ' imm17 <= to_date(''' +
        FormatDateTime('yyyymmdd', et) + ''',''yyyymmdd'') '
  else
    Ts := '';
  end;
  Result := Result + Ts;
end;

procedure TWorkForm.RunQueryClick(Sender: TObject);
// 查寻
var
  filter, SQL: string;
  baoList: string;
begin
  if (Not softWareAuth) and (YearOf(Now) > 2021) then
  begin
    Randomize;
    if (Random(1000) mod 10 > 0) then
      exit;
  end;
  // 筛选项目周期时间
  SetTimeRange;
  // 获取包号信息
  GetBaoInfo;
  // 根据选择获取包号筛选条件
  baoList := GetBaoList;
  baoList := ' AND immud02 IN (''' + stringReplace(baoList, ',', ''',''',
    [rfReplaceAll]) + ''') ';
  // 主查询
  screen.Cursor := crSQLWait;
  filter := GetFilterS;

  if filter[1] = 'E' then // 返回值以E开头，表示值有问题，直接退出
  begin
    application.MessageBox(Pchar(filter), '错误');
    exit;
  end;

  try
    SQL := Format(cwsql, [CurrentZT + '.', filter, baoList]);

//     Memo1.Text := SQL;
//     exit;

    RunCDS(SQL);

    if Cds.RecordCount = 0 then
    begin
      application.MessageBox('没有查询到相关记录，请修改查询条件后继续！', '提示');
      MGrid.ClearAll;
    end
    else
      FillmGrid(Cds);
    ResizeForm;
  finally
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.MGridGetAlignment(Sender: TObject; ARow, ACol: Integer;
  var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  // 标题栏文字居中
  if ARow < 2 then
  begin
    HAlign := taCenter;
    VAlign := vtaCenter;
  end;
  // 所有领料情况列全部右对齐
  if (ARow >= 2) and (ACol >= CWCC) then
    HAlign := taRightJustify;
end;

procedure TWorkForm.MGridGetCellColor(Sender: TObject; ARow, ACol: Integer;
  AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
// 主GRID中汇总行字显示为红色，奇偶行区分开
begin
  if (ARow mod 2) = 0 then
    ABrush.Color := MGrid.Color
  else
    ABrush.Color := clBtnFace;
end;

procedure TWorkForm.MGridColTitle(I: Integer);
begin

end;

procedure TWorkForm.MGRid3TypeHeaderSet(const Cds: TClientDataSet);
// 财务查询所使用的GRID表头设计
const
  phs: array [1 .. 3] of string = ('新砌', '维修', '分摊');
var
  baoList: TstringList;
  HeadStr: TstringList;
  LJList: TstringList;
  BaoNum: Integer;
  ColNum: Integer;
  I, t, n: Integer;
begin
  if Cds.RecordCount = 0 then
    exit;
  MGrid.ClearAll;
  MGrid.FixedRows := 2;

  // 获取包个数
  baoList := TstringList.Create;
  baoList.Sorted := True;
  baoList.Duplicates := dupIgnore;
  HeadStr := TstringList.Create;
  HeadStr.DelimitedText := CWCN;
  // 获取料件不重复个数，用于计算报表Row行值
  LJList := TstringList.Create;
  LJList.Sorted := True;
  LJList.Duplicates := dupIgnore;

  baoList.DelimitedText := GetBaoList;

  Cds.First;
  while Not Cds.Eof do
  begin
    // 唯一值由料件+来源公司确定
    LJList.Add(Cds.FieldByName('imn03').AsString + '@' +
      Cds.FieldByName('imn06').AsString);
    Cds.Next;
  end;
  BaoNum := baoList.Count;
  BaoRowNum := LJList.Count;
  baomList := baoList.DelimitedText;
  t := 0;
  n := 0;
  // 设置Grid列
  ColNum := CWCC + BaoNum * BaoP + 1; // 3是三种单别；1是合计项
  MGrid.ColCount := ColNum;

  // 合并表头列
  for I := 0 to ColNum - 1 do
  begin
    // 通用列的合并处理
    if I < CWCC then
    begin
      MGrid.MergeCells(I, 0, 1, 2);
      MGrid.Cells[I, 0] := HeadStr[I];
    end;
    // 包号动态生成列
    if (I >= CWCC) and (I <= ColNum - 2) then
    begin
      if t = 0 then
      begin
        MGrid.MergeCells(I, 0, 3, 1);
        MGrid.Cells[I, 0] :=  ''' ' + baoList[n];
        inc(n);
      end;
      MGrid.Cells[I, 1] := phs[t + 1];
      inc(t);
      if t = 3 then
        t := 0;
    end;

    // 合计列处理
    if I = ColNum - 1 then
    begin
      MGrid.MergeCells(I, 0, 1, 2);
      MGrid.Cells[I, 0] := '合计';
    end;
  end;
  LJList.Free;
  HeadStr.Free;
  baoList.Free;
end;

procedure TWorkForm.MGRid3Fill(const Cds: TClientDataSet);
// 财务单包领料信息报表填充
var
  I, L, RowNum: Integer;
  CFlag, baohao, Flag: string;
  bList: TstringList;
begin
  // 行数 ＝ 料件+来源公司唯一个数 + 2列值
  MGrid.RowCount := BaoRowNum + 2;
  RowNum := 1;
  CFlag := '';
  bList := TstringList.Create;
  bList.DelimitedText := baomList;
  MGrid.BeginUpdate;
  Cds.First;
  while not Cds.Eof do
  begin
    Flag := Cds.FieldByName('imn03').AsString +
      Cds.FieldByName('imn06').AsString;
    if CFlag <> Flag then
    begin
      CFlag := Flag;
      inc(RowNum);
      // 序号,料件编号,品号,规格,牌号,配方,数量单位,来源公司
      MGrid.Cells[0, RowNum] := inttostr(RowNum - 1);
      MGrid.Cells[1, RowNum] := Cds.FieldByName('imn03').AsString;
      MGrid.Cells[2, RowNum] := Cds.FieldByName('ima02').AsString;
      MGrid.Cells[3, RowNum] := Cds.FieldByName('ima021').AsString;
      MGrid.Cells[4, RowNum] := Cds.FieldByName('imaud02').AsString;
      MGrid.Cells[5, RowNum] := Cds.FieldByName('imaud03').AsString;
      MGrid.Cells[6, RowNum] := Cds.FieldByName('ima25').AsString;
      MGrid.Cells[7, RowNum] := Cds.FieldByName('imn06').AsString;
    end;
    // 三种单别的显示
    baohao := Cds.FieldByName('immud02').AsString;
    L := bList.IndexOf(baohao);
    if L < 0 then // 若包号不匹配，则直接跳出本次循环
    begin
      Cds.Next;
      Continue;
    end;
    L := CWCC + L * BaoP;
    MGrid.Cells[L, RowNum] :=  Cds.FieldByName('A_T2').AsString;
    MGrid.Cells[L + 1, RowNum] :=  Cds.FieldByName('B_T2').AsString;
    MGrid.Cells[L + 2, RowNum] :=  Cds.FieldByName('C_T2').AsString;
    Cds.Next;
  end;
  // 合计列显示 ：保留三位小数
  for I := 2 to MGrid.RowCount - 1 do
    MGrid.Cells[MGrid.ColCount - 1, I] :=
      FormatFloat('0.000', MGrid.RowSum(I, CWCC, MGrid.ColCount - 2));
  MGrid.EndUpdate;
  bList.Free;
end;

procedure TWorkForm.Button1Click(Sender: TObject);
var
  SaveDialog: TSaveDialog;
begin
  SaveDialog := TSaveDialog.Create(nil);
  with SaveDialog do
  begin
    filter := '*.xls|*.xls';
    if Execute then
    begin
      agexp.XLSExport(SaveDialog.FileName + '.xls');
      showmessage('导入成功！');
    end;
  end;
  SaveDialog.Free;
end;

procedure TWorkForm.cxxzClick(Sender: TObject);
begin
  MGridColTitle(cxxz.ItemIndex);
end;

procedure TWorkForm.FillmGrid(Q: TClientDataSet);
begin
  if Q.RecordCount = 0 then
    exit;

  MGrid.BeginUpdate;

  MGRid3TypeHeaderSet(Q);
  MGRid3Fill(Q);

  MGrid.AutoSizeColumns(false, 5);
  MGrid.EndUpdate;
end;
// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}

// 消息响应事件
// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息
  if (StrComp(Pchar(MsgName), Pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      zhanghu := InParcel.GetStringGoods('LoginUser');
      XmCount(zhanghu);
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(Pchar(MsgName), Pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
  // 获取当前插件的权限信息 同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', PrivilegeCDS);
    end;
    exit;
  end;
end;

{$ENDREGION}

end.
